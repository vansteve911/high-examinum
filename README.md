High-Examinum
--

基于graphql的web服务demo

- 后端apollo-server，前端vue-apollo2。
- 支持crud service封装多种数据源（sql ORM为knex）
- server建议node 8.6+以上，启动方式：

```
cd server/
node index.js
```

- client启动方式，安装vue-cli(3)：

```
cd client/
vue serve
```

