const AUTH_TOKEN = 'apollo-token'

export async function onLogin(apolloClient, token) {
  if (typeof localStorage !== 'undefined' && token) {
    localStorage.setItem(AUTH_TOKEN, token)
  }
  try {
    await apolloClient.resetStore()
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log('%cError on cache reset (login)', 'color: orange;', e.message)
  }
}

export async function signIn(token, apolloClient) {
  if (typeof localStorage !== 'undefined' && token) {
    localStorage.setItem(AUTH_TOKEN, token)
  }
  if (apolloClient) {
    try {
      await apolloClient.resetStore();
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(
        "%cError on cache reset (login)",
        "color: orange;",
        e.message
      ); // TODO
    }
  }
}

export async function onLogout(apolloClient) {
  if (typeof localStorage !== 'undefined') {
    localStorage.removeItem(AUTH_TOKEN)
  }
  if (apolloClient) {
    try {
      await apolloClient.resetStore()
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log('%cError on cache reset (logout)', 'color: orange;', e.message)
    }
  }
}

export function signOut() {
  if (typeof localStorage !== 'undefined') {
    localStorage.removeItem(AUTH_TOKEN)
  }
}

export function onApiError(error) {
  // eslint-disable-next-line no-console
  console.warn('has hasAuthError', error, error.graphQLErrors, hasAuthError(error));
  if (hasAuthError(error)) {
    if (typeof localStorage !== 'undefined') {
      localStorage.removeItem(AUTH_TOKEN)
    }
    window.location.href = '#/login';
  } else if (isUserExpiredError(error)) {
    if (typeof localStorage !== 'undefined') {
      localStorage.removeItem(AUTH_TOKEN)
    }
    alert("试用时间已过，请升级为正式用户");
    window.location.href = '/';
  } else if (hasUnactivatedError(error)) {
    window.location.href = '#/home';
  }
}

export function loggedIn() {
  return !!localStorage.getItem(AUTH_TOKEN);
}

function hasAuthError(error) {
  // TODO to be refined!!!
  // eslint-disable-next-line no-console
  console.log(error);
  return (error && error.networkError && error.networkError.result.errors[0].extensions.code === 'UNAUTHENTICATED');
}

function isUserExpiredError(error) {
  // eslint-disable-next-line no-console
  console.log('in isUserExpiredError', error.graphQLErrors && error.graphQLErrors[0].message);
  return error.graphQLErrors && error.graphQLErrors[0].message === "试用时间已过，请升级为正式用户";
}

function hasUnactivatedError(error) {
  // eslint-disable-next-line no-console
  console.log('in hasUnactivatedError', error.graphQLErrors && error.graphQLErrors[0].message);
  return error.graphQLErrors && error.graphQLErrors[0].message === "用户未激活";
}
