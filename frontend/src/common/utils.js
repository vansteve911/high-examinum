import { onApiError } from "./auth";

export async function invokeApi(op, errorTip = 'Error') {
  try {
    const { data, errors } = await op();
    if (errors) {
      Vue.prototype.$message({
        message: `${errorTip}: ${errors[0]}`,
        type: 'warning'
      });
    } else {
      return data;
    }
  } catch (e) {
    Vue.prototype.$message.error(`${errorTip}: ${e}`);
    onApiError(e);
  }
}

export async function getLoginUser(vueComponent, doReadCache = true) {
  // eslint-disable-next-line no-console
  console.log("getLoginUser by", vueComponent.name); // TODO tmp
  if (doReadCache && vueComponent.$root.loginUser) {
    // eslint-disable-next-line no-console
    console.log("return $root.loginUser", vueComponent.$root.loginUser); // TODO tmp
    return vueComponent.$root.loginUser;
  }
  let response;
  try {
    response = await vueComponent.$apollo.query({
      query: require("../graphql/MyProfile.gql"),
    });
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error("MyProfile.gql error", e);
    onApiError(e);
    return undefined;
  }
  vueComponent.$root.loginUser = response.data.myProfile;
  // eslint-disable-next-line no-console
  console.log("return response.loginUser", response.data.myProfile); // TODO tmp
  return response.data.myProfile;
}

export async function getLoginAdmin(vueComponent, doReadCache = true) {
  // eslint-disable-next-line no-console
  console.log("getLoginAdmin by", vueComponent.name); // TODO tmp
  if (doReadCache && vueComponent.$root.loginAdmin) {
    // eslint-disable-next-line no-console
    console.log("return $root.loginAdmin", vueComponent.$root.loginAdmin); // TODO tmp
    return vueComponent.$root.loginAdmin;
  }
  let response;
  try {
    response = await vueComponent.$apollo.query({
      query: require("../graphql/MyAdminProfile.gql"),
    });
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error("MyAdminProfile.gql error", e);
    onApiError(e);
    return undefined;
  }
  vueComponent.$root.loginAdmin = response.data.myAdminProfile;
  // eslint-disable-next-line no-console
  console.log("return response.loginAdmin", response.data.myAdminProfile); // TODO tmp
  return response.data.myAdminProfile;
}

export function isMobileBrowser() {
  const sUserAgent = navigator.userAgent.toLowerCase();
  const bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
  const bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
  const bIsMidp = sUserAgent.match(/midp/i) == "midp";
  const bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
  const bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
  const bIsAndroid = sUserAgent.match(/android/i) == "android";
  const bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
  const bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
  if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM) {
    return true
  } else {
    return false
  }
}