export const YEARS = [
  { id: 2019, name: "2019" },
  { id: 2018, name: "2018" },
  { id: 2017, name: "2017" },
  { id: 2016, name: "2016" }
];

export const CATEGORIES = [{ id: 0, name: "理工类" }, { id: 1, name: "文史类" }]

export const ALL_BATCHES = [
  { id: 0, name: "第一批" },
  { id: 1, name: "第二批" },
  { id: 2, name: "第一批预科" },
  { id: 3, name: "第二批预科" }
];

export const PROVINCES = [
  { id: 11, name: "北京" }, { id: 12, name: "天津" }, { id: 13, name: "河北" }, { id: 14, name: "山西" }, { id: 15, name: "内蒙古" }, { id: 21, name: "辽宁" }, { id: 22, name: "吉林" }, { id: 23, name: "黑龙江" }, { id: 31, name: "上海" }, { id: 32, name: "江苏" }, { id: 33, name: "浙江" }, { id: 34, name: "安徽" }, { id: 35, name: "福建" }, { id: 36, name: "江西" }, { id: 37, name: "山东" }, { id: 41, name: "河南" }, { id: 42, name: "湖北" }, { id: 43, name: "湖南" }, { id: 44, name: "广东" }, { id: 45, name: "广西" }, { id: 46, name: "海南" }, { id: 50, name: "重庆" }, { id: 51, name: "四川" }, { id: 52, name: "贵州" }, { id: 53, name: "云南" }, { id: 54, name: "西藏" }, { id: 61, name: "陕西" }, { id: 62, name: "甘肃" }, { id: 63, name: "青海" }, { id: 64, name: "宁夏" }, { id: 65, name: "新疆" }, { id: 71, name: "台湾" }, { id: 81, name: "香港" }, { id: 82, name: "澳门" }
];

const NON_MINORITY_BATCHES = [
  { id: 0, name: "第一批" },
  { id: 1, name: "第二批" }
];

export const HAN_ETHNIC_ID = 1

export function availableApplyBatches(candidateInfo) {
  return candidateInfo.ethnicity === HAN_ETHNIC_ID ? NON_MINORITY_BATCHES : ALL_BATCHES
}

export const CANDIDATE_CATEGORIES = [
  { id: 0, name: "理科" },
  { id: 1, name: "文科" }
];

export const USER_TYPES = [
  { id: 0, name: "体验用户" },
  { id: 1, name: "正式用户" },
  { id: -1, name: "已删除用户" }
];

export const ALL_ETHNICS = [
  { id: HAN_ETHNIC_ID, name: "汉族" },
  { id: 9, name: "壮族" }, // TODO: put forward
  { id: 7, name: "苗族" },
  { id: 12, name: "瑶族" },
  { id: 11, name: "侗族" },
  { id: 31, name: "仫佬族" },
  { id: 35, name: "毛南族" },
  { id: 4, name: "回族" },
  { id: 49, name: "京族" },
  { id: 8, name: "彝族" },
  { id: 24, name: "水族" },
  { id: 36, name: "仡佬族" },
  { id: 2, name: "满族" },
  { id: 3, name: "蒙古族" },
  { id: 5, name: "藏族" },
  { id: 6, name: "维吾尔族" },
  { id: 10, name: "布依族" },
  { id: 13, name: "白族" },
  { id: 14, name: "土家族" },
  { id: 15, name: "哈尼族" },
  { id: 16, name: "哈萨克族" },
  { id: 17, name: "傣族" },
  { id: 18, name: "黎族" },
  { id: 19, name: "傈僳族" },
  { id: 20, name: "佤族" },
  { id: 21, name: "畲族" },
  { id: 22, name: "高山族" },
  { id: 23, name: "拉祜族" },
  { id: 25, name: "东乡族" },
  { id: 26, name: "纳西族" },
  { id: 27, name: "景颇族" },
  { id: 28, name: "柯尔克孜族" },
  { id: 29, name: "土族" },
  { id: 30, name: "达斡尔族" },
  { id: 32, name: "羌族" },
  { id: 33, name: "布朗族" },
  { id: 34, name: "撒拉族" },
  { id: 37, name: "锡伯族" },
  { id: 38, name: "阿昌族" },
  { id: 39, name: "普米族" },
  { id: 40, name: "朝鲜族" },
  { id: 41, name: "塔吉克族" },
  { id: 42, name: "怒族" },
  { id: 43, name: "乌孜别克族" },
  { id: 44, name: "俄罗斯族" },
  { id: 45, name: "鄂温克族" },
  { id: 46, name: "德昂族" },
  { id: 47, name: "保安族" },
  { id: 48, name: "裕固族" },
  { id: 50, name: "塔塔尔族" },
  { id: 51, name: "独龙族" },
  { id: 52, name: "鄂伦春族" },
  { id: 53, name: "赫哲族" },
  { id: 54, name: "门巴族" },
  { id: 55, name: "珞巴族" },
  { id: 56, name: "基诺族" }
];

export const PRIORITY_LEVELS = ["A", "B", "C", "D", "E", "F"];

export const SIM_APPLICATION_STATUES = [
  {
    id: 0,
    key: "DRAFT",
    name: "草稿"
  },
  {
    id: 1,
    key: "SUBMITTED",
    name: "已提交"
  },
  {
    id: 2,
    key: "APPROVED",
    name: "教师已通过"
  },
  {
    id: 3,
    key: "DECLINED",
    name: "教师已驳回"
  },
  {
    id: -1,
    key: "DELETED",
    name: "已删除"
  }
]

// article consts

// export const ARTICLE_TYPES = [
//   {
//   }
// ]

export const ARTICLE_SPECIAL_KEYS = [
  {
    key: "siteIntro",
    name: "网站介绍",
    type: "intro"
  },
  {
    key: "aboutUs",
    name: "关于我们",
    type: "intro"
  },
  {
    key: "teacherIntro1",
    name: "教师介绍一",
    type: "teacher"
  },
  {
    key: "teacherIntro2",
    name: "教师介绍二",
    type: "teacher"
  },
  {
    key: "teacherIntro3",
    name: "教师介绍三",
    type: "teacher"
  },
  {
    key: "teacherIntro4",
    name: "教师介绍四",
    type: "teacher"
  },
  {
    key: "teacherIntro5",
    name: "教师介绍五",
    type: "teacher"
  },
  {
    key: "teacherIntro6",
    name: "教师介绍六",
    type: "teacher"
  },
]

// admin consts

export const ADMIN_TYPES = [
  {
    'id': -1,
    'name': '失效管理员',
  },
  {
    'id': 0,
    'name': '内容编辑',
  },
  {
    'id': 1,
    'name': '审核员',
  },
  {
    'id': 2,
    'name': '教师',
  },
  {
    'id': 999,
    'name': '超级管理员',
  },
];

export const ADMIN_PRIVILEGS = [
  {
    'key': 'CREATE_ADMIN',
    'name': '创建管理员',
  },
  {
    'key': 'MODIFY_ADMIN',
    'name': '修改管理员信息',
  },
  {
    'key': 'VIEW_ADMIN_INFO',
    'name': '查看管理员信息',
  },
  {
    'key': 'MODIFY_USER_INFO',
    'name': '修改用户信息',
  },
  {
    'key': 'CHANGE_USER_TYPE',
    'name': '变更用户类型',
  },
  {
    'key': 'VIEW_USER_INFO',
    'name': '查看用户信息',
  },
  {
    'key': 'EDIT_SCHOOL_INFO',
    'name': '编辑学校信息',
  },
  {
    'key': 'EDIT_CONTENT_INFO',
    'name': '编辑内容信息',
  },
];