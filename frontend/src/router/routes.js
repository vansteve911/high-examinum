import Layout from '../pages/layout/Layout.vue'
import StaticPageLayout from '../pages/layout/StaticPageLayout.vue'
import Login from '../pages/Login.vue'
import InviteCodeBatch from '../pages/InviteCodeBatch.vue'
import NewInviteCode from '../pages/NewInviteCode.vue'
import Index from '../pages/Index.vue'
import IndexMobile from '../pages/IndexMobile.vue'
import NotFoundPage from '../pages/404.vue'

import AdmissionAnalyse from '../pages/AdmissionAnalyse.vue'
import Recommend from '../pages/Recommend.vue'
import SimApplication from "../pages/SimApplication.vue";
import UserHome from '../pages/UserHome.vue'
import PasswordManage from '../pages/PasswordManage.vue'

import ArticlePage from "../pages/Article.vue";

import SchoolManage from '../pages/admin/SchoolManage.vue';
import AdminManage from '../pages/admin/AdminManage.vue';
import ArticleManage from '../pages/admin/ArticleManage.vue';
import InviteCodeManage from '../pages/admin/InviteCodeManage.vue';
import UserManage from '../pages/admin/UserManage.vue';
import SystemConfigManage from '../pages/admin/SystemConfigManage.vue';


// TODO for test only
import SimApplyTargetSelector from "../components/SimApplyTargetSelector.vue";

import { isMobileBrowser } from "../common/utils";

export default [
  {
    path: '/',
    component: isMobileBrowser() ? IndexMobile : Index
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/inviteCodeBatch',
    component: InviteCodeBatch
  },
  {
    path: '/newInviteCode',
    component: NewInviteCode
  },
  
  {
    path: '/page',
    component: StaticPageLayout,
    children: [
      {
        path: 'article',
        component: ArticlePage
      }
    ],
  },
  {
    path: '',
    component: Layout,
    children: [
      {
        path: '/admissionAnalyse',
        component: AdmissionAnalyse
      },
      {
        path: '/recommend',
        component: Recommend,
      },
      {
        path: '/simApplication',
        component: SimApplication,
      },
      {
        path: '/home',
        component: UserHome
      },
      {
        path: '/activate',
        component: PasswordManage
      },
      {
        path: '/changePassword',
        component: PasswordManage
      },
      // admin pages
      {
        path: '/schoolManage',
        component: SchoolManage
      },
      {
        path: '/adminManage',
        component: AdminManage
      },
      {
        path: '/articleManage',
        component: ArticleManage
      },
      {
        path: '/inviteCodeManage',
        component: InviteCodeManage
      },
      {
        path: '/userManage',
        component: UserManage
      },
      {
        path: '/configManage',
        component: SystemConfigManage
      },
      // TODO test pages
      {
        path: '/simApplySelector',
        component: SimApplyTargetSelector,

      },
      // TODO more 
      {
        path: '*',
        component: NotFoundPage
      }
    ]
  },
  {
    path: '*',
    component: NotFoundPage
  }
]