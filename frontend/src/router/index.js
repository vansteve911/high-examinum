import routes from './routes'
import { loggedIn } from '../common/auth';

// load vue-router middleware
// Vue.use(VueRouter)

const router = new VueRouter({
    routes
});

// TODO apply login validator
router.beforeEach((route, redirect, next) => {
    if (!loggedIn() && route.path !== '/login' && route.path !== '/' && route.path !== '/page/article' && route.path !== '/inviteCodeBatch' && route.path !== '/newInviteCode') {
        next({
            path: '/login',
            query: {
                redirect: route.fullPath
            }
        })
    } else {
        next()
    }
});

export default router;