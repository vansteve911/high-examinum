
import App from "./App.vue";

import router from "./router";
import "./plugins/element";
import { createProvider } from "./plugins/vue-apollo";
import { isMobileBrowser } from "./common/utils";

Vue.config.productionTip = false;

new Vue({
  router,
  loginUser: null,
  loginAdmin: null,
  apolloProvider: createProvider(),
  render: h => h(App),
  data() {
    return {
      isMobileBrowser: isMobileBrowser()
    }
  }
}).$mount("#app")
