module.exports = {
  productionSourceMap: false, //process.env.NODE_ENV === "PROD" ? false : true,
  configureWebpack: {
    externals: {
      vue: "Vue",
      element: "ElementUI",
      "vue-router": "Vue-Router"
    }
  }
}