#!/bin/bash
set -e
set -x

if [[ "$#" == "0" ]]; then
  echo "Usage: deploy.sh <PROD|TEST>";
  exit 1
fi

NODE_ENV="$1"
if [[ "$NODE_ENV" != "PROD" ]] && [[ "$NODE_ENV" != "TEST" ]];then
  echo "illegal env $NODE_ENV"
  exit 1
fi

BASE_DIR=$(cd "$(dirname "$0")/..";pwd)
client_dir="$BASE_DIR/frontend" # TODO tmp this folder
server_dir="$BASE_DIR/server"

# build client dist 
echo "build front-end files..."
cd "$client_dir"
if [[ "$NODE_ENV" == "PROD" ]]; then
  npm run build
else
  yarn build
fi
mkdir -p "$server_dir/public"
rm -f "$server_dir/public/*"
cp -r "$client_dir/dist/"* "$server_dir/public"

# install server
echo "building back-end files..."
cd "$server_dir"
# npm install
if [[ "$NODE_ENV" == "PROD" ]]; then
  npm install
else
  yarn install
fi

echo "build completed."
