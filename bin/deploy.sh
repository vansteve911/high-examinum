#!/bin/bash
set -e

if [[ "$#" == "0" ]]; then
  echo "Usage: build.sh <PROD|TEST>";
  exit 1
fi

BASE_DIR=$(cd "$(dirname "$0")/..";pwd)
client_dir="$BASE_DIR/client"
server_dir="$BASE_DIR/server"

if [[ "$1" = "PROD" ]];then
  source "$BASE_DIR/conf/prod.env"
elif [[ "$1" = "TEST" ]];then
  source "$BASE_DIR/conf/test.env"
else
  echo "illegal env $NODE_ENV"
  exit 1
fi

# deploy params
SERVER_PORT="4000"

# make logs TODO: tmp!
mkdir -p "$LOG_DIR"
log_file="$LOG_DIR/app_`date +%Y%m%d`.log"
touch "$log_file"
nohup node "$server_dir/src/index.js" >> "$log_file" 2>&1 &
echo "server started at $(date) on port $SERVER_PORT"
