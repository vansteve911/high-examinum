#!/bin/bash
set -e
set -x

BASE_DIR=$(cd "$(dirname "$0")/..";pwd)
client_dir="$BASE_DIR/client"
server_dir="$BASE_DIR/server"

# install client
echo "install front-end..."
cd "$client_dir"
npm install
# install server
echo "install back-end..."
cd "$server_dir"
npm install

echo "build completed."

backend_pid=$(ps aux|grep "node index.js" | grep -v grep | awk '{print $2}')
if [[ "$backend_pid" == "" ]]; then
  echo "WARNING: no running server is detected, please run command `node index.js` in server/src/ "
fi

echo "start running front-end dev server..."
VUE_APP_GRAPHQL_HTTP='http://localhost:4000/graphql' npm run dev

