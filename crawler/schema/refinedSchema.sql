CREATE TABLE `school` (
  `id` INT PRIMARY KEY,
  `name` CHAR(20) NOT NULL,
  `province` INT NOT NULL,
  `raw_intro` TEXT NOT NULL,
  `attributes` TEXT NOT NULL
);

CREATE TABLE `school_admission_history` (
  `school_id` INT NOT NULL,
  `category` INT NOT NULL,
  `batch` INT NOT NULL,
  `year` INT NOT NULL,
  `recruit_count` INT NOT NULL,
  `candidate_count` INT NOT NULL,
  `enroll_count` INT NOT NULL,
  `lowest_score` INT NOT NULL,
  `lowest_rank` INT NOT NULL,
  `average_score` INT NOT NULL,
  `average_rank` INT NOT NULL
);

CREATE TABLE `major_admission_history` (
  `school_id` INT NOT NULL,
  `category` INT NOT NULL,
  `batch` INT NOT NULL,
  `school_major` CHAR(50) NOT NULL,
  `year` INT NOT NULL,
  `recruit_count` INT NOT NULL,
  `reapply_count` INT NOT NULL,
  `lowest_score` INT NOT NULL,
  `lowest_rank` INT NOT NULL,
  `average_score` INT NOT NULL,
  `average_rank` INT NOT NULL,
  `score_dist` TEXT NOT NULL
);