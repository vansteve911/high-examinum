-- FORMAL tables

CREATE TABLE `school`(
  `id` INT PRIMARY KEY,
  `code` INT NOT NULL DEFAULT '0',
  `name` CHAR(20) NOT NULL,
  `province` INT NOT NULL,
  `categories` CHAR(10) NOT NULL,
  `batches` CHAR(10) NOT NULL
);

CREATE TABLE `province` (
  `id` INT PRIMARY KEY,
  `full_name` CHAR(20),
  `short_name` CHAR(10),
  `abbreviation` CHAR(5),
  `fake_id` INT
);

CREATE TABLE `school_admission_history` (
  `category` INT,
  `batch` INT,
  `school_id` INT,
  `year` INT,
  `lowest_score` INT NOT NULL DEFAULT '-1',
  `lowest_rank` INT NOT NULL DEFAULT '-1',
  `average_score` INT NOT NULL DEFAULT '-1',
  `average_rank` INT NOT NULL DEFAULT '-1',
  `recruit_count` INT NOT NULL DEFAULT '-1',
  `candidate_count` INT NOT NULL DEFAULT '-1',
  `enroll_count` INT NOT NULL DEFAULT '-1'
);

-- from tmp_major_admission_history & tmp_major_recruit_plan
CREATE TABLE `major_admission_history` (
  `category` INT,
  `batch` INT,
  `school_id` INT,
  `year` INT,
  `major_id` INT NOT NULL DEFAULT -1,
  `school_major` CHAR(50),
  `lowest_score` INT NOT NULL DEFAULT '-1',
  `lowest_rank` INT NOT NULL DEFAULT '-1',
  `average_score` INT NOT NULL DEFAULT '-1',
  `average_rank` INT NOT NULL DEFAULT '-1',
  `recruit_count` INT NOT NULL DEFAULT '-1'
);

-- from tmp_score_distribution
CREATE TABLE `admission_score_distribution` (
  `category` INT,
  `batch` INT,
  `school_id` INT,
  `year` INT,
  `major_id` INT NOT NULL DEFAULT -1,
  `school_major` CHAR(50),
  `score` INT,
  `ranking` INT,
  `enroll_count` INT
);

-- from tmp_school_major_ranking
CREATE TABLE `school_major_ranking` (
  `school_id` INT,
  `major_id` INT NOT NULL DEFAULT -1,
  `major_name` CHAR(50),
  `categories` CHAR(10),
  `level` CHAR(50),
  `rank` INT NOT NULL DEFAULT -1,
  `total_ranks` INT NOT NULL DEFAULT -1
);

--
CREATE TABLE `audit_record` (
  `object_id` CHAR(128) NOT NULL,
  `type` INT NOT NULL,
  `status` INT NOT NULL DEFAULT '0',
  `operator` INT NOT NULL DEFAULT '',
  `desc` CHAR(256) NOT NULL DEFAULT '',
  `attributes` CHAR(256) NOT NULL DEFAULT '',
  `add_time` DATETIME,
  `update_time` DATETIME
);