-- mid tables
CREATE TABLE `tmp_school_batch`(
  `id` INT,
  `name` CHAR(50) NOT NULL,
  `province_name` CHAR(20) NOT NULL,
  `category` INT NOT NULL,
  `batch` INT NOT NULL
);

CREATE TABLE `tmp_school_id_code_relation` (
  `id` INT PRIMARY KEY,
  `code` INT
);


CREATE TABLE `tmp_major_recruit_plan` (
  `category` INT,
  `batch` INT,
  `school_id` INT,
  `recruit_count` INT,
  `major_name` CHAR(50)
);

CREATE TABLE `tmp_major_admission_history` (
  `category` INT,
  `batch` INT,
  `school_id` INT,
  `major_name` CHAR(50),
  `year` INT,
  `lowest_score` INT NOT NULL DEFAULT '-1',
  `lowest_rank` INT NOT NULL DEFAULT '-1',
  `average_score` INT NOT NULL DEFAULT '-1',
  `average_rank` INT NOT NULL DEFAULT '-1',
  `recruit_count` INT NOT NULL DEFAULT '-1'
);

CREATE TABLE `tmp_score_distribution` (
  `category` INT,
  `batch` INT,
  `school_id` INT,
  `year` INT,
  `score` INT,
  `ranking` INT,
  `enroll_count` INT,
  `major_name` CHAR(50)
);

CREATE TABLE `tmp_school_major_ranking` (
  `category` INT,
  `school_id` INT,
  `major_id` INT,
  `major_name` CHAR(50),
  `level` CHAR(50),
  `rank` INT NOT NULL DEFAULT -1,
  `total_ranks` INT NOT NULL DEFAULT -1
);


CREATE TABLE `tmp_major_recruit_plan` (
  `category` INT,
  `batch` INT,
  `school_id` INT,
  `recruit_count` INT,
  `major_name` CHAR(50)
);

-- FORMAL tables

CREATE TABLE `school`(
  `id` INT PRIMARY KEY,
  `code` INT NOT NULL DEFAULT '0',
  `name` CHAR(20) NOT NULL,
  `province` INT NOT NULL,
  `categories` CHAR(10) NOT NULL,
  `batches` CHAR(10) NOT NULL
);

CREATE TABLE `province` (
  `id` INT PRIMARY KEY,
  `full_name` CHAR(20),
  `short_name` CHAR(10),
  `abbreviation` CHAR(5),
  `fake_id` INT
);

CREATE TABLE `school_admission_history` (
  `category` INT,
  `batch` INT,
  `school_id` INT,
  `year` INT,
  `lowest_score` INT NOT NULL DEFAULT '-1',
  `lowest_rank` INT NOT NULL DEFAULT '-1',
  `average_score` INT NOT NULL DEFAULT '-1',
  `average_rank` INT NOT NULL DEFAULT '-1',
  `recruit_count` INT NOT NULL DEFAULT '-1',
  `candidate_count` INT NOT NULL DEFAULT '-1',
  `enroll_count` INT NOT NULL DEFAULT '-1'
);

-- from tmp_major_admission_history & tmp_major_recruit_plan
CREATE TABLE `major_admission_history` ( 
  `category` INT,
  `batch` INT,
  `school_id` INT,
  `year` INT,
  `major_id` INT NOT NULL DEFAULT -1,
  `school_major` CHAR(50),
  `lowest_score` INT NOT NULL DEFAULT '-1',
  `lowest_rank` INT NOT NULL DEFAULT '-1',
  `average_score` INT NOT NULL DEFAULT '-1',
  `average_rank` INT NOT NULL DEFAULT '-1',
  `recruit_count` INT NOT NULL DEFAULT '-1'
);
INSERT INTO `major_admission_history` 
SELECT 
`category`,
`batch`,
`school_id`,
'2019',
'-1',
`major_name`,
'-1',
'-1',
'-1',
'-1',
`recruit_count`
FROM tmp_major_recruit_plan;
INSERT INTO `major_admission_history` 
SELECT 
`category`,
`batch`,
`school_id`,
`year`,
'-1',
`major_name`,
`lowest_score`,
`lowest_rank`,
`average_score`, 
`average_rank`,
`recruit_count`
FROM tmp_major_admission_history;

-- from tmp_score_distribution
CREATE TABLE `admission_score_distribution` (
  `category` INT,
  `batch` INT,
  `school_id` INT,
  `year` INT,
  `major_id` INT NOT NULL DEFAULT -1,
  `school_major` CHAR(50),
  `score` INT,
  `ranking` INT,
  `enroll_count` INT
);
INSERT INTO `admission_score_distribution` 
SELECT 
`category`,
`batch`,
`school_id`,
`year`,
'-1',
`major_name`,
`score`,
`ranking`,
`enroll_count`
FROM tmp_score_distribution;

-- from tmp_school_major_ranking
CREATE TABLE `school_major_ranking` (
  `school_id` INT,
  `major_id` INT NOT NULL DEFAULT -1,
  `major_name` CHAR(50),
  `categories` CHAR(10),
  `level` CHAR(50),
  `rank` INT NOT NULL DEFAULT -1,
  `total_ranks` INT NOT NULL DEFAULT -1
);
INSERT INTO `school_major_ranking` 
SELECT 
`school_id`, 
'-1',
`major_name`, 
group_concat(DISTINCT `category`),
min(`level`), 
max(`rank`),
max(`total_ranks`) `total_ranks`
FROM `tmp_school_major_ranking`
WHERE `rank` != '-1' AND `total_ranks` != '-1'
GROUP BY `school_id`, `major_name`;

