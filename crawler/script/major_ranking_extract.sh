#!/bin/bash
BASE_DIR=$(cd "$(dirname "$0")/../output";pwd)
mkdir -p "$BASE_DIR/csv"

for category in "like" "wenke"; do
  cd "$BASE_DIR/${category}_major_ranking"
  cate_code="0"
  if [[ "$category" == "wenke" ]]; then
      cate_code="1"
  fi
  for outfile_type in "school_major_ranking"; do
    echo "" > "$BASE_DIR/csv/${category}_${outfile_type}.csv"
  done
  IFS=$(echo -en "\n\b"); for file in `ls`; do
    IFS=$(echo -en " ")
    arr=(${file//_/ })
    echo "extract major ranking of ${arr[1]}"
    jq_expr='. as {school_code: $school} | 
      .ranking | 
        map( 
          ["'${cate_code}'", $school, .id, .name, .level, .rank, .total_ranks] | map(gsub("\\t";"")) | join("\t")
        ) | join("\n")'
    < "$file" jq -r "$jq_expr" | grep -E "^." >> "$BASE_DIR/csv/${category}_school_major_ranking.csv"
  done
done

cd "$BASE_DIR/csv"
cat like_school_major_ranking.csv wenke_school_major_ranking.csv | sort | uniq > school_major_ranking.csv
