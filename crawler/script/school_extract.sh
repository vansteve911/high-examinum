#!/bin/bash
BASE_DIR=$(cd "$(dirname "$0")/../output";pwd)
mkdir -p "$BASE_DIR/csv"

for outfile_type in "school_batch"; do
  echo "" > "$BASE_DIR/csv/${category}_${outfile_type}.csv"
done
for category in "like" "wenke"; do
    cate_code="0"
    if [[ "$category" == "wenke" ]]; then
        cate_code="1"
    fi
    cd "$BASE_DIR/${category}_school_list"
    IFS=$(echo -en "\n\b"); for file in `ls`; do
      echo "extracting $file"
      jq_expr='. as {province_id: $prov, batch_id: $batch} | 
        .schools | 
          map(
            [.id, .name, $prov, '${cate_code}', $batch] | join("\t")
          ) | join("\n") '
      < "$file" jq  -r "$jq_expr" | sort | uniq >> "${BASE_DIR}/csv/${category}_school_batch.csv"
    done
done
cd "$BASE_DIR/csv"
cat like_school_batch.csv wenke_school_batch.csv | sort | uniq > school_batch.csv

