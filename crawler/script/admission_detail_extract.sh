#!/bin/bash
BASE_DIR=$(cd "$(dirname "$0")/../output";pwd)
mkdir -p "$BASE_DIR/csv"

for category in "like" "wenke"; do
    cd "$BASE_DIR/${category}_admission_detail"
    cate_code="0"
    if [[ "$category" == "wenke" ]]; then
        cate_code="1"
    fi
    for outfile_type in "admission_detail" "school_admission_history" "major_admission_history" "enroll_history"; do
      echo "" > "$BASE_DIR/csv/${category}_${outfile_type}.csv"
    done
    IFS=$(echo -en "\n\b"); for file in `ls`; do
        prefix=${file%.*}
        IFS=$(echo -en " ")
        arr=(${prefix//_/ })
        batch_id="0"
        case "${arr[3]}" in 
            本科第一批) batch_id="0";;
            本科第二批) batch_id="1";;
            本科第一批预科) batch_id="2";;
            本科第二批预科B) batch_id="3";;
        esac
        echo "exporting: ${arr[1]}"
        # export major_recruit_plan
        jq_expr='. as {school_code: $school} | 
          .major_recruit_plan | 
            map( 
              ["'${cate_code}'", "'${batch_id}'", $school, .current_plan_recruit_count, .name] | join("\t")
            ) | join("\n")'
        < "$file" jq -r "$jq_expr" >> "$BASE_DIR/csv/${category}_admission_detail.csv"
        # export school_admission_history
        jq_expr='. as {school_code: $school} | 
          .school_admission_history |
              map(. as $row | 
                (["'${cate_code}'", "'${batch_id}'", $school] + 
                  ( ["year", "lowest_score", "lowest_rank", "average_score", "average_rank", "recruit_count", "candidate_count", "enroll_count"] | 
                    map($row[.]|sub("\\n";""))
                  )
                ) | join("\t") 
              ) | join("\n")'
        < "$file" jq -r "$jq_expr" >> "$BASE_DIR/csv/${category}_school_admission_history.csv"
        # export major_admission_history
        jq_expr='. as {school_code: $school} |
         .major_admission_history | 
          map(. as $row | 
            (["'${cate_code}'", "'${batch_id}'", $school] + 
              (["name", "year", "lowest_score", "lowest_rank", "average_score", "average_rank", "recruit_count"] | 
                map($row[.] | sub("-";""))
              )
            ) | join("\t") 
          ) | join("\n")'
        < "$file" jq -r "$jq_expr" >> "$BASE_DIR/csv/${category}_major_admission_history.csv"
        # export enroll_history
        jq_expr='. as {school_code: $school} |
        .enroll_history | 
          map(. as {year: $year, enroll_detail: $enroll_detail} |
            $enroll_detail |
              map(. as {ranking: $ranking, score: $score, major_distribution: $major_distribution} |
                $major_distribution |
                  map(
                    ["'${cate_code}'", "'${batch_id}'", $school, $year, $score, $ranking, .count, .major] | map(gsub("\\t";"")) | join("\t")
                  ) | join("\n")
              ) | join("\n")
          ) | join("\n")' 
        < "$file" jq -r "$jq_expr" >> "$BASE_DIR/csv/${category}_enroll_history.csv"
    done
done


# {
# "average_score":"688",
# "apply_count":"0",
# "lowest_score":"685",
# "year":"2016",
# "recruit_count":"1",
# "lowest_rank":"16",
# "average_rank":"10",
# "name":"工商管理类"
# },

