#!/bin/bash
BASE_DIR=$(cd "$(dirname "$0")/../output";pwd)
mkdir -p "$BASE_DIR/csv"

for outfile_type in "school_id_code_relation"; do
  echo "" > "$BASE_DIR/csv/${category}_${outfile_type}.csv"
done

for category in "like" "wenke"; do
  cate_code="0"
  if [[ "$category" == "wenke" ]]; then
      cate_code="1"
  fi
  cd "$BASE_DIR/${category}_admission_detail"
  IFS=$(echo -en "\n\b"); for file in `ls`; do 
    < "$file" jq -r '[.school_code, .school_id]|join("\t")' > "$BASE_DIR/csv/${category}_school_id_code_relation.csv"
  done
done

cd "$BASE_DIR/csv"
cat like_school_id_code_relation.csv wenke_school_id_code_relation.csv | sort | uniq > school_id_code_relation.csv
