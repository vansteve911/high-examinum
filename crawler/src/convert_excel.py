# -*- coding: utf-8 -*-

import traceback

from common.common_utils import read_json_file
from common.file_utils import to_path, mkdir, file_exists, listdir
from common.excel_utils import write_to_excel
from meta import meta_config

# TODO


def trans_dict_key(input_dict, key_mapping):
    return dict((key_mapping[k], v) for (k, v) in input_dict.items())


def trans_dict_list(dict_list, key_mapping):
    return [trans_dict_key(x, key_mapping) for x in dict_list]


def dump_adimission_detail(category, output_config):
    output_base_dir = output_config['base_dir']
    # init output dir
    admission_detail_output_dir = to_path(output_base_dir, '%s_admission_detail' % category)
    dump_excel_dir = to_path(output_base_dir, 'dump_excel', 'adimission_detail', category)
    mkdir(dump_excel_dir)
    for file_name in listdir(admission_detail_output_dir):
        admission_detail = None  # TODO fix read json error
        try:
            admission_detail = read_json_file(to_path(admission_detail_output_dir, file_name))
        except Exception:
            print('read file %s failed!' % file_name)
            traceback.print_exc()
            continue
        dump_file = to_path(dump_excel_dir, file_name.replace('.json', ''), file_postfix='.xlsx')
        if file_exists(dump_file):
            print('file %s already exists, skipped' % dump_file)
            continue
        sheets_data = [
            {
                'name': '分专业招生计划',
                'key_name_pairs': [
                    ('name', '专业名称'),
                    ('current_plan_recruit_count', '计划招生人数'),
                ],
                'row_dicts': admission_detail['major_recruit_plan']
            },
            {
                'name': '学校招生计划',
                'key_name_pairs': [
                    ('recruit_count', '计划招生人数'),
                ],
                'row_dicts': [admission_detail['school_recruit_plan']]
            },
            {
                'name': '历年学校录取详情',
                'key_name_pairs': [
                    ('year', '年份'),
                    ('lowest_score', '录取最低分数'),
                    ('lowest_rank', '录取最低分排名'),
                    ('average_score', '录取平均分数'),
                    ('average_rank', '录取平均排名'),
                    ('recruit_count', '计划录取人数'),
                    ('candidate_count', '填报人数'),
                    ('enroll_count', '实际录取人数'),
                ],
                'row_dicts': admission_detail['school_admission_history']
            },
            {
                'name': '历年分专业录取详情',
                'key_name_pairs': [
                    ('name', '专业名称'),
                    ('year', '年份'),
                    ('lowest_score', '录取最低分数'),
                    ('lowest_rank', '录取最低分排名'),
                    ('average_score', '录取平均分数'),
                    ('average_rank', '录取平均排名'),
                    ('recruit_count', '计划录取人数'),
                    ('apply_count', '实际录取人数'),
                ],
                'row_dicts': admission_detail['major_admission_history']
            }
        ]
        try:
            write_to_excel(dump_file, sheets_data, is_dict_row=True)
        except Exception:
            print('dump excel file %s failed!' % dump_file)
            traceback.print_exc()
        print('dumped %s' % dump_file)
        # break


def main():
    # category = sys.argv[1]
    dump_adimission_detail('like', meta_config.OUTPUT)


if __name__ == '__main__':
    main()
