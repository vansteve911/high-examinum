# -*- coding: utf-8 -*-

import requests
import json

YEARS = range(2015, 2021)
RANK_TYPES = { 0: '武书连', 1: '校友会', 2: '软科版'}


def crawl_rank():
    for (rank_type, organization) in RANK_TYPES.items():
        for year in YEARS:
            response = requests.request('get', 'https://h5.okaoyan.com/api/university-rank/list', params={
                'organization': organization,
                'year': year,
            }).content.decode('utf8', 'ignore')
            for info in json.loads(response)['data']:
                print('\t'.join([str(rank_type), str(year), str(info['no']), info['universityName']]))


if __name__ == '__main__':
    crawl_rank()
