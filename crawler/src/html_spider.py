# -*- coding: utf-8 -*-

import sys
import json
import itertools
import re

import requests
from scrapy import Selector

from common.common_utils import read_txt_file, read_json_file, txt_file_iterator, output_to_file, strip_html_tags, json_output_to_csv
from common.file_utils import to_path, mkdir, file_exists, listdir
from meta import meta_config

reload(sys)
sys.setdefaultencoding('utf8')


PROVINCE_NAME_MAP = dict((x['shortName'], x) for x in read_json_file('./meta/provinces.json'))
BATCHES = read_json_file('./meta/batches.json')
BATCH_NAME_MAP = dict((x['name'], x) for x in BATCHES)
BATCH_CODE_MAP = dict((x['code'], x) for x in BATCHES)
CATEGORIES = read_json_file('./meta/categories.json')
CATEGORY_NAME_MAP = dict((x['name'], x) for x in CATEGORIES)
CATEGORY_KEY_MAP = dict((x['key'], x) for x in CATEGORIES)


def parse_request_config(request_builder_config):
    headers = dict((arr[0].strip(' '), arr[1].strip(' '))
                   for arr in txt_file_iterator(request_builder_config['headers_file'],
                                                row_mapper=lambda x: x.replace('\n', '').split(':')))
    headers.update({'Cookie': read_txt_file(request_builder_config['cookies_file'])})
    return {
        'headers': headers
    }


output_base_dir = "../data/output"
request_config = parse_request_config(meta_config.REQUEST_BUILDER)
headers = request_config['headers'].copy()


# tools method
def init():
    mkdir(output_base_dir)
    # for category in CATEGORIES:
    #     for batch in BATCHES:
    #         mkdir(to_path(output_base_dir, category['name'], batch['name']))


# tools method
def do_request(url, method='get', params=None, data=None, headers=None, cookies=None, must_contains=None):
    print('Requesting ' + url)  # TODO
    try:
        html = requests.request(method, url, params=params, data=data, headers=headers, cookies=cookies, timeout=30).content.decode('utf8', 'ignore')
    except Exception as e:
        print('error!', e)
        html = requests.request(method, url, params=params, data=data, headers=headers, cookies=cookies,
                                timeout=30).content.decode('utf8', 'ignore')
    if must_contains and not (must_contains in html):
        print('NOT LOGGED IN! url: %s' % url)
        exit(1)
    return html


def write_html(base_path, segments, html):
    path = to_path(base_path, '_'.join(map(str, segments)), file_postfix='.html')
    print('writing to %s' % path)
    with open(path, 'w') as f:
        f.write(html)


# Common method: find total page
def find_total_pages(html_sel, xpath_expression):
    total_pages_txt = html_sel.xpath(xpath_expression).extract_first()
    return int(re.search('\d+/(\d+)', total_pages_txt).group(1))


# CRAWL http://www.shenqi66.cn/system01.aspx?years=2019&Times=9&subject=&SchoolClass=
def crawl_system1_page(category, batch, year):
    # http://www.shenqi66.cn/system01.aspx?SchoolClass=&Area=&Times=9&subject=&years=2019&PageNo=2
    base_path = to_path(output_base_dir, category, batch, 'sys1')
    mkdir(base_path)

    def crawl_page(page_no):
        # existed_file = to_path(base_path, '_'.join(map(str, ['system1', 'year', year, 'page', page_no])), file_postfix='.html') # TODO fff
        # if file_exists(existed_file):
        #     print('page %s already existed, passed' % page_no)
        #     return []
        sub_url = 'http://www.shenqi66.cn/system01.aspx?SchoolClass=&Area=&Times=%s&subject=&years=%s&PageNo=%s' % (
            batch, year, page_no)
        sub_html = do_request(sub_url, headers=headers, must_contains='baogaotitle')
        write_html(base_path, ['sys1', 'year', year, 'page', page_no], sub_html)
        # find school page links
        sub_html_sel = Selector(text=sub_html)
        return sub_html_sel.xpath('/html/body/form[@id="form1"]/div/table[@class="gpj font_style_1"]/tr/td/a/@href').extract()

    url = 'http://www.shenqi66.cn/system01.aspx?years=%s&Times=%s&subject=&SchoolClass=' % (year, batch)
    html = do_request(url, headers=headers, must_contains='baogaotitle')
    write_html(base_path, ['sys1', 'year', year, 'page', 1], html)
    html_sel = Selector(text=html)
    # find total pages
    total_pages_txt = html_sel.xpath(
        '/html/body/form[@id="form1"]/div/div[@class="page"]/span[@class="pager"]/a[not(@href)]/text()').extract_first()
    total_pages = int(re.search('\d+/(\d+)', total_pages_txt).group(1))
    for page_no in range(1, total_pages + 1):
        school_page_urls = crawl_page(page_no)
        # TODO craw system2 schools
        for school_page_url in school_page_urls:
            school_code = re.search('schoolcode=(\d+)', school_page_url).group(1)
            crawl_system2_page(category, year, school_code, batch)


def crawl_system2_page(category, year, school_code, batch):
    # http://www.shenqi66.cn/system2.aspx?years=2019&schoolcode=10001&Times=9
    url = 'http://www.shenqi66.cn/system2.aspx?years=%s&schoolcode=%s&Times=%s' % (year, school_code, batch)
    html = do_request(url, headers=headers, must_contains='baogaotitle')
    base_path = to_path(output_base_dir, category, batch, 'sys2')
    mkdir(base_path)
    existed_file = to_path(base_path, '_'.join(map(str, ['sys2', 'year', year, 'school', school_code])),
                           file_postfix='.html')  # TODO fff
    if file_exists(existed_file):
        print('file of school_code %s already existed, passed' % school_code)
        return
    write_html(base_path, ['sys2', 'year', year, 'school', school_code], html)
    html_sel = Selector(text=html)
    major_detail_first_url = html_sel.xpath('/html/body/form[@id="form1"]/div/a[contains(@href, "/details.aspx")]/@href').extract_first()
    current_year = int(year)
    for offset in [1, 2, 3]:
        crawl_major_admission_detail(major_detail_first_url, current_year, current_year - offset, school_code,
                                     BATCH_CODE_MAP[batch], CATEGORY_KEY_MAP[category])
    # crawl school major rankings
    crawl_school_major_ranking(school_code)


def crawl_major_admission_detail(first_url, current_year, selected_year, school_code, batch_info, category_info):
    # http://www.shenqi66.cn/Details.aspx?selectyears=2017&years=2019&times=%e6%9c%ac%e7%a7%91%e7%ac%ac%e4%b8%80%e6%89%b9&schoolid=4&subjectgroup=%e6%96%87%e5%8f%b2%e7%b1%bb
    url = 'http://www.shenqi66.cn' + re.sub(r'subjectgroup=[^&]+', 'subjectgroup=%s' % category_info['name'],
                                            re.sub(
                                                r'times=[^&]+', 'times=%s' % batch_info['name'],
                                                re.sub(r'selectyears=\d+', 'selectyears=%s' % selected_year, first_url)
                                            ))
    html = do_request(url, headers=headers, must_contains='daohang1')
    base_path = to_path(output_base_dir, category_info['key'], batch_info['code'], 'sys2_major_detail')
    mkdir(base_path)
    write_html(base_path, ['sys2_major_detail', 'year', current_year, 'school', school_code], html)


def crawl_school_major_ranking(school_code):
    base_path = to_path(output_base_dir, 'major_ranking')
    mkdir(base_path)

    def crawl_page(page_no):
        sub_url = 'http://www.shenqi66.cn/Professionalranking.aspx?PageSize=20&SchoolCode=%s&PageNo=%s' % (school_code, page_no)
        sub_html = do_request(sub_url, headers=headers, must_contains='daohang1')
        write_html(base_path, ['major_rank', school_code, 'page', page_no], sub_html)

    url = 'http://www.shenqi66.cn/Professionalranking.aspx?schoolcode=%s' % school_code
    html = do_request(url, headers=headers, must_contains='daohang1')
    write_html(base_path, ['major_rank', school_code], html)
    html_sel = Selector(text=html)
    total_pages_txt = html_sel.xpath('/html/body/form[@id="form1"]/div/div/div/div[@id="ContentPlaceHolder1_PageInfo"]/span[@class="pager"]/a[contains(text(), "/")]/text()').extract_first()
    total_pages = int(re.search('\d+/(\d+)', total_pages_txt).group(1))
    for page_no in range(1, total_pages + 1):
        crawl_page(page_no)


def main():
    # category = 'wenke'
    # batch = '9'
    year = sys.argv[1]
    # year = '2019'
    init()
    for category in ['like']:
        for batch in ['9', '12', '14', '15']:
            crawl_system1_page(category, batch, year)


if __name__ == '__main__':
    main()

