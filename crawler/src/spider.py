# -*- coding: utf-8 -*-

import sys
import json
import itertools
import re

import requests
from scrapy import Selector

from common.common_utils import read_txt_file, read_json_file, txt_file_iterator, output_to_file, strip_html_tags, json_output_to_csv
from common.file_utils import to_path, mkdir, file_exists, listdir
from meta import meta_config

reload(sys)
sys.setdefaultencoding('utf8')


PROVINCE_NAME_MAP = dict((x['shortName'], x) for x in read_json_file('./meta/provinces.json'))
BATCHES = read_json_file('./meta/batches.json')
BATCH_NAME_MAP = dict((x['name'], x) for x in BATCHES)
BATCH_CODE_MAP = dict((x['code'], x) for x in BATCHES)
CATEGORIES = read_json_file('./meta/categories.json')
CATEGORY_NAME_MAP = dict((x['name'], x) for x in CATEGORIES)
CATEGORY_KEY_MAP = dict((x['key'], x) for x in CATEGORIES)


def parse_validation_params(selector):
    return {
        '__VIEWSTATEGENERATOR': selector.css('#__VIEWSTATEGENERATOR').xpath('@value').extract_first(),
        '__VIEWSTATE': selector.css('#__VIEWSTATE').xpath('@value').extract_first(),
        '__EVENTVALIDATION': selector.css('#__EVENTVALIDATION').xpath('@value').extract_first()
    }


def parse_select_options(html_selector, css_selector_expression):
    select_dom = html_selector.css(css_selector_expression)
    name = select_dom.xpath('@name').extract_first()
    options = []
    for option_dom in select_dom.xpath('option'):
        value = option_dom.xpath('@value').extract_first()
        if not value:
            continue  # ignore empty value
        text = option_dom.xpath('text()').extract_first()
        options.append({
            'name': name,
            'value': value,
            'text': text
        })
    return options


def parse_select_province_param_combination_iter(html_selector, config):
    combinations = []
    for combination_config in config['param_combinations']:
        combinations.append(parse_select_options(html_selector, combination_config['css_selector']))
    return itertools.product(*combinations)


def parse_request_config(request_builder_config):
    headers = dict((arr[0].strip(' '), arr[1].strip(' '))
                   for arr in txt_file_iterator(request_builder_config['headers_file'],
                                                row_mapper=lambda x: x.replace('\n', '').split(':')))
    headers.update({'Cookie': read_txt_file(request_builder_config['cookies_file'])})
    return {
        'headers': headers
    }


def get_school_list(category, request_config, param_builder_config, output_config):
    headers = request_config['headers'].copy()
    home_path_html = do_request('http://www.shenqi66.cn/Index.aspx', headers=headers)
    if 'ddl_SchoolArea' not in home_path_html:
        print('not logged in!!!')   # TODO log instead
        exit(1)
    html_selector = Selector(text=home_path_html)
    validation_params = parse_validation_params(html_selector)
    param_combination_iter = parse_select_province_param_combination_iter(html_selector, param_builder_config)
    output_base_dir = output_config['base_dir']
    school_list_output_dir = to_path(output_base_dir, '%s_school_list' % category)  # TODO control wenke/like in param
    mkdir(school_list_output_dir)
    for combination in param_combination_iter:
        # if school data of current province is retrieved and stored in output file then pass
        province_info = [item for item in combination if item['name'] == 'ctl00$Rright$ddl_SchoolArea'][0]
        batch_info = [item for item in combination if item['name'] == 'ctl00$Rright$Times2'][0]
        file_name = '%s_%s.json' % (province_info['text'], batch_info['text'])
        school_list_result_file = to_path(school_list_output_dir, file_name)
        if file_exists(school_list_result_file):
            print('file exists: %s, skip' % file_name)  # TODO log instead
            continue
        # request for
        post_data = param_builder_config['default_params'].copy()
        post_data.update(dict((item['name'], item['value']) for item in combination))
        post_data.update(validation_params)
        selected_province_page_html = do_request('http://www.shenqi66.cn/Index.aspx', method='post',
                                                 data=post_data,
                                                 headers=headers)
        if 'Rright_ddl_School' not in selected_province_page_html:
            print('not logged in!!!')   # TODO log instead
            exit(1)
        html_selector = Selector(text=selected_province_page_html)
        schools_result = {
            'province_id': province_info['value'],
            'batch_id': batch_info['value'],
            'schools': [{'id': option['value'], 'name': option['text']}
                        for option in parse_select_options(html_selector, 'select#Rright_ddl_School')]
        }
        output_to_file(school_list_result_file, [json.dumps(schools_result, ensure_ascii=False)])
        print('retrieved schools, batch: %s, province: %s' % (batch_info['text'], province_info['text'])) # TODO log instead


def do_request(url, method='get', params=None, data=None, headers=None, cookies=None):
    print('Requesting ' + url)  # TODO
    return requests.request(method, url, params=params, data=data, headers=headers, cookies=cookies).content.decode('utf8', 'ignore')


def crawl_all_schools_in_list(category_key, request_config, output_config):
    output_base_dir = output_config['base_dir']
    school_list_dir = to_path(output_base_dir, '%s_school_list' % category_key)
    # init
    output_school_infos_dir = to_path(output_base_dir, 'school_infos')
    output_school_admission_history_list_dir = to_path(output_base_dir, 'school_history_list')
    output_major_admission_history_list_dir = to_path(output_base_dir, 'major_history_list')
    for p in [output_school_infos_dir, output_school_admission_history_list_dir, output_major_admission_history_list_dir]:
        mkdir(p)
    for file_name in listdir(school_list_dir):
        file_path = to_path(school_list_dir, file_name)
        province_name, batch_name = re.search('([^_]+)_([^.]+)', unicode(file_name)).groups()
        province_schools = read_json_file(file_path)
        print 'Start crawling province: %s, batch: %s' % (province_name, batch_name)
        for school_info in province_schools['schools']:
            school_id = school_info['id']
            school_name = school_info['name']
            print 'crawling %s of %s, %s' % (school_name, category_key, batch_name)
            crawl_school_all_info(category_key, batch_name, school_id, school_name, province_name, request_config, output_config)


# get report (System2.aspx)
# parse: school_info: {"tmp_id", "", "enroll_rule", "raw_intro"}, parse 2019 school_admission_history & major_admission_plan
# for year in [2016, 2013, 2010], get System2.aspx, parse school_admission_history
# for year in (2018 to 2010) get 往年专业录取详情 (detail.aspx) by tmp_id, parse each score distribution, then convert to major-scoreCount
# merge year's major_admission_plan & major_score_dist to major_admission_detail
# get 该校所有专业全国排名
def crawl_school_all_info(category_key, batch_name, school_id, school_name, province_name, request_config, output_config):
    def request_and_get_html(url, response_flag):
        html = do_request(url, method='post', headers=request_config['headers'])
        if response_flag not in html:
            raise Exception('not logged in!')
        return html

    category = CATEGORY_KEY_MAP[category_key]
    batch = BATCH_NAME_MAP[batch_name]
    output_base_dir = output_config['base_dir']
    output_school_infos_dir = to_path(output_base_dir, 'school_infos')
    output_school_admission_history_list_dir = to_path(output_base_dir, 'school_history_list')
    output_major_admission_history_list_dir = to_path(output_base_dir, 'major_history_list')
    outfile_school_info = to_path(output_school_infos_dir, '%s_%s_info.csv' % (school_id, school_name))
    outfile_school_history = to_path(output_school_admission_history_list_dir, '%s_%s_%s_%s_school.csv' % (school_id, school_name, category['id'], batch['id']))
    outfile_major_history = to_path(output_major_admission_history_list_dir, '%s_%s_%s_%s_major.csv' % (school_id, school_name, category['id'], batch['id']))
    if file_exists(outfile_school_history) or file_exists(outfile_major_history):
        print 'already exists: %s' % outfile_major_history
        return
    system2_2019_html_selector = Selector(text=request_and_get_html('http://www.shenqi66.cn/System2.aspx?Times=%s&years=2019&schoolcode=%s&SchoolName=' % (batch['code'], school_id), 'baogaotitle'))
    school_info = parse_school_info(province_name, school_id, school_name, system2_2019_html_selector)
    # school_admission_history_list
    school_admission_history_list = []
    year_2019_school_recruit_plan = parse_2019_school_recruit_plan(system2_2019_html_selector)
    for year_school_admission_history in [year_2019_school_recruit_plan] + parse_school_admission_history(system2_2019_html_selector):
        year_school_admission_history['school_id'] = school_id
        year_school_admission_history['category'] = category['id']
        year_school_admission_history['batch'] = batch['id']
        school_admission_history_list.append(year_school_admission_history)
    # major_admission_history_list
    major_admission_history_list = []
    year_2019_major_recruit_plans = parse_2019_major_recruit_plans(system2_2019_html_selector)
    all_year_major_detail_dict = {}
    for year in ['2018', '2017', '2016']:
        url = 'http://www.shenqi66.cn/details.aspx?subjectgroup=%s&times=%s&selectyears=%s&years=2019&userscores=%s&schoolid=%s' % (
            category['name'], batch_name, year, category['user_score'], school_info['inner_id'])
        year_major_detail_html_selector = Selector(text=request_and_get_html(url, u'录取详情'))
        all_year_major_detail_dict[year] = parse_major_enroll_detail(year_major_detail_html_selector)
    for major_admission_info in year_2019_major_recruit_plans + parse_major_admission_history(system2_2019_html_selector):
        major_admission_info['school_id'] = school_id
        major_admission_info['category'] = category['id']
        major_admission_info['batch'] = batch['id']
        year = major_admission_info['year']
        if all_year_major_detail_dict.get(year):
            major_admission_info['score_dist'] = all_year_major_detail_dict[year].get(major_admission_info['school_major'], '')
        major_admission_history_list.append(major_admission_info)
    if file_exists(outfile_school_info):
        print 'SKIPPED school info output: %s' % school_name
    else:
        json_output_to_csv(outfile_school_info, [school_info], columns=['id', 'name', 'province', 'raw_intro', 'attributes'])
        print 'output school info: %s' % school_name
    json_output_to_csv(outfile_school_history, school_admission_history_list,
                       columns=['school_id', 'category', 'batch', 'year', 'recruit_count', 'candidate_count', 'enroll_count',
                                'lowest_score', 'lowest_rank', 'average_score', 'average_rank'])
    print 'output school history: %s, size: %s' % (school_name, len(school_admission_history_list))
    json_output_to_csv(outfile_major_history, major_admission_history_list,
                       columns=['school_id', 'category', 'batch', 'school_major', 'year', 'recruit_count', 'reapply_count',
                                'lowest_score', 'lowest_rank', 'average_score', 'average_rank', 'score_dist'])
    print 'output major history: %s, size: %s' % (school_name, len(major_admission_history_list))


def parse_school_info(province_name, school_id, school_name, selector):
    links = selector.xpath('//div[@class="baogaotitle"]/following-sibling::a/@href').extract()
    inner_id = int(re.search(r'schoolid=(\d+)', links[0]).group(1))
    intro_spans = selector.xpath('//div[@class="baogaotitle"]/following-sibling::span[@style="font-size: 16px; font-weight: bold; padding-top: 20px; height:36px; line-height:36px;"]/span').extract()
    enroll_rule = strip_html_tags(intro_spans[0])
    raw_intro = strip_html_tags(intro_spans[1])
    ethnic_bonus = u'不认可加分' not in enroll_rule
    major_score_ladder = None
    if u'按专业级差安排' not in enroll_rule:
        match = re.search(u'专业志愿级差分[^\d]+([^。.]+)', raw_intro)
        if match:
            major_score_ladder = match.group(1)
    return {'id': school_id, 'inner_id': inner_id, 'name': school_name, 'province': PROVINCE_NAME_MAP[province_name]['id'], 'raw_intro': raw_intro,
            'attributes': json.dumps({'ethnic_bonus': ethnic_bonus, 'major_score_ladder': major_score_ladder})}


def parse_2019_school_recruit_plan(selector):
    recruit_count = selector.xpath(
        '//div[@class="baogaotitle"][1]/following-sibling::p[1]/span[@class="red"][last()]/text()').extract_first()
    return {'year': 2019, 'recruit_count': recruit_count} # other fields empty


def parse_2019_major_recruit_plans(selector):
    major_recruit_plans = []
    for tr_selector in selector.xpath('//table[@class="gpj font_style_1"][2]/tr')[1:]:
        td_selectors = tr_selector.xpath('td')
        if len(td_selectors) != 15:
            continue
        school_major = td_selectors[0].xpath('a/text()').extract_first()
        recruit_count = td_selectors[1].xpath('text()').extract_first()
        major_recruit_plans.append({'year': 2019, 'school_major': school_major, 'recruit_count': recruit_count})
    return major_recruit_plans


def extract_int(x):
    return re.search('\d+', x).group(0)


def parse_school_admission_history(selector):
    report_tables = selector.css('table.gpj.font_style_1')
    # parse school admission history
    school_admission_history_table_rows = report_tables[0].css('tr')[1:]  # skip thead
    school_admission_history_infos = []
    for tr_selector in school_admission_history_table_rows:
        td_selectors = tr_selector.css('td')
        recruit_count, candidate_count, enroll_count = re.search('(\d+)-(\d+)-(\d+)',
                                                                 strip_html_tags(td_selectors[8 if len(td_selectors) == 10 else 7].extract())).groups()
        school_history_row = {
            'year': extract_int(td_selectors[0].extract()),
            'lowest_score': extract_int(td_selectors[2].extract()),
            'lowest_rank': extract_int(td_selectors[3].extract()),
            'average_rank': extract_int(td_selectors[5].extract()),
            'average_score': extract_int(td_selectors[6].extract()),
            'recruit_count': recruit_count,
            'candidate_count': candidate_count,
            'enroll_count': enroll_count,
        }
        school_admission_history_infos.append(school_history_row)
    return school_admission_history_infos


def parse_major_admission_history(selector):
    major_admission_infos = []
    current_major = None
    for tr_selector in selector.xpath('//table[@class="gpj font_style_1"][2]/tr')[1:]:
        td_selectors = tr_selector.xpath('td')
        td_offset = 0
        if len(td_selectors) == 15:
            current_major = td_selectors[0].xpath('a/text()').extract_first()
            td_offset = 2
        counts_str = strip_html_tags(td_selectors[1 + td_offset].extract())
        if counts_str == '---':
            continue
        year = strip_html_tags(td_selectors[td_offset].xpath('text()').extract_first())
        arr = counts_str.split('-')
        recruit_count = arr[0]
        reapply_count = arr[1]
        lowest_score = td_selectors[td_offset + 3].xpath('text()').extract_first()
        lowest_rank = td_selectors[td_offset + 4].xpath('text()').extract_first()
        average_rank = td_selectors[td_offset + 6].xpath('text()').extract_first()
        average_score = td_selectors[td_offset + 7].xpath('text()').extract_first()
        major_admission_infos.append({
            'school_major': current_major,
            'year': year,
            'recruit_count': recruit_count,
            'reapply_count': reapply_count,
            'lowest_score': lowest_score,
            'lowest_rank': lowest_rank,
            'average_rank': average_rank,
            'average_score': average_score
        })
    return major_admission_infos


def parse_major_enroll_detail(selector):
    major_details = {}
    for row_selector in selector.css('form#form1 div table.gpj.font_style_1 tr')[1:]:
        cols = row_selector.xpath('td/text()').extract()
        score = int(cols[0])
        score_enroll_count = int(cols[1])
        for major_dist in cols[3].split(u'人,'):
            major, enroll_count = re.search(r'([^\d]+)(\d+)', major_dist).groups()
            major_score_dist = major_details.get(major)
            if not major_score_dist:
                major_score_dist = []
                major_details[major] = major_score_dist
            major_score_dist.append({'score': score, 'enroll_count': score_enroll_count})
    result_dict = {}
    for (major, score_dist) in major_details.items():
        score_dist.sort(lambda o1, o2: o2['score'] - o1['score'])
        score_dist_str = ','.join([('%s:%s' % (x['score'], x['enroll_count'])) for x in score_dist])
        result_dict[major] = score_dist_str
    return result_dict


def test_crawl_school_all_info():
    res = crawl_school_all_info('like', u'本科第一批', '10593', u'广西大学' ,u'广西', parse_request_config(meta_config.REQUEST_BUILDER),
                                meta_config.OUTPUT)
    print res


def test_crawl_all_schools_in_list():
    crawl_all_schools_in_list('like', parse_request_config(meta_config.REQUEST_BUILDER), meta_config.OUTPUT)


def main():
    category = sys.argv[1]
    if category != 'like' and category != 'wenke':
        raise Exception('illegal category')
    request_config = parse_request_config(meta_config.REQUEST_BUILDER)
    output_config = meta_config.OUTPUT
    crawl_all_schools_in_list(category, request_config, output_config)


if __name__ == '__main__':
    main()
