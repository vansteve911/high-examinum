# -*- coding: utf-8 -*-
from common.common_utils import strip_html_tags


REQUEST_BUILDER = {
    'cookies_file': 'meta/normal_li_cookies.txt',
    # 'cookies_file': 'meta/priveledge_wen_cookies.txt',
    # 'cookies_file': 'meta/normal_wen_cookies.txt',
    # 'cookies_file': 'meta/priveledge_wen_cookies.txt',
    'headers_file': 'meta/common_headers.txt'
}

SELECT_PROVINCE_PARAM_BUILDER = {
    'param_combinations': [
        {
            'name': 'ctl00$Rright$Times2',
            'type': 'from_select_dom',
            'css_selector': 'select#Rright_Times2'
        },
        {
            'name': 'ctl00$Rright$ddl_SchoolArea',
            'type': 'from_select_dom',
            'css_selector': 'select#Rright_ddl_SchoolArea'
        }
    ],
    'headers': {
        'Referer': 'http://www.shenqi66.cn/Index.aspx'
    },
    'default_params': {
        '__EVENTTARGET': 'ctl00$Rright$ddl_SchoolArea',
        '__EVENTARGUMENT': '',
        '__LASTFOCUS': '',
        'ctl00$Rright$year1': '2019',
        'ctl00$Rright$Times1': '9',
        'ctl00$Rright$year2': '2019',
        'ctl00$Rright$Times2': '9',
        'ctl00$Rright$ddl_SchoolArea': '10',
        'ctl00$Rright$ddl_School': '',
        'ctl00$Rright$txtSchoolName': '',
        'ctl00$Rright$year3': '2019',
        'ctl00$Rright$Times3': '9',
        'ctl00$Rright$txt_profession3': '',
        'ctl00$Rright$year4': '2019',
        'ctl00$Rright$txtStudentName': '',
        'ctl00$Rright$txtMobileTel': '',
        'ctl00$Rright$ddlCoachType': '1'
    }

}

PARSE_CONFIG = {
    'school_major_analyse': {
        'school_admission_history': {
            'type': 'table',
            'column_metas': [
                {
                    'index': 0,
                    'key': 'year',
                    # 'title': '年份'
                },
                {
                    'index': 2,
                    'key': 'lowest_score',
                    # 'title': '录取最低分'
                },
                {
                    'index': 3,
                    'key': 'lowest_rank',
                    # 'title': '最低分名次'
                },
                {
                    'index': 5,
                    'key': 'average_rank',
                    # 'title': '平均分平次'
                },
                {
                    'index': 6,
                    'key': 'average_score',
                    # 'title': '平均分'
                },
                {
                    'index_func': lambda td_selectors: 8 if len(td_selectors) == 10 else 7,
                    'key': 'recruit_count',
                    # 'title': '招生人数',
                    'handler': lambda td_sel: strip_html_tags(td_sel.extract().replace('\r', '')).strip(' ').split('-')[0]
                },
                {
                    'index_func': lambda td_selectors: 8 if len(td_selectors) == 10 else 7,
                    'key': 'candidate_count',
                    # 'title': '投档人数',
                    'handler': lambda td_sel: strip_html_tags(td_sel.extract().replace('\r', '')).strip(' ').split('-')[1]
                },
                {
                    'index_func': lambda td_selectors: 8 if len(td_selectors) == 10 else 7,
                    'key': 'enroll_count',
                    # 'title': '最低分名次',
                    'handler': lambda td_sel: strip_html_tags(td_sel.extract().replace('\r', '')).strip(' ').split('-')[2]
                }
            ]
        },
        'major_admission_history': {
            'type': 'table',
            'column_metas': [
                {
                    'index': 0,
                    'key': 'year',
                    # 'title': '年份'
                },
                {
                    'index': 1,
                    'key': 'recruit_count',
                    # 'title': '招生人数',
                    'handler': lambda td_sel: strip_html_tags(td_sel.extract().replace('\r', '')).strip(' ').split('-')[0]
                },
                {
                    'index': 1,
                    'key': 'apply_count',
                    # 'title': '征集数',
                    'handler': lambda td_sel: strip_html_tags(td_sel.extract().replace('\r', '')).strip(' ').split('-')[1]
                },
                {
                    'index': 3,
                    'key': 'lowest_score',
                    # 'title': '录取最低分'
                },
                {
                    'index': 4,
                    'key': 'lowest_rank',
                    # 'title': '最低分名次'
                },
                {
                    'index': 6,
                    'key': 'average_rank',
                    # 'title': '平均分名次'
                },
                {
                    'index': 7,
                    'key': 'average_score',
                    # 'title': '平均分名次'
                }
            ]
        }
    }
}

OUTPUT = {
    'base_dir': '../data/output'
}
