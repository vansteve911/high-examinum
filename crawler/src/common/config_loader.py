#! /usr/bin/python
# -*- coding: UTF-8 -*-
from common.common_utils import read_json_file
from common.file_utils import file_exists


def __load_config():
    conf = read_json_file('../conf/config_common.json')
    platform_config_file = '../conf/config_platform.json'
    if file_exists(platform_config_file):
        conf.update(read_json_file(platform_config_file))
    conf.update(read_json_file('../conf/config_env.json'))
    return conf


global_config = __load_config()