import logging
import logging.handlers

from common.config_loader import global_config
from common.file_utils import *


LOG_LEVELS = {
    'DEBUG': logging.DEBUG,
    'INFO': logging.INFO,
    'WARN': logging.WARN,
    'ERROR': logging.ERROR
}


def default_logger(name='root', level='INFO'):
    path = to_path(global_config['work_base_dir'], global_config['logs_dir'])
    return logger_factory(name, path, level)


def logger_factory(name, path, level):
    logger = logging.getLogger(name)
    if not logger.handlers:
        # init the logger
        if not file_exists(path):
            mkdir(path)
        log_file = to_path(path, name, file_postfix='.log')
        handler = logging.handlers.TimedRotatingFileHandler(log_file, when='D', interval=1, backupCount=300)
        formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(threadName)s [%(filename)s:%(lineno)d] %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        logger.setLevel(LOG_LEVELS[level])
    return logger
