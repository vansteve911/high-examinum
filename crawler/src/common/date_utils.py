#! /usr/bin/python
# -*- coding: UTF-8 -*-

from datetime import datetime


def time_delta(t1_str, t2_str, format='%H:%M:%S', timeunit='seconds'):
    t1 = datetime.strptime(t1_str, format)
    t2 = datetime.strptime(t2_str, format)
    (delta_val, sign) = (t1 - t2, 1) if (t1 > t2) else (t2 - t1, -1)
    if timeunit == 'seconds':
        return sign * delta_val.seconds
    elif timeunit == 'microseconds':
        return sign * delta_val.microseconds
    elif timeunit == 'days':
        return sign * delta_val.days
    return None
