# -*- coding: utf-8 -*-
import xlsxwriter
import traceback
import sys

reload(sys)
sys.setdefaultencoding('utf8')


def write_to_excel(filename, sheets_data, is_dict_row=False):
    workbook = None
    try:
        workbook = xlsxwriter.Workbook(filename)
        # print(filename)
        for sheet_data in sheets_data:
            worksheet = workbook.add_worksheet(sheet_data['name'])
            i = 0
            if is_dict_row:
                # write table head
                key_name_pairs = sheet_data['key_name_pairs']
                j = 0
                for (key, name) in key_name_pairs:
                    # worksheet.write(i, j, col)
                    worksheet.write(i, j, name.decode('utf-8') if isinstance(name, str) else str(name))
                    j += 1
                i = 1
                for row_dict in sheet_data['row_dicts']:
                    j = 0
                    for (key, name) in key_name_pairs:
                        col = row_dict[key]
                        worksheet.write(i, j, col.encode('utf8') if isinstance(col, unicode) else col)
                        # worksheet.write(i, j, col)
                        j += 1
                    i += 1
            else:
                table_head = sheet_data.get('head')
                if table_head:
                    j = 0
                    for col in table_head:
                        # worksheet.write(i, j, col)
                        worksheet.write(i, j, col.decode('utf-8') if isinstance(col, str) else str(col))
                        j += 1
                    i = 1
                # write table body
                for row in sheet_data['body']:
                    j = 0
                    for col in row:
                        worksheet.write(i, j, col.encode('utf8') if isinstance(col, unicode) else col)
                        # worksheet.write(i, j, col)
                        j += 1
                    i += 1
    except Exception:
        print('write to excel failed!')
        traceback.print_exc()
    finally:
        if workbook:
            workbook.close()
