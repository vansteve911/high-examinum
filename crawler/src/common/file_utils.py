#! /usr/bin/python
# -*- coding: UTF-8 -*-

import os


def to_path(path, *paths, **kwargs):
    path = os.path.join(path, *paths)
    file_postfix = kwargs.get('file_postfix')
    if file_postfix:
        path += file_postfix
    return path


def mkdir(path):
    os.popen('mkdir -p %s' % path).read()


def rm(path):
    os.popen('rm -rf %s' % path).read()


def cp(path, dest_path, is_dir=False):
    cmd = 'cp -r' if is_dir else 'cp'
    os.popen(' '.join([cmd, path, dest_path])).read()


def listdir(path, filename_mapper=lambda x: x):
    return filter(lambda x: x is not None, (filename_mapper(filename) for filename in os.listdir(path)))


def touch(path):
    os.popen('touch %s' % path).read()


def file_exists(path):
    return os.path.exists(path)


def download(url, local_path):
    cmd = 'wget -q -P %s %s' % (local_path, url)
    os.popen(cmd).read()


def unzip(zip_file, extract_to):
    zip_file_name = os.path.split(zip_file)[-1]
    copied_zip_file = os.path.join(extract_to, zip_file_name)
    cmd = ' && '.join(['cp %s %s' % (zip_file, copied_zip_file),
                       'cd %s' % extract_to,
                       'tar xzf %s' % zip_file_name,
                       'rm -rf %s' % copied_zip_file])
    os.popen(cmd).read()


def zip_files(work_dir, zip_file, src_file_pattern):
    cmd = ' && '.join(['cd %s' % work_dir,
                       'tar czf %s %s' % (zip_file, src_file_pattern),
                       'cd -'])
    os.popen(cmd).read()





