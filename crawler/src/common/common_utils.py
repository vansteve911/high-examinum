#! /usr/bin/python
# -*- coding: UTF-8 -*-
import json
import hashlib
import re


HTML_TAG_PATTERN = re.compile(r'<\\?[^>]+>')


def txt_file_iterator(filename, row_mapper=lambda x: x, row_filter=None):
    if not callable(row_mapper):
        raise Exception('row mapper is not function! %s' % row_mapper)
    with open(filename) as f:
        while True:
            line = f.readline()
            if not line:
                break
            row_data = row_mapper(line)
            if not row_data or (callable(row_filter) and not row_filter(row_data)):
                continue
            yield row_data


def read_txt_file(filename):
    text = ''
    for line in txt_file_iterator(filename, row_mapper=lambda x: x.replace('\n', '')):
        text += line
    return text


def read_json_file(filename):
    json_str = ''
    for line in txt_file_iterator(filename, row_mapper=lambda x: x.replace('\n', '')):
        json_str += line
    return json.loads(json_str)


def output_to_csv(outfilename, generator, separator='\t'):
    with open(outfilename, 'a') as f:
        for row in generator:
            f.write(to_tab_row(row, separator) + '\n')


def json_output_to_csv(outfilename, generator, columns, separator='\t', mode='w'):
    with open(outfilename, mode) as f:
        for json_obj in generator:
            row = [json_obj.get(k, '') for k in columns]
            f.write(to_tab_row(row, separator) + '\n')


def output_to_file(outfilename, generator):
    with open(outfilename, 'a') as f:
        for row in generator:
            f.write(row + '\n')


def to_tab_row(row, separator='\t'):
    return separator.join(list((x.encode('utf8') if type(x) is unicode else str(x)) for x in row))


def to_safe_quoted_str(input):
    return '"%s"' % input.replace('"', '\"')


def md5_digest(input):
    md5 = hashlib.md5()
    md5.update(input)
    return md5.hexdigest()


def await_threads_complete(threads):
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()


def strip_html_tags(input_str):
    return HTML_TAG_PATTERN.sub('', input_str)

