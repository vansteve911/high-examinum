# -*- coding: utf-8 -*-

import sys
import json
import itertools
import random
import time
import re

import requests
from scrapy import Selector

from common.common_utils import read_txt_file, read_json_file, txt_file_iterator, output_to_file, strip_html_tags
from common.file_utils import to_path, mkdir, file_exists, listdir
from meta import meta_config

reload(sys)
sys.setdefaultencoding('utf8')


def parse_validation_params(selector):
    return {
        '__VIEWSTATEGENERATOR': selector.css('#__VIEWSTATEGENERATOR').xpath('@value').extract_first(),
        '__VIEWSTATE': selector.css('#__VIEWSTATE').xpath('@value').extract_first(),
        '__EVENTVALIDATION': selector.css('#__EVENTVALIDATION').xpath('@value').extract_first()
    }


def parse_select_options(html_selector, css_selector_expression):
    select_dom = html_selector.css(css_selector_expression)
    name = select_dom.xpath('@name').extract_first()
    options = []
    for option_dom in select_dom.xpath('option'):
        value = option_dom.xpath('@value').extract_first()
        if not value:
            continue  # ignore empty value
        text = option_dom.xpath('text()').extract_first()
        options.append({
            'name': name,
            'value': value,
            'text': text
        })
    return options


def parse_select_province_param_combination_iter(html_selector, config):
    combinations = []
    for combination_config in config['param_combinations']:
        combinations.append(parse_select_options(html_selector, combination_config['css_selector']))
    return itertools.product(*combinations)


def parse_request_config(request_builder_config):
    headers = dict((arr[0].strip(' '), arr[1].strip(' '))
                   for arr in txt_file_iterator(request_builder_config['headers_file'],
                                                row_mapper=lambda x: x.replace('\n', '').split(':')))
    headers.update({'Cookie': read_txt_file(request_builder_config['cookies_file'])})
    return {
        'headers': headers
    }


def get_school_list(category, request_config, param_builder_config, output_config):
    headers = request_config['headers'].copy()
    # headers.update({'Referer': 'http://www.shenqi66.cn/Login.aspx'}) # TODO fix set referer bug
    home_path_html = do_request('http://www.shenqi66.cn/Index.aspx', headers=headers)
    # cookies = request_config['cookies'],
    if 'ddl_SchoolArea' not in home_path_html:
        print('not logged in!!!')   # TODO log instead
        exit(1)
    html_selector = Selector(text=home_path_html)
    validation_params = parse_validation_params(html_selector)
    param_combination_iter = parse_select_province_param_combination_iter(html_selector, param_builder_config)
    # headers.update({'Referer': 'http://www.shenqi66.cn/Index.aspx'})  # TODO fix set referer bug
    # init output dir
    output_base_dir = output_config['base_dir']
    school_list_output_dir = to_path(output_base_dir, '%s_school_list' % category)  # TODO control wenke/like in param
    mkdir(school_list_output_dir)
    for combination in param_combination_iter:
        # if school data of current province is retrieved and stored in output file then pass
        province_info = [item for item in combination if item['name'] == 'ctl00$Rright$ddl_SchoolArea'][0]
        batch_info = [item for item in combination if item['name'] == 'ctl00$Rright$Times2'][0]
        file_name = '%s_%s.json' % (province_info['text'], batch_info['text'])
        school_list_result_file = to_path(school_list_output_dir, file_name)
        if file_exists(school_list_result_file):
            print('file exists: %s, skip' % file_name)  # TODO log instead
            continue
        # request for
        post_data = param_builder_config['default_params'].copy()
        post_data.update(dict((item['name'], item['value']) for item in combination))
        post_data.update(validation_params)
        selected_province_page_html = do_request('http://www.shenqi66.cn/Index.aspx', method='post',
                                                 data=post_data,
                                                 headers=headers)
        if 'Rright_ddl_School' not in selected_province_page_html:
            print('not logged in!!!')   # TODO log instead
            exit(1)
        html_selector = Selector(text=selected_province_page_html)
        schools_result = {
            'province_id': province_info['value'],
            'batch_id': batch_info['value'],
            'schools': [{'id': option['value'], 'name': option['text']}
                        for option in parse_select_options(html_selector, 'select#Rright_ddl_School')]
        }
        output_to_file(school_list_result_file, [json.dumps(schools_result, ensure_ascii=False)])
        print('retrieved schools, batch: %s, province: %s' % (batch_info['text'], province_info['text'])) # TODO log instead


def do_request(url, method='get', params=None, data=None, headers=None, cookies=None):
    # sleep_millis = random.randint(100, 500)
    # print('sleep %s millis and then %s request %s' % (sleep_millis, method, url))
    # time.sleep(1.0 / sleep_millis)
    return requests.request(method, url, params=params, data=data, headers=headers, cookies=cookies).content.decode('utf8', 'ignore')


def crawl_school_list(category):
    request_config = parse_request_config(meta_config.REQUEST_BUILDER)
    get_school_list(category, request_config, meta_config.SELECT_PROVINCE_PARAM_BUILDER, meta_config.OUTPUT)


# crawl school major info
def parse_admission_detail(html_selector, parse_config):
    report_tables = html_selector.css('table.gpj.font_style_1')
    # parse school admission history
    school_admission_history_table_rows = report_tables[0].css('tr')[1:]  # skip thead
    school_admission_history_infos = []
    for tr_selector in school_admission_history_table_rows:
        td_selectors = tr_selector.css('td')
        school_history_row = {}
        for column_meta in parse_config['school_admission_history']['column_metas']:
            handler = column_meta.get('handler', lambda td_sel: td_sel.xpath('text()').extract_first().strip()
                                      .replace('\n', ''))
            index = column_meta['index'] if column_meta.get('index') is not None else column_meta['index_func'](
                td_selectors)
            school_history_row.update({
                column_meta['key']: handler(td_selectors[index])
                # 'title': column_meta['title'],

            })
        school_admission_history_infos.append(school_history_row)
    # parse current year recruit plan count  # TODO to be refined
    school_recruit_plan_desc = strip_html_tags(html_selector.css('div.baogaotitle')[0].xpath('following-sibling::p')[0]
                                               .extract())
    recruit_plan_total_count = re.findall(u'\d+', school_recruit_plan_desc.encode('utf8'))[-1]
    # parse school id & code
    report_two_urls = html_selector.css('div.baogaotitle')[1].xpath('following-sibling::a/@href')
    school_id = re.search(r'schoolid=(\d+)', report_two_urls[0].extract()).group(1)
    school_code = re.search(r'schoolcode=(\d+)', report_two_urls[1].extract()).group(1)
    # parse major admission history
    major_admission_detail_rows = report_tables[1].css('tr')[1:]  # skip thead
    major_admission_history_infos = []
    major_recruit_plan_infos = []
    major_history_row = None
    for tr_selector in major_admission_detail_rows:
        td_selectors = tr_selector.css('td')
        skip_column_count = 0
        if len(td_selectors) == 15:
            if major_history_row is not None:
                major_admission_history_infos.append(major_history_row)
            # major_name = strip_html_tags(td_selectors[0].css('a')[0].xpath('text()').extract_first()).replace('\r', '').strip(' ').replace('\n', '')
            major_name = strip_html_tags(td_selectors[0].css('a')[0].xpath('text()').extract_first())# .replace('\r', '').strip(' ').replace('\n', '')
            major_history_row = {
                'name': major_name
            }
            major_recruit_plan_infos.append({
                'name': major_name,
                'current_plan_recruit_count': strip_html_tags(td_selectors[1].extract()).replace('\r', '').strip(' ')
            })
            skip_column_count = 2
        for column_meta in parse_config['major_admission_history']['column_metas']:
            handler = column_meta.get('handler', lambda td_sel: td_sel.xpath('text()').extract_first().strip()
                                      .replace('\n', ''))
            index = column_meta['index'] if column_meta.get('index') is not None else column_meta['index_func'](
                td_selectors)
            major_history_row.update({
                column_meta['key']: handler(td_selectors[index + skip_column_count])
            })
        # major_admission_history_infos.append(major_history_row)
    if major_history_row:
        major_admission_history_infos.append(major_history_row)
    return {
        'school_id': school_id,
        'school_code': school_code,
        'school_admission_history': school_admission_history_infos,
        'school_recruit_plan': {
            'recruit_count': recruit_plan_total_count,
        },
        'major_admission_history': major_admission_history_infos,
        'major_recruit_plan': major_recruit_plan_infos
    }


def parse_enroll_detail(html_selector):
    enroll_detail = []
    for row_selector in html_selector.css('form#form1 div table.gpj.font_style_1 tr')[1:]:
        cols = row_selector.css('td').xpath('text()').extract()
        enroll_detail.append({
            'score': int(cols[0]),
            'score_enroll_count': int(cols[1]),
            'ranking': int(cols[2]),
            'major_distribution': parse_major_distribution(cols[3])
        })
    return enroll_detail


def parse_major_distribution(content):
    details = []
    for major_info in content.split('人,'):
        major, enroll_count = re.search(r'([^\d]+)(\d+)', major_info).groups()
        details.append({
            'major': major,
            'count': int(enroll_count)
        })
    return details

BATCH_NAMES = {
    '9': '本科第一批',
    '12': '本科第二批',
    '14': '本科第一批预科',
    '15': '本科第二批预科B'
}


def crawl_enroll_history(category, request_config, school_id, year, batch_id):
    category_name, user_score = ('文史类', '557') if (category == 'wenke') else ('理工类', '605')
    # url = '/details.aspx?subjectgroup=文史类&times=本科第一批&years=2019&userscores=542&schoolid=1592' % ()
    url = 'http://www.shenqi66.cn/details.aspx?subjectgroup=%s&times=%s&selectyears=%s&years=2019&userscores=%s&schoolid=%s' % (
        category_name, BATCH_NAMES[batch_id], year, user_score, school_id)
    print('crawling enroll history, school_id: %s, year: %s' % (school_id, year))
    enroll_history_html = do_request(url, headers=request_config['headers'])
    if '录取详情' not in enroll_history_html:
        raise Exception('fetch failed, school_id: %s, year: %s' % (school_id, year))
    return {
        'year': year,
        'enroll_detail': parse_enroll_detail(Selector(text=enroll_history_html))
    }


def school_list_iterator(school_list_dir):
    for file_name in listdir(school_list_dir):
        file_path = to_path(school_list_dir, file_name)
        province_info = read_json_file(file_path)
        group_name = file_name.replace('.json', '')
        province_id = province_info['province_id']
        batch_id = province_info['batch_id']
        for school_info in province_info['schools']:
            yield {
                'batch': batch_id,
                'province': province_id,
                'id': school_info['id'],
                'name': school_info['name'],
                'group': group_name
            }


def crawl_school_major_infos(category, request_config, parse_config, output_config):
    output_base_dir = output_config['base_dir']
    school_list_output_dir = to_path(output_base_dir, '%s_school_list' % category)
    # init output dir
    admission_detail_output_dir = to_path(output_base_dir, '%s_admission_detail' % category)
    mkdir(admission_detail_output_dir)
    for school_info in school_list_iterator(school_list_output_dir):
        file_name = '%s_%s_%s.json' % (school_info['id'], school_info['name'], school_info['group'])
        admission_detail_file = to_path(admission_detail_output_dir, file_name)
        if file_exists(admission_detail_file):
            print('file_exists: %s, skip' % file_name)
            continue
        print('crawling school: %s' % school_info['name'])
        url = 'http://www.shenqi66.cn/System2.aspx?Times=%s&years=2019&schoolcode=%s&SchoolName=' % (school_info['batch'],
                                                                                                     school_info['id'])
        analyse_path_html = do_request(url, method='post', headers=request_config['headers'])
        if 'baogaotitle' not in analyse_path_html:
            raise Exception('not logged in!')
        admission_detail_result = parse_admission_detail(Selector(text=analyse_path_html), parse_config)
        # crawl enroll history by school id
        enroll_history = []
        for year in ['2018', '2017', '2016']:
            enroll_detail = crawl_enroll_history(category, request_config, admission_detail_result['school_id'], year, school_info['batch'])
            enroll_history.append(enroll_detail)
        admission_detail_result.update({
            'enroll_history': enroll_history
        })
        output_to_file(admission_detail_file, [json.dumps(admission_detail_result, ensure_ascii=False)])
        print('success parsed admission result of school %s' % school_info['name'])


def parse_major_ranking_total_page(html_selector):
    page_info = html_selector.css('div#ContentPlaceHolder1_PageInfo span.pager a')[0].xpath('text()').extract_first()
    return int(re.search(r'/(\d+)', page_info).group(1))


def parse_major_ranking(html_selector):
    major_infos = []
    for row_selector in html_selector.css('table.auto-style1')[0].css('tr')[1:]:
        cols = row_selector.css('td').xpath('text()').extract()
        rank_arr = cols[2].split('/')
        match = re.search(r'(\d+):(.+)', rank_arr[0].replace('名', ':'))
        rank, level = (match.group(1), match.group(2)) if match else ('', rank_arr[0])
        major_infos.append({
            'id': cols[0],
            'name': cols[1],
            'rank': rank,
            'level': level,
            'total_ranks': rank_arr[1] if len(rank_arr) == 2 else ''
        })
    return major_infos


def crawl_school_major_ranking(school_code, request_config):
    url = 'http://www.shenqi66.cn/Professionalranking.aspx?PageSize=20&SchoolCode=%s' % school_code
    print('crawling major ranking, school_code: %s, page: 1' % school_code)
    first_page_html = do_request(url, headers=request_config['headers'])
    if '专业全国排名' not in first_page_html:
        raise Exception('not logged in!')
    html_selector = Selector(text=first_page_html)
    total_page = parse_major_ranking_total_page(html_selector)
    print('school_code: %s, total page: %s' % (school_code, total_page))
    major_rankings = parse_major_ranking(html_selector)
    for page_no in range(total_page + 1)[2:]:
        url = 'http://www.shenqi66.cn/Professionalranking.aspx?PageSize=20&SchoolCode=%s&PageNo=%s' % (school_code, page_no)
        print('crawling major ranking, school_code: %s, page: %s' % (school_code, page_no))
        page_html = do_request(url, headers=request_config['headers'])
        if '专业全国排名' not in first_page_html:
            raise Exception('not logged in!')
        for rank_info in parse_major_ranking(Selector(text=page_html)):
            major_rankings.append(rank_info)
    return major_rankings


def crawl_all_school_major_ranking(category, request_config, output_config):
    output_base_dir = output_config['base_dir']
    school_list_output_dir = to_path(output_base_dir, '%s_school_list' % category)
    # init output dir
    major_ranking_output_dir = to_path(output_base_dir, '%s_major_ranking' % category)
    mkdir(major_ranking_output_dir)
    for school_info in school_list_iterator(school_list_output_dir):
        file_name = '%s_%s_majors.json' % (school_info['id'], school_info['name'])
        major_ranking_file = to_path(major_ranking_output_dir, file_name)
        if file_exists(major_ranking_file):
            print('file_exists: %s, skip' % file_name)
            continue
        print('crawling school major ranking: %s' % school_info['name'])
        major_ranking = crawl_school_major_ranking(school_info['id'], request_config)
        result = {
            'school_code': school_info['id'],
            'ranking': major_ranking
        }
        output_to_file(major_ranking_file, [json.dumps(result, ensure_ascii=False)])

# unit tests
def test_parse_admission_detail():
    major_analyse_html = read_txt_file('../example/school_major_analyse.html')
    school_major_info = parse_admission_detail(Selector(text=major_analyse_html),
                                               meta_config.PARSE_CONFIG['school_major_analyse'])
    print(json.dumps(school_major_info, ensure_ascii=False))


def test_parse_enroll_history():
    enroll_history_html = read_txt_file('../example/school_enroll_history.html')
    school_enroll_history = parse_enroll_detail(Selector(text=enroll_history_html))
    print(json.dumps(school_enroll_history, ensure_ascii=False))


def test_crawl_enroll_history():
    school_enroll_history = crawl_enroll_history('like', parse_request_config(meta_config.REQUEST_BUILDER), '383', '2018', '9')
    print(json.dumps(school_enroll_history, ensure_ascii=False))


def test_crawl_major_rankings():
    major_ranking = crawl_school_major_ranking('10285', parse_request_config(meta_config.REQUEST_BUILDER))
    print(json.dumps(major_ranking, ensure_ascii=False))


# main


def main():
    # unit test entry
    # test_parse_admission_detail()
    # test_crawl_enroll_history()
    # test_crawl_major_rankings()
    # main entry
    run_mode = sys.argv[1]
    category = sys.argv[2]
    if run_mode == 'school_list':
        crawl_school_list(category)
    elif run_mode == 'admission_detail':
        crawl_school_major_infos(category, parse_request_config(meta_config.REQUEST_BUILDER),
                                 meta_config.PARSE_CONFIG['school_major_analyse'], meta_config.OUTPUT)
    elif run_mode == 'major_ranking':
        crawl_all_school_major_ranking(category, parse_request_config(meta_config.REQUEST_BUILDER), meta_config.OUTPUT)
    # output_base_dir = meta_config.OUTPUT['base_dir']
    # school_list_output_dir = to_path(output_base_dir, 'school_list')
    # for s in school_list_iterator(school_list_output_dir):
    #     print(s)
    #     break
    # crawl_school_major_infos(parse_request_config(meta_config.REQUEST_BUILDER),
    #                          meta_config.PARSE_CONFIG['school_major_analyse'], meta_config.OUTPUT)


if __name__ == '__main__':
    main()
