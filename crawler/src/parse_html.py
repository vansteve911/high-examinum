# -*- coding: utf-8 -*-

import sys
import os
# import json
import re

from scrapy import Selector
from common.common_utils import read_txt_file, output_to_csv, strip_html_tags


reload(sys)
sys.setdefaultencoding('utf8')

CATEGORY_MAP = {0: 'like', 1: 'wenke'}
BATCH_MAP = {0: '9', 1: '12', 2: '14', 3: '15'}
INT_PATTERN = re.compile(r'\d+')
MAJOR_ENROLL_COUNT_PATTERN = re.compile(ur'(\D+)(\d+)人')

SCHOOL_ADMISSION_HISTORY_COLUMNS = ['school_id', 'category', 'batch', 'year', 'recruit_count', 'candidate_count', 'enroll_count', 'lowest_score', 'lowest_rank', 'average_score', 'average_rank']
MAJOR_ADMISSION_HISTORY_COLUMNS = ['school_id', 'category', 'batch', 'school_major', 'year', 'recruit_count', 'reapply_count', 'lowest_score', 'lowest_rank', 'average_score', 'average_rank', 'score_dist']

BASE_PATH = '../data/output'
SCHOOL_INFOS_OUTPUT_FILE = BASE_PATH + '/school_admission_history.tsv'
MAJOR_INFOS_OUTPUT_FILE = BASE_PATH + '/major_admission_history.tsv'


def extract_int(input_str, default=-1):
    match = INT_PATTERN.search(input_str)
    num = match and int(match.group(0))
    return num if num is not None else default


def parse_or_default(input_str, f, default):
    try:
        return f(input_str)
    except:
        return default


def int_or_default(input_str, default=-1):
    return parse_or_default(input_str, int, default)


def load_sys2_data(category, batch, start_year, school_id):
    # TODO parse school_id, category, batch from filename
    filename = BASE_PATH + '/%s/%s/sys2/sys2_year_%s_school_%s.html' % (CATEGORY_MAP[category], BATCH_MAP[batch], start_year, school_id)
    html_sel = Selector(text=read_txt_file(filename))
    # parse school_admission_history
    school_admission_history_list = []
    table_sels = html_sel.xpath('/html/body/form[@id="form1"]/div/table[@class="gpj font_style_1"]')
    for school_history_table_tr_sel in table_sels[0].xpath('tr')[1:]:
        td_texts = [strip_html_tags(raw_td_html) for raw_td_html in school_history_table_tr_sel.xpath('td').extract()]
        school_history_info = {'school_id': school_id, 'category': category, 'batch': batch, 'year': extract_int(td_texts[0]),
                               'lowest_score': extract_int(td_texts[2]), 'lowest_rank': extract_int(td_texts[3]),
                               'average_rank': extract_int(td_texts[5]), 'average_score': td_texts[6]}
        counts = td_texts[8 if len(td_texts) == 10 else 7].split('-')
        school_history_info['recruit_count'] = extract_int(counts[0])
        school_history_info['candidate_count'] = extract_int(counts[1])
        school_history_info['enroll_count'] = extract_int(counts[2])
        school_admission_history_list.append(school_history_info)
    # parse major_admission_history
    major_admission_history_list = []
    current_major_name = None
    for major_history_table_tr_sel in table_sels[1].xpath('tr')[1:]:
        td_texts = [strip_html_tags(raw_td_html) for raw_td_html in major_history_table_tr_sel.xpath('td').extract()]
        major_history_info = {
            'school_id': school_id,
            'category': category,
            'batch': batch,
        }
        td_offset = 2 if len(td_texts) > 10 else 0
        if td_offset == 2:
            current_major_name = re.search(r'\S+', td_texts[0]).group(0)
            if start_year == 2019:  # only append to year 2019
                latest_year_major_info = {
                    'school_id': school_id,
                    'school_major': current_major_name,
                    'category': category,
                    'batch': batch,
                    'year': start_year,
                    'recruit_count': extract_int(td_texts[1]),
                    'reapply_count': -1,
                    'lowest_score': -1,
                    'lowest_rank': -1,
                    'average_rank': -1,
                    'average_score': -1,
                    'score_dist': ''  # TODO tmp no content
                }
                major_admission_history_list.append(latest_year_major_info)
        major_history_info['school_major'] = current_major_name
        major_history_info['year'] = extract_int(td_texts[td_offset])
        counts_text = strip_html_tags(td_texts[1 + td_offset]).split('-')
        major_history_info['recruit_count'] = int_or_default(counts_text[0])
        major_history_info['reapply_count'] = int_or_default(counts_text[1])
        major_history_info['lowest_score'] = extract_int(td_texts[3 + td_offset])
        major_history_info['lowest_rank'] = extract_int(td_texts[4 + td_offset])
        major_history_info['average_rank'] = extract_int(td_texts[6 + td_offset])
        major_history_info['average_score'] = extract_int(td_texts[7 + td_offset])
        major_history_info['score_dist'] = ''  # TODO tmp no content
        major_admission_history_list.append(major_history_info)
    output_to_csv(SCHOOL_INFOS_OUTPUT_FILE,
                  info_iterator(school_admission_history_list, SCHOOL_ADMISSION_HISTORY_COLUMNS))
    output_to_csv(MAJOR_INFOS_OUTPUT_FILE,
                  info_iterator(major_admission_history_list, MAJOR_ADMISSION_HISTORY_COLUMNS))
    print('output: cat=%s, batch=%s, school=%s, start_year=%s' % (category, batch, school_id, start_year))
    # print('\t'.join(SCHOOL_ADMISSION_HISTORY_COLUMNS))
    # for info in school_admission_history_list:
    #     print('\t'.join([str(info[col]) for col in SCHOOL_ADMISSION_HISTORY_COLUMNS]))
    # print('\t'.join(MAJOR_ADMISSION_HISTORY_COLUMNS))
    # for info in major_admission_history_list:
    #     print('\t'.join([str(info[col]) for col in MAJOR_ADMISSION_HISTORY_COLUMNS]))
    # print json.dumps({
    #     'school': school_admission_history_list,
    #     'major': major_admission_history_list
    # }, ensure_ascii=False, indent=2)  # TODO


def info_iterator(rows, columns):
    for row in rows:
        yield (str(row[col]) for col in columns)


def clear_files():
    for output_file in [SCHOOL_INFOS_OUTPUT_FILE, MAJOR_INFOS_OUTPUT_FILE]:
        with open(output_file, 'w') as f:
            f.write('')


def traverse_sys2_infos():
    for category in [0, 1]:
        for batch in [0, 1, 2, 3]:
            for root, dirs, files in os.walk(BASE_PATH + '/%s/%s/sys2' % (CATEGORY_MAP[category], BATCH_MAP[batch])):
                for filename in files:
                    match = re.search(r'sys2_year_(\d+)_school_(\d+).html', filename)
                    start_year = int(match.group(1))
                    school_id = int(match.group(2))
                    # print(category, batch, start_year, school_id)
                    load_sys2_data(category, batch, start_year, school_id)


if __name__ == '__main__':
    clear_files()
    traverse_sys2_infos()
    # load_sys2_data(0, 0, 2019, 10001)
