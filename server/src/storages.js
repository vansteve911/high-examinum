const appConfig = require('./config');
const { SqliteDataSource } = require('./datasource/sqlDataSource');
const { MemDataSource } = require('./datasource/memDataSource');
const localStorageData = require('./const/localStorageData');
// const { admissionBatch, admissionCategory, accountType, userType, gender, adminType, adminTypePrivilege, probabilityLevel } = require('./const/localStorageData'); // TODO: to be removed

const sqliteDataSource = new SqliteDataSource({ dataFile: appConfig.sqlite.dataFile });

module.exports = {
  school: sqliteDataSource.buildStorage({
    table: 'school_new',
    primaryKey: 'id'
  }),
  province: sqliteDataSource.buildStorage({
    table: 'province',
    primaryKey: 'id'
  }),
  schoolAdmissionHistory: sqliteDataSource.buildStorage({
    // table: 'school_admission_history',
    table: 'school_admission_history_new',
    primaryKey: 'id' // TODO actually no id field 
  }),
  majorAdmissionHistory: sqliteDataSource.buildStorage({
    // table: 'major_admission_history',
    table: 'major_admission_history_new',
    primaryKey: 'id' // TODO actually no id field 
  }),
  scoreRanking: sqliteDataSource.buildStorage({
    table: 'score_ranking',
    primaryKey: 'id' // TODO actually no id field 
  }),
  category: new MemDataSource(localStorageData['admissionCategory']),
  batch: new MemDataSource(localStorageData['admissionBatch']),
  probabilityLevel: new MemDataSource(localStorageData['probabilityLevel']),
  yearCutoffScore: new MemDataSource(localStorageData['yearCutoffScore']),
  schoolYearRank: sqliteDataSource.buildStorage({
    table: 'school_year_rank',
    primaryKey: 'school_id'
  }),
  // ethinc & province
  ethnic: new MemDataSource(localStorageData['ethnic']),
  // TODO province
  // account, user, admin
  account: sqliteDataSource.buildStorage({
    table: 'account',
    primaryKey: 'account'
  }),
  accountType: new MemDataSource(localStorageData['accountType']),
  user: sqliteDataSource.buildStorage({
    table: 'user',
    primaryKey: 'id'
  }),
  userType: new MemDataSource(localStorageData['userType']),
  gender: new MemDataSource(localStorageData['gender']),
  admin: sqliteDataSource.buildStorage({
    table: 'admin',
    primaryKey: 'user_id'
  }),
  adminType: new MemDataSource(localStorageData['adminType']),
  adminPrivilege: new MemDataSource(localStorageData['adminPrivilege']),
  // teacher & student related
  teacher: sqliteDataSource.buildStorage({
    table: 'teacher',
    primaryKey: 'user_id'
  }),
  teacherStudentRelation: sqliteDataSource.buildStorage({
    table: 'teacher_student_relation',
    primaryKey: 'id'
  }),
  teacherStudentRelationStatus: new MemDataSource(localStorageData['teacherStudentRelationStatus']),
  studentRemark: sqliteDataSource.buildStorage({
    table: 'student_remark',
    primaryKey: 'id'
  }),
  studentRemarkStatus: new MemDataSource(localStorageData['studentRemarkStatus']),
  // sim_application
  simApplication: sqliteDataSource.buildStorage({
    table: 'sim_application',
    primaryKey: 'id'
  }),
  simApplicationStatus: new MemDataSource(localStorageData['simApplicationStatus']),
  // search inverted index
  searchInvertedIdx: sqliteDataSource.buildStorage({
    table: 'search_inverted_idx',
    primaryKey: 'key'
  }),
  schoolInvertedIdx: sqliteDataSource.buildStorage({
    table: 'school_inverted_idx',
    primaryKey: 'key'
  }),
  // article
  article: sqliteDataSource.buildStorage({
    table: 'article',
    primaryKey: 'id'
  }),
  // invite code
  inviteCode: sqliteDataSource.buildStorage({
    table: 'invite_code',
    primaryKey: 'code'
  }),
  // invite code batch
  inviteCodeBatch: sqliteDataSource.buildStorage({
    table: 'invite_code_batch',
    primaryKey: 'id'
  }),
  // system config
  systemConfig: sqliteDataSource.buildStorage({
    table: 'system_config',
    primaryKey: 'key'
  }),
  // TODO: issue & message
}