const storages = require('../storages');
const crudService = require('./crudService');
const { HAN_ETHNIC_ID } = require('../const/localeConstants');

const { assertAndThrow, isInEnum, isMobile, mergeInfo, parsePagination } = require('../utils/common');
const { sqlFieldMappingConfig } = require('../utils/dataAccess');

const userService = crudService(storages.user, sqlFieldMappingConfig);
const accountService = crudService(storages.account, sqlFieldMappingConfig); // TODO: to be refined
const systemConfigService = require('./systemConfigService');

const GENDERS = { // TODO: use const in MemDataSource
  MALE: 0,
  FEMALE: 1
};
const CANDIDATE_INFO_FIELDS = [
  'category', 'ethnicity', 'year', 'score', 'bonusWithinProvince', 'bonusInterProvince'
];
const USER_BASIC_INFO_FIELDS = [
  'mobile', 'name', 'realName', 'wechat', 'email', 'gender', 'candidateInfo'
];

async function modifyBasicInfo(existedUser, updates) {
  if (updates['candidateInfo']) {
    const enableCandidateInfoEdit = await systemConfigService.get('enableCandidateInfoEdit');
    if (enableCandidateInfoEdit === 'n') {
      throw new Error('目前已关闭修改分数入口，如有疑问请联系工作人员');
    }
    const mergedCanditeInfo = mergeInfo(CANDIDATE_INFO_FIELDS, JSON.parse(existedUser['candidateInfo']), updates['candidateInfo']);
    if (mergedCanditeInfo.ethnicity === HAN_ETHNIC_ID) {
      mergedCanditeInfo.bonusWithinProvince = 0;
      mergedCanditeInfo.bonusInterProvince = 0;
    }
    console.log('mergedCanditeInfo', mergedCanditeInfo); // TODO: tmp
    updates['candidateInfo'] = JSON.stringify(mergedCanditeInfo);
  }
  const mergedInfo = mergeInfo(USER_BASIC_INFO_FIELDS, existedUser, updates);
  await userService.modify({ id: existedUser['id'] }, mergedInfo);
  return mergedInfo;
}

async function modifyMobile(userId, oldMobile, newMobile) {
  const query = { userId, account: oldMobile };
  const accountInfos = await accountService.queryList(query);
  if (!accountInfos) {
    console.log('no account with mobile ', oldMobile); // TODO: log
    return;
  } else if (accountInfos.length > 1) {
    console.error('multiple records with same mobile');
    return;
  }
  return accountService.modify(query, { account: newMobile });
}
const USER_TYPES = {
  TEMP_USER: storages.userType.get('key', 'TEMP_USER'),
  FULL_USER: storages.userType.get('key', 'FULL_USER'),
  DELETED_USER: storages.userType.get('key', 'DELETED_USER'),
};

module.exports = {
  USER_TYPES,
  async createUser(accountInfo, userInfoParams) {
    const userType = userInfoParams['type'];
    console.warn(userInfoParams); // TODO:
    assertAndThrow(storages.userType.get('id', userType) !== undefined, '错误的用户类型');
    const username = userInfoParams['name'] || '用户' + (
      isMobile(accountInfo['account']) ? accountInfo['account'].replace(/(\d{2})(\d{5})/g, '\$1*******') : accountInfo['userId']
    );
    const userInfo = {
      id: accountInfo['userId'],
      type: userInfoParams['type'],
      name: username,
      realName: userInfoParams['realName'] || '',
      mobile: userInfoParams['mobile'] || (isMobile(accountInfo['account']) && accountInfo['account'] || ''),
      wechat: userInfoParams['wechat'] || '',
      email: userInfoParams['email'] || '',
      gender: isInEnum(GENDERS, userInfoParams['gender']) ? userInfoParams['gender'] : -1,
      candidateInfo: '{}',
      addTime: accountInfo['addTime'],
      updateTime: accountInfo['updateTime'],
    };
    await userService.add(userInfo);
    return userInfo;
  },
  async getOne(userId) {
    return userService.getOne(userId);
  },
  parseType(user) {
    return storages.userType.get('id', user['type']);
  },
  parseGender(user) {
    return storages.gender.get('id', user['gender']);
  },
  parseCandidateInfo(user) {
    return JSON.parse(user['candidateInfo']);
  },
  async queryUserList({ query, pagination }) {
    return userService.queryList(query, undefined, parsePagination(pagination));
  },
  async modifyInfo(id, updates) {
    const existedUser = await userService.getOne(id);
    if (!existedUser) {
      throw new Error('用户不存在'); // TODO: use custom error
    }
    if (updates['mobile']) {
      assertAndThrow(isMobile(updates['mobile']), '手机号格式错误');
      await modifyMobile(id, existedUser['mobile'], updates['mobile']);
    }
    if (updates['name'] && updates['name'] !== existedUser['name']) {
      console.log(updates['name'], "updates['name']"); // TODO 
      const sameNameUsers = await userService.queryList({ name: updates['name'] });
      if (sameNameUsers) {
        console.warn('sameNameUsers existed: ', sameNameUsers); // TODO: tmp
        throw new Error('已有同名用户'); // TODO: use custom error;
      }
    }
    return modifyBasicInfo(existedUser, updates);
  },
  isUserCandidateInfoValid(candidateInfo) {
    if (!candidateInfo) {
      return false;
    }
    const { category, ethnicity, year, score, bonusWithinProvince, bonusInterProvince } = candidateInfo;
    return parseInt(category) !== NaN && parseInt(ethnicity) !== NaN && parseInt(year) !== NaN && parseInt(score) !== NaN;
  },
  async changeUserType(id, type) {
    assertAndThrow(storages.userType.get('id', type) !== undefined, '错误的用户类型');
    const user = await userService.getOne(id);
    if (!user) {
      throw new Error('用户不存在'); // TODO: use custom error
    }
    if (user['type'] === type) {
      throw new Error('新旧类型相同，无需更改'); // TODO: use custom error
    }
    return userService.modify({ id }, { type });
  },
  async delOne(userId) {
    if (userId !== undefined) {
      return userService.delOne(userId);
    }
  }
}
