const { article: articleStorage } = require('../storages');
const { mapObjectKeys, underScoreToCamalCase, camelCaseToUnderScore, currentDatetimeString, parsePagination } = require('../utils/common');
const crudService = require('./crudService');

const sqlFieldMappingConfig = {
    deserialize: row => mapObjectKeys(row, underScoreToCamalCase),
    serialize: data => mapObjectKeys(data, camelCaseToUnderScore)
};
const articleService = crudService(articleStorage, sqlFieldMappingConfig);

module.exports = {
  async getOne({ id, key }) {
    if (id) {
      return articleService.getOne(id);
    } else if (key) {
      const articles = await articleService.queryList({ key });
      return articles && articles[0];
    }
    return undefined;
  },
  async getList({ keys, type, pagination }) {
    const query = {};
    if (keys !== undefined) {
      return articleService.queryCustom(builder => builder.whereIn('key', keys));
    } else {      
      if (type !== undefined) {
        query['type'] = type;
      }
      return articleService.queryList(query, undefined, parsePagination(pagination));
    }
  },
  async add(articleCreation, loginUser) {
    const time = currentDatetimeString();
    const article = {
      author: loginUser.name,
      addTime: time,
      updateTime: time,
      ...articleCreation,
    };
    return articleService.add(article);
  },
  async modify(articleUpdates, loginUser) {
    const id = articleUpdates.id;
    const existedArticle = await articleService.getOne(id);
    if (!existedArticle) {
      throw new Error('文章不存在');
    }
    const time = currentDatetimeString();
    const article = {
      lastEditor: loginUser.id,
      updateTime: time,
      ...articleUpdates,
    };
    return articleService.modify({ id }, article);
  }
}