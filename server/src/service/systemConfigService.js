const { systemConfig: systemConfigStorage } = require('../storages');
const { currentDatetimeString, assertAndThrow } = require('../utils/common');
const crudService = require('./crudService');

const { sqlFieldMappingConfig } = require('../utils/dataAccess');

const systemConfigService = crudService(systemConfigStorage, sqlFieldMappingConfig);

module.exports = {
  async queryList(pagination, keyword) {
    const query = keyword ? ['key', 'like', `%${keyword}%`] : undefined;
    return systemConfigService.queryList(query, undefined, pagination);
  },
  async get(key) {
    const record = await systemConfigService.getOne(key);
    return record && record.value;
  },
  async set(key, value, editor) {
    const record = await systemConfigService.getOne(key);
    if (record == undefined) {
      throw new Error('配置不存在');
    }
    const timeStr = currentDatetimeString();
    return systemConfigService.modify({ key }, { key, value, editor, updateTime: timeStr });
  },
  async upsert(record, editor) {
    const { key, title, value } = record;
    assertAndThrow(key !== undefined, '配置id不能为空');
    assertAndThrow(title !== undefined, '配置名不能为空');
    assertAndThrow(value !== undefined, '配置值不能为空');
    const existedRecord = await systemConfigService.getOne(key);
    const timeStr = currentDatetimeString();
    if (existedRecord === undefined) {
      return systemConfigService.add({ addTime: timeStr, updateTime: timeStr, editor, ...record });
    } else {
      return systemConfigService.modify({ key }, { updateTime: timeStr, value, title, editor });
    }
  }
}
