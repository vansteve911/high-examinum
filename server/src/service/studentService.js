const { teacher: teacherStorage, teacherStudentRelation: teacherStudentRelationStorage, teacherStudentRelationStatus: teacherStudentRelationStatuses,
  studentRemark: studentRemarkStorage, studentRemarkStatus: studentRemarkStatuses } = require('../storages');
const { sqlFieldMappingConfig } = require('../utils/dataAccess');
const crudService = require('./crudService');

const teacherService = crudService(teacherStorage, sqlFieldMappingConfig);
const teacherStudentRelationService = crudService(teacherStudentRelationStorage, sqlFieldMappingConfig);
const studentRemarkService = crudService(studentRemarkStorage, sqlFieldMappingConfig);

module.exports = {
  // TODO:
}

