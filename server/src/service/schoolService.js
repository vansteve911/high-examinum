const storages = require('../storages');
const { instanceType } = require('../utils/common');
const crudService = require('./crudService');

const { sqlFieldMappingConfig } = require('../utils/dataAccess');

const schoolService = crudService(storages.school, sqlFieldMappingConfig);

const schoolHistoryService = crudService(storages.schoolAdmissionHistory, sqlFieldMappingConfig); // TODO: move to schoolService
const majorHistoryService = crudService(storages.majorAdmissionHistory, sqlFieldMappingConfig); // TODO: move to schoolService
const scoreRankingService = crudService(storages.scoreRanking, sqlFieldMappingConfig); // TODO: move to schoolService

function buildAdmissionQueryFilters(schoolId, admissionQuery, schoolMajor) {
  const { category, batch, years } = admissionQuery;
  const filters = [{ school_id: schoolId, category, batch }];
  if (years) {
    filters.push(['year', 'in', years]);
  }
  if (schoolMajor) {
    filters.push({ school_major: schoolMajor });
  }
  return filters;
}

async function upsertRecord(dataService, query, updates) {
  const existedRecord = await dataService.queryList(query);
  if (existedRecord.length) {
    return dataService.modify(query, updates);
  } else {
    const newRecord = { ...query, ...updates };
    return dataService.add(newRecord);
  }
}

async function queryByAdmissionFilter({ filters, admissionQuery }) {
  let queryFilters = [];
  const typeOfFilters = instanceType(filters);
  if (typeOfFilters === 'Array') {
    queryFilters = queryFilters.concat(filters);
  } else if (typeOfFilters === 'Object') {
    queryFilters.push(filters);
  }
  if (admissionQuery) {
    let { category, batch } = admissionQuery;
    // queryFilters = queryFilters.concat([ // TODO fix bug
    //   ['categories', 'like', `%${category}%`],
    //   ['batches', 'like', '%' + batch + '%']
    // ]);
    queryFilters = queryFilters.concat([
      `("," || categories || ",") like "%,${category}%"`,
      `("," || batches || ",") like "%,${batch}%"`,
    ]);
  }
  return schoolService.queryByFilters(queryFilters);
}

module.exports = {
  async getOne(id) {
    return schoolService.getOne(id)
  },
  async batchGet(ids) {
    return schoolService.queryCustom(builder => builder.whereIn('id', ids));
  },
  queryByAdmissionFilter,
  async queryByRecommendFilter(filters) {
    const { province, category, batch, majorKeyword, year } = filters;
    if (parseInt(province) === NaN) {
      throw new Error("no valid filters");
    }
    const schools = await this.queryByAdmissionFilter({
      filters: { province },
      admissionQuery: { category, batch }
    });
    // TODO year major filter
    return schools;
  },
  parseAttributes({ attributes }) {
    const { ethnic_bonus, major_score_ladder } = JSON.parse(attributes);
    return { ethnic_bonus, major_score_ladder };
  },
  async queryScoreRankingList({ year, category, score }) {
    return scoreRankingService.queryList({ year, category, sumRule: 0, score });
  },
  async querySchoolHistoryList({ schoolId, category, batch, years }) {
    const filters = buildAdmissionQueryFilters(schoolId, { category, batch, years });
    return schoolHistoryService.queryByFilters(filters);
  },
  async queryMajorHistoryList({ schoolId, schoolMajor, category, batch, years }) {
    const filters = buildAdmissionQueryFilters(schoolId, { category, batch, years }, schoolMajor);
    return majorHistoryService.queryByFilters(filters);
  },
  // search by name
  async searchByName({ name, category, batch }) {
    return schoolService.queryCustom(builder => {
      let returnQuery = builder.where('name', 'like', `%${name}%`);
      if (category !== undefined) {
        returnQuery = returnQuery.andWhere('categories', 'like', `%${category}%`);
      }
      if (batch !== undefined) {
        returnQuery = returnQuery.andWhere('batches', 'like', `%${batch}%`);
      }
      return returnQuery;
    });
  },
  // edit APIs
  async modifySchoolAdmissionHistory(query, updates) {
    const { schoolId, category, batch, year } = query;
    const { recruiteCount, candidateCount, enrollCount, lowestScore, lowestRank, averageScore, averageRank } = updates;
    // TODO validation
    return upsertRecord(schoolHistoryService, 
      { schoolId, category, batch, year },
      { recruiteCount, candidateCount, enrollCount, lowestScore, lowestRank, averageScore, averageRank });
  },
  async modifyMajorAdmissionHistory(query, updates) {
    const { schoolId, category, batch, year, schoolMajor } = query;
    const { recruiteCount, reapplyCount, lowestScore, lowestRank, averageScore, averageRank, scoreDist } = updates;
    // TODO validation
    return upsertRecord(
      majorHistoryService,
      { schoolId, category, batch, year, schoolMajor },
      { recruiteCount, reapplyCount, lowestScore, lowestRank, averageScore, averageRank, scoreDist });
  },
  async modifySchoolInfo(id, updates) {
    const { name, province, rawIntro, attributes } = updates;
    // TODO validation
    return upsertRecord(
      schoolService,
      { id }, 
      { name, province, rawIntro, attributes: JSON.stringify(attributes) }
    );
  }
}
