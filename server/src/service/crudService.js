const { instanceType } = require('../utils/common');

function getFunction(f) {
  return (f instanceof Function) && f;
}

function wrapSyncOrAsyncFunction(f, wrapAsync, wrapSync) {
  if (!f instanceof Function) {
    return undefined;
  }
  return (f.constructor.name === 'AsyncFunction') ? wrapAsync(f) : wrapSync(f);
}

function buildQueryFromFilters(builder, filters) {
  let builtQuery = builder;
  const typeOfFilters = instanceType(filters);
  if (typeOfFilters === 'Array') {
    for (const filter of filters) {
      const typeOfFilter = instanceType(filter);
      if (typeOfFilter === 'Array') {
        builtQuery = builtQuery.andWhere(...filter);
      } else if (typeOfFilter === 'Object') {
        builtQuery = builtQuery.andWhere(filter);
      } else if (typeOfFilter === 'String') {
        builtQuery = builtQuery.andWhereRaw(filter);
      }
    }
  } else if (typeOfFilters === 'Object') {
    builtQuery = builtQuery.where(filters);
  }
  return builtQuery;
}

module.exports = function (storage, { serialize, deserialize }) {
  const storageToData = getFunction(deserialize) || (x => x);

  const dataToStorage = getFunction(serialize) || (x => x);
  // console.log('test dataToStorage', serialize, dataToStorage, dataToStorage({ dataToStorage: 'dataToStorage' })); // TODO fuck

  const getOne = wrapSyncOrAsyncFunction(storage.getOne, 
    (asyncGetOne) => {
      return async (id, fields) => {
        let result = await asyncGetOne(id, fields);
        return storageToData(result);
      };
    },
    (syncGetOne) => {
      return (id, fields) => {
        let result = syncGetOne(id, fields);
        return storageToData(result);
      }
    }
  )

  const queryList = wrapSyncOrAsyncFunction(storage.queryList,
    (asyncQueryList) => {
      return async (query, fields, pagination) => {
        const storageQuery = dataToStorage(query);
        let resultList = await asyncQueryList(storageQuery, fields, pagination);
        return resultList.map(storageToData);
      };
    },
    (syncQueryList) => {
      return (query, fields, pagination) => {
        const storageQuery = dataToStorage(query);
        let resultList = syncQueryList(storageQuery, fields, pagination);
        return resultList.map(storageToData);
      };
    }
  );

  const queryCustom = wrapSyncOrAsyncFunction(storage.queryCustom,
    (asyncQueryCustom) => {
      return async (queryBuilder, fields) => {
        let result = await asyncQueryCustom(queryBuilder, fields);
        if (Array.isArray(result)) {
          return result.map(storageToData);
        } else {
          return storageToData(result);
        }
      };
    },
    (syncQueryCustom) => {
      return (queryBuilder, fields) => {
        let result = syncQueryCustom(queryBuilder, fields);
        if (Array.isArray(result)) {
          return result.map(storageToData);
        } else {
          return storageToData(result);
        }
      };
    }
  );

  const queryByFilters = wrapSyncOrAsyncFunction(storage.queryCustom,
    (asyncQueryCustom) => {
      return async (filters) => {
        let result = await asyncQueryCustom(builder => buildQueryFromFilters(builder, filters))
        if (Array.isArray(result)) {
          return result.map(storageToData);
        } else {
          return storageToData(result);
        }
      };
    },
    (syncQueryCustom) => {
      return (filters) => {
        let result = syncQueryCustom(builder => buildQueryFromFilters(builder, filters))
        if (Array.isArray(result)) {
          return result.map(storageToData);
        } else {
          return storageToData(result);
        }
      };
    }
  );

  const add = wrapSyncOrAsyncFunction(storage.add,
    (asyncAdd) => {
      return async (data) => {
        return await asyncAdd(dataToStorage(data));
      };
    },
    (syncAdd) => {
      return (data) => {
        return syncAdd(dataToStorage(data));
      };
    },
  );

  const modify = wrapSyncOrAsyncFunction(storage.modify,
    (asyncModify) => {
      return async (query, updates) => {
        const storageQuery = dataToStorage(query);
        return await asyncModify(storageQuery, dataToStorage(updates));
      };
    },
    (syncModify) => {
      return (query, updates) => {
        const storageQuery = dataToStorage(query);
        return syncModify(storageQuery, dataToStorage(updates));
      };
    },
  );

  const modifyCustom = wrapSyncOrAsyncFunction(storage.modifyCustom,
    (asyncModifyCustom) => {
      return async (queryBuilder, updates) => {
        return await asyncModifyCustom(queryBuilder, dataToStorage(updates));
      };
    },
    (syncModifyCustom) => {
      return (queryBuilder, updates) => {
        return syncModifyCustom(queryBuilder, dataToStorage(updates));
      };
    },
  );

  const put = wrapSyncOrAsyncFunction(storage.put,
    (asyncPut) => {
      return async (newData) => {
        return await asyncPut(dataToStorage(newData));
      };
    },
    (syncPut) => {
      return (newData) => {
        return syncPut(dataToStorage(newData));
      };
    },
  );

  const del = wrapSyncOrAsyncFunction(storage.del,
    (asyncDel) => {
      return async (query) => {
        const storageQuery = dataToStorage(query);
        return await asyncDel(storageQuery);
      };
    },
    (syncDel) => {
      return (query) => {
        const storageQuery = dataToStorage(storageQuery);
        return syncDel(storageQuery);
      };
    },
  );

  const delOne = wrapSyncOrAsyncFunction(storage.delOne,
    (asyncDelOne) => {
      return async (id) => {
        return await asyncDelOne(id);
      };
    },
    (syncDelOne) => {
      return (id) => {
        return syncDelOne(id);
      };
    },
  );

  return { 
    getOne, 
    queryList, 
    queryCustom,
    queryByFilters,
    add, 
    modify, 
    modifyCustom,
    put,
    del,
    delOne
  };
}