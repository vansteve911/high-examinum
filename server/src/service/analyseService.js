const { admissionAnalyse } = require('../utils/analyse');
const schoolService = require('./schoolService');
const searchService = require('./searchService');
const { GX_PROVINCE_ID } = require('../const/admissionConstants');
const { category: categoryStorage, ethnic: ethnicStorage, yearCutoffScore: yearCutoffScoreStorage, schoolYearRank: schoolYearRankStorage } = require('../storages');
const crudService = require('./crudService');
const { sqlFieldMappingConfig } = require('../utils/dataAccess');

const RANK_TYPE_WSL = 0, RANK_TYPE_XYH = 1;

const schoolYearRankService = crudService(schoolYearRankStorage, sqlFieldMappingConfig);

async function searchAndAnalyse({ schoolId, schoolMajor, category, batch, years, year, score, candidateInfo }) {
  // get school
  const school = await schoolService.getOne(schoolId);
  const ethnicBonus = (school.province === GX_PROVINCE_ID ? candidateInfo.bonusWithinProvince : candidateInfo.bonusInterProvince) || 0;
  const finalScore = score + ethnicBonus;
  const rankings = await schoolService.queryScoreRankingList({ year, category, score: finalScore });
  const currentRank = rankings[0]['rank'];
  let admissionHistory = [];
  if (schoolMajor) {
    admissionHistory = await schoolService.queryMajorHistoryList({ schoolId, schoolMajor, category, batch, years });
  } else {
    admissionHistory = await schoolService.querySchoolHistoryList({ schoolId, category, batch, years });
  }
  // get rankings
  const schoolYearRankings = await schoolYearRankService.queryList({ schoolId });
  for (let admissionInfo of admissionHistory) {
    const yearCutoffScoreInfo = yearCutoffScoreStorage.getByFilter({ 
      year: admissionInfo.year,
      category, 
      batch
    });
    admissionInfo.cutoffScore = yearCutoffScoreInfo.score;
    const rankingInfoFromXYH = schoolYearRankings.find(x => x.year == admissionInfo.year && x.rankOrg == RANK_TYPE_XYH);
    const rankingInfoFromWSL = schoolYearRankings.find(x => x.year == admissionInfo.year && x.rankOrg == RANK_TYPE_WSL);
    admissionInfo.rankingFromXYH = rankingInfoFromXYH && rankingInfoFromXYH.rank;
    admissionInfo.rankingFromWSL = rankingInfoFromWSL && rankingInfoFromWSL.rank;
  }
  return {
    ...admissionAnalyse(admissionHistory, year, currentRank),
    scoreDescription: school.rawIntro
  }
}

function sortByProbabilityDesc(o1, o2) {
  return o2.probability - o1.probability;
}

function sortByProbabilityAsc(o1, o2) {
  return o1.probability - o2.probability;
}

module.exports = {
  async schoolApplyAnalyse({ year, score, candidateInfo, schoolId, category, batch, schoolMajors, applyPriotiy }) {
    const years = [year - 1, year - 2, year - 3];
    const schoolResult = await searchAndAnalyse({ schoolId, category, batch, years, year, score, candidateInfo });
    const majorResults = [];
    for (let schoolMajor of schoolMajors) {
      const majorResult = await searchAndAnalyse({ schoolId, schoolMajor, category, batch, years, year, score, candidateInfo });
      majorResult.name = schoolMajor;
      majorResults.push(majorResult);
    }
    return { 
      schoolResult, 
      majorResults: majorResults.sort((o1, o2) => o2.probability - o1.probability),
      description: `您是${year}年${categoryStorage.get('id', category).name}${candidateInfo.ethnicity ? ethnicStorage.get('id', candidateInfo.ethnicity).name : ''}考生，原始分数${score}`,
    };
  },
  async schoolRecommend(year, score, candidateInfo, category, batch, filters, query) {
    const years = [year - 1, year - 2, year - 3];
    const candidateSchools = await searchService.searchSchool(query, { category, batch }, filters);
    console.log('>>> candidateSchools', candidateSchools); // TODO tmp 
    const groupChong = []; // 冲
    const groupWen = []; // 稳
    const groupBao = []; // 保
    for (let school of candidateSchools) {
      const schoolId = school.id;
      const analyseResult = await searchAndAnalyse({ schoolId, category, batch, years, year, score, candidateInfo });
      analyseResult.name = school.name;
      const probability = analyseResult.probability;
      if (probability < 0) {
        console.warn("analyse result with invalid probability", analyseResult); // TODO
      } else if (probability <= 69) {
        groupChong.push(analyseResult);
      } else if (probability <= 94) {
        groupWen.push(analyseResult);
      } else {
        groupBao.push(analyseResult);
      }
    }
    return [
      { name: "chong", description: "冲 - 可以尝试争取", results: groupChong.sort(sortByProbabilityAsc) },
      { name: "wen", description: "稳 - 成功概率较高", results: groupWen.sort(sortByProbabilityAsc) },
      { name: "bao", description: "保 - 成功概率很高", results: groupBao.sort(sortByProbabilityDesc) },
    ];
  }
}