const { inviteCode: inviteCodeStorage, inviteCodeBatch: inviteCodeBatchStorage } = require('../storages');
const { assertAndThrow, mapObjectKeys, underScoreToCamalCase, camelCaseToUnderScore, currentDatetimeString, parsePagination } = require('../utils/common');
const crudService = require('./crudService');
const authService = require('./authService');

const { hash, createRandom } = require('../utils/auth');

const BATCH_TYPE_PRIVILEGED = 0, BATCH_TYPE_NORMAL = 1;

const sqlFieldMappingConfig = {
  deserialize: row => mapObjectKeys(row, underScoreToCamalCase),
  serialize: data => mapObjectKeys(data, camelCaseToUnderScore)
};

const codeBatchService = crudService(inviteCodeBatchStorage, sqlFieldMappingConfig);
const codeService = crudService(inviteCodeStorage, sqlFieldMappingConfig);

async function addCodeBatch(data) {
  const { name, codeCount, creatorId } = data;
  const sameNameBatch = await codeBatchService.queryList({ name });
  assertAndThrow(!sameNameBatch || sameNameBatch.length == 0, '同名批次存在');
  const time = currentDatetimeString();
  const codeBatch = { name, type: BATCH_TYPE_NORMAL, codeCount, remainCodeCount: codeCount, creatorId, addTime: time, updateTime: time };
  return codeBatchService.add(codeBatch);
}

async function getCodeBatch(id) {
  return codeBatchService.getOne(id);
}

async function getCodeBatches(pagination) {
  return codeBatchService.queryList({}, undefined, parsePagination(pagination));
}

async function getCodesByBatch(batchId) {
  return codeService.queryList({ batchId });
}

async function getCode(code) {
  return codeService.getOne(code);
}

async function adminRegisterPrivilegedInviteCode(registerInfo, adminUser) {
  let privilegedCodeBatch = await codeBatchService.queryList({ type: BATCH_TYPE_PRIVILEGED });
  if (!privilegedCodeBatch || privilegedCodeBatch.length === 0) {
    // add new privileged code batch
    const time = currentDatetimeString();
    const codeBatch = { name: '特权邀请码批次', type: BATCH_TYPE_PRIVILEGED, codeCount: -1, remainCodeCount: -1, creatorId: adminUser.id, addTime: time, updateTime: time };
    await codeBatchService.add(codeBatch);
    const privBatches = await codeBatchService.queryList({ type: BATCH_TYPE_PRIVILEGED });
    privilegedCodeBatch = privBatches[0];
  }
  const registerResult = await createNewInviteCodeUser(privilegedCodeBatch.id, { ...registerInfo, registerFullUser: true });
  // add code record
  const time = currentDatetimeString();
  const newInviteCodeRecord = {
    code: registerResult.inviteCode,
    batchId: privilegedCodeBatch.id,
    ownerId: registerResult.user.id,
    addTime: time,
    updateTime: time
  };
  await codeService.add(newInviteCodeRecord);
  return registerResult;
}

async function inviteCodeRegister(registerInfo) {
  const batchId = registerInfo.batchId;
  const codeBatch = await getCodeBatch(batchId);
  assertAndThrow(codeBatch !== undefined, '批次不存在');
  assertAndThrow(codeBatch.type === BATCH_TYPE_NORMAL, '批次类型错误');
  assertAndThrow(codeBatch.remainCodeCount > 0, '这一批次的验证码已经抢完了');
  // TODO add lock
  return createNormalInviteCodeUser(codeBatch, registerInfo);
}

async function createNormalInviteCodeUser(codeBatch, registerInfo) {
  assertAndThrow(codeBatch.remainCodeCount > 0, '这一批次的验证码已经抢完了');
  const batchId = codeBatch.id;
  let newInviteCode = undefined;
  try {
    const registerResult = await createNewInviteCodeUser(batchId, { ...registerInfo, registerFullUser: false });
    // update 
    const time = currentDatetimeString();
    const newInviteCodeRecord = {
      code: registerResult.inviteCode, 
      batchId, 
      ownerId: registerResult.user.id, 
      addTime: time, 
      updateTime: time
    };
    await codeService.add(newInviteCodeRecord);
    await codeBatchService.modify({ id: batchId }, { remainCodeCount: codeBatch.remainCodeCount - 1 });
    return registerResult;
  } catch (error) {
    console.log('into rollback', error); // TODO:
    if (newInviteCode) {
      await codeService.delOne(newInviteCode);
      await codeBatchService.modify({ id: batchId }, { remainCodeCount: codeBatch.remainCodeCount - 1 });
    }
    throw error;
  }
}

async function createNewInviteCodeUser(batchId, { mobile, specifiedInviteCode, registerFullUser }) {
  let newUserId = undefined;
  let inviteCode = undefined;
  if (specifiedInviteCode !== undefined) {
    const sameValueCode = await getCode(specifiedInviteCode);
    assertAndThrow(sameValueCode === undefined, '已存在相同邀请码');
    inviteCode = specifiedInviteCode;
  } else {
    let sameValueCode = undefined;
    do {
      inviteCode = (parseInt(hash(`${batchId}_${new Date().getTime()}_${createRandom()}`), 16) % 100000000).toString();
      while (inviteCode.length < 8) {
        inviteCode = '0' + inviteCode;
      }
      sameValueCode = await getCode(inviteCode);
    } while (sameValueCode !== undefined);
  }
  try {
    const registerResult = await authService.inviteCodeRegister({ mobile, inviteCode, registerFullUser });
    newUserId = registerResult.user.id;
    return { ...registerResult, inviteCode };
  } catch(error) {
    console.log('into rollback', error); // TODO:
    if (newUserId) {
      await authService.destroyUserAccount(newUserId);
    }
    throw error;
  }
}

module.exports = {
  addCodeBatch,
  getCodeBatch,
  getCodeBatches,
  getCode,
  getCodesByBatch,
  inviteCodeRegister,
  adminRegisterPrivilegedInviteCode,
};