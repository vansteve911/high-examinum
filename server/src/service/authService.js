const storages = require('../storages');
const crudService = require('./crudService');
const userService = require('./userService');

const { AuthenticationError } = require('apollo-server-koa');

const { createSalt, encryptPassword, hash, createToken, verifyToken } = require('../utils/auth');
const { assertAndThrow, currentDatetimeString, isMobile, mapObjectKeys, underScoreToCamalCase, camelCaseToUnderScore } = require('../utils/common');
const sqlFieldMappingConfig = { // TODO: 
  deserialize: row => mapObjectKeys(row, underScoreToCamalCase),
  serialize: data => mapObjectKeys(data, camelCaseToUnderScore)
};

const accountService = crudService(storages.account, sqlFieldMappingConfig);

const ACCOUNT_TYPES = {
  INVITE_CODE_REGISTER: storages.accountType.get('key', 'INVITE_CODE_REGISTER'),
  PASSWORD_REGISTER: storages.accountType.get('key', 'PASSWORD_REGISTER'),
};

function currentTime(includeAddTime = false) {
  const time = currentDatetimeString();
  return {
    addTime: includeAddTime ? time : undefined,
    updateTime: time
  };
}

function encryptSaltAndCredential(password) {
  const salt = createSalt();
  return { salt, credential: encryptPassword(password, salt) };
}

function createAccount(accountType, { account, password, inviteCode }) {
  assertAndThrow(account !== undefined, '必须指定账号');
  const result = {};
  // const accountTypeKey = accountType['key'];
  const accountTypeId = accountType['id'];
  if (ACCOUNT_TYPES.PASSWORD_REGISTER === accountType) {
    assertAndThrow(password !== undefined, '必须指定密码');
    assertAndThrow(isMobile(account), '不是合法的手机号');
    Object.assign(result, encryptSaltAndCredential(password));
  } else if (ACCOUNT_TYPES.INVITE_CODE_REGISTER === accountType) {
    assertAndThrow(inviteCode !== undefined, '邀请码为空');
    assertAndThrow(isMobile(account), '不是合法的手机号');
    Object.assign(result, encryptSaltAndCredential(inviteCode));
  } else {
    throw new Error('账号类型无效');
  }
  Object.assign(result,
    {
      account,
      userId: hash(`${accountTypeId}_${account}`),
      type: accountTypeId,
      ...currentTime(true)
    });
  return result;
}

function createAuthToken({ account, userId, type, userType, createTime }) {
  return createToken({ account, userId, type, userType, createTime });
}

async function register(accountType, registerInfo) {
  const accountInfo = createAccount(accountType,
    {
      ...registerInfo,
      account: registerInfo['mobile']
    });
  const { account, userId } = accountInfo;
  const existedAccount = await accountService.getOne(account);
  if (existedAccount) {
    throw new Error('已注册');
  }
  const existedUser = await userService.getOne(userId);
  if (existedUser) {
    console.log('create new account for existed user', userId, account); // TODO: log
  }
  try {
    await accountService.add(accountInfo);
    const userType = (registerInfo.registerFullUser || accountType === ACCOUNT_TYPES.PASSWORD_REGISTER) ? userService.USER_TYPES.FULL_USER['id'] : userService.USER_TYPES.TEMP_USER['id'];
    const user = await userService.createUser(accountInfo, { ...registerInfo, type: userType });
    return {
      token: createAuthToken({ 
        userType,
        createTime: user.addTime,
        ...accountInfo 
      }),
      user
    };
  } catch (error) {
    console.error('error registering account', error);
    await userService.delOne(userId);
    await accountService.delOne(account);
    throw error;
  }
}

module.exports = {
  ACCOUNT_TYPES,
  async passwordRegister(registerInfo) {
    return register(ACCOUNT_TYPES.PASSWORD_REGISTER, registerInfo);
  },
  async inviteCodeRegister(registerInfo) {
    return register(ACCOUNT_TYPES.INVITE_CODE_REGISTER, registerInfo);
  },
  async login({ account, password }) {
    const accountInfo = await accountService.getOne(account);
    if (accountInfo) {
      const { salt, credential, userId } = accountInfo;
      if (userId && credential === encryptPassword(password, salt)) {
        const user = await userService.getOne(userId);
        if (user.type === userService.USER_TYPES.DELETED_USER['id']) {
          throw new AuthenticationError('账号已注销');
        }
        return {
          token: createAuthToken({ ...accountInfo, createTime: user.addTime, userType: user.type }),
          user
        }
      }
    }
    throw new AuthenticationError('登录失败');
  },
  async getUserAccounts(userId) {
    return accountService.queryList({ userId });
  },
  async destroyUserAccount(userId) {
    await accountService.del({ userId });
    await userService.delOne(userId);
  },
  async modifyMobile(userId, oldMobile, newMobile) {
    const query = { userId, account: oldMobile };
    const accountInfos = await accountService.queryList(query);
    if (!accountInfos) {
      console.log('no account with mobile ', oldMobile); // TODO: log
      return;
    } else if (accountInfos.length > 1) {
      console.error('multiple records with same mobile');
      return;
    }
    return accountService.modify(query, { account: newMobile });
  },
  async activateInviteCodeAccount(account, password, candidateInfo, otherInfo) {
    assertAndThrow(password !== undefined, '密码为空');
    assertAndThrow(userService.isUserCandidateInfoValid(candidateInfo), '考生信息不完整');
    const accountInfo = await accountService.getOne(account);
    assertAndThrow(accountInfo !== undefined, '账号不存在');
    assertAndThrow(accountInfo['type'] === ACCOUNT_TYPES.INVITE_CODE_REGISTER['id'], '不是邀请码注册账号');
    const { salt, credential } = encryptSaltAndCredential(password);
    const modifyAccountRes = await accountService.modify({ account }, { type: ACCOUNT_TYPES.PASSWORD_REGISTER['id'], salt, credential });
    if (!modifyAccountRes) {
      throw new Error(`modifyAccountRes failed, ${modifyAccountRes}`); // TODO:
    }
    const { userId } = accountInfo;
    await userService.modifyInfo(userId, { ...otherInfo, candidateInfo });
    const user = await userService.getOne(userId);
    return {
      // token: createAuthToken({ salt, credential, account, userId, type: ACCOUNT_TYPES.PASSWORD_REGISTER['id'] }),
      token: createAuthToken({ 
        account, 
        userId, 
        type: ACCOUNT_TYPES.PASSWORD_REGISTER['id'], 
        userType: user.type,
        createTime: user.addTime, 
      }),
      user
    };
  },
  getRequestAuthContext({ ctx }) {
    const authorization = ctx.header['authorization'];
    if (authorization) {
      const token = authorization.replace('Bearer ', '') // TODO: can be refined?
      try {
        const authInfo = verifyToken(token);
        return { authInfo };
      } catch (error) {
        throw new AuthenticationError('登录失效，请重新登录');
      }
    }
    return {};
  },
}