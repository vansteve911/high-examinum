const { simApplication: simApplicationStorage, simApplicationStatus: simApplicationStatusStorage } = require('../storages');
const { currentDatetimeString, assertAndThrow } = require('../utils/common');
const crudService = require('./crudService');

const { sqlFieldMappingConfig } = require('../utils/dataAccess');

const simApplyService = crudService(simApplicationStorage, sqlFieldMappingConfig);

const SIM_APPLY_STATUSES = {
  DRAFT: simApplicationStatusStorage.get('key', 'DRAFT'),
  SUBMITTED: simApplicationStatusStorage.get('key', 'SUBMITTED'),
  REVIEWED: simApplicationStatusStorage.get('key', 'REVIEWED'),
  APPROVED: simApplicationStatusStorage.get('key', 'APPROVED'),
  DECLINED: simApplicationStatusStorage.get('key', 'DECLINED'),
}

module.exports = {
  parseType(simApplication) {
    return simApplicationStatusStorage.get('id', simApplication['status']);
  },
  async getUserApplications({ userId }) {
    return simApplyService.queryList({ userId });
  },
  async saveApplication(userId, content) {
    if (userId === undefined) {
      throw new Error('userId undefined');
    }
    try {
      JSON.parse(content);
    } catch (error) {
      throw new Error('格式错误');
    }
    const existedApplications = await this.getUserApplications({ userId });
    const submittedApplication = existedApplications.find(x => x['status'] === SIM_APPLY_STATUSES.SUBMITTED.id);
    if (submittedApplication) {
      throw new Error('已经提交待审核，不可更改');
    }
    const draftApplication = existedApplications.find(x => x['status'] === SIM_APPLY_STATUSES.DRAFT.id);
    const time = currentDatetimeString();
    if (draftApplication !== undefined) {
      await simApplyService.modify({ id: draftApplication['id'] },
        {
          content,
          updateTime: time
        });
    } else {
      await simApplyService.add({
        userId,
        status: SIM_APPLY_STATUSES.DRAFT.id,
        content,
        addTime: time,
        updateTime: time,
      });
    }
  },
  async submitApplication(userId, content) {
    if (userId === undefined) {
      throw new Error('userId undefined');
    }
    try {
      JSON.parse(content);
    } catch (error) {
      throw new Error('格式错误');
    }
    const existedApplications = await this.getUserApplications({ userId });
    const submittedApplication = existedApplications.find(x => x['status'] === SIM_APPLY_STATUSES.SUBMITTED.id);
    if (submittedApplication) {
      throw new Error('已经提交待审核，不可再次提交');
    }
    const draftApplication = existedApplications.find(x => x['status'] === SIM_APPLY_STATUSES.DRAFT.id);
    const time = currentDatetimeString();
    if (draftApplication !== undefined) {
      return simApplyService.modify({ id: draftApplication['id'] },
        {
          content,
          status: SIM_APPLY_STATUSES.SUBMITTED.id,
          updateTime: time
        });
    } else {
      return simApplyService.add({
        userId,
        status: SIM_APPLY_STATUSES.SUBMITTED.id,
        content,
        addTime: time,
        updateTime: time,
      });
    }
  },
  async reviewApplication(userId, status, comment) {
    if (userId === undefined) {
      throw new Error('userId undefined');
    }
    if (status !== SIM_APPLY_STATUSES.APPROVED.id || status !== SIM_APPLY_STATUSES.DECLINED.id) {
      throw new Error('不能指定这一状态');
    }
    assertAndThrow(comment !== undefined, '必须指定评语');
    const simApplications = await getUserApplications({ userId });
    const submittedApplication = simApplications.find(x => x['status'] === SIM_APPLY_STATUSES.SUBMITTED.id);
    if (submittedApplication === undefined) {
      throw new Error('尚无待审核的志愿填报');
    }
    return simApplyService.modify({ id: draftApplication['id'] },
    {
      comment,
      status: SIM_APPLY_STATUSES.SUBMITTED.id,
      updateTime: time
    });
  }
}