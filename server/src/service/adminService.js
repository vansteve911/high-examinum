const { admin: adminStorage, adminType: adminTypeStorage, adminPrivilege: adminPrivilegeStorage } = require('../storages');
const { mapObjectKeys, underScoreToCamalCase, camelCaseToUnderScore, mergeInfo, assertAndThrow } = require('../utils/common');
const crudService = require('./crudService');
const authService = require('./authService');
// const userService = require('./userService');

const sqlFieldMappingConfig = {
    deserialize: row => mapObjectKeys(row, underScoreToCamalCase),
    serialize: data => mapObjectKeys(data, camelCaseToUnderScore)
};
const adminService = crudService(adminStorage, sqlFieldMappingConfig);

// const SUPERVISOR_ID = 999;
const ADMIN_TYPES = {
    INVALID: adminTypeStorage.get('key', 'INVALID'),
    EDITOR: adminTypeStorage.get('key', 'EDITOR'),
    AUDITOR: adminTypeStorage.get('key', 'AUDITOR'),
    TEACHER: adminTypeStorage.get('key', 'TEACHER'),
    SUPERVISOR: adminTypeStorage.get('key', 'SUPERVISOR'),
};
const ADMIN_PRIVILEGES = {
    CREATE_ADMIN: 'CREATE_ADMIN',
    MODIFY_ADMIN: 'MODIFY_ADMIN',
    VIEW_ADMIN_INFO: 'VIEW_ADMIN_INFO',
    MODIFY_USER_INFO: 'MODIFY_USER_INFO',
    CHANGE_USER_TYPE: 'CHANGE_USER_TYPE',
    VIEW_USER_INFO: 'VIEW_USER_INFO',
    EDIT_SCHOOL_INFO: 'EDIT_SCHOOL_INFO',
    EDIT_CONTENT_INFO: 'EDIT_CONTENT_INFO',
    REVIEW_SIM_APPLICATION: 'REVIEW_SIM_APPLICATION',
    REGISTER_PRIVILEGED_INVITE_CODE: 'REGISTER_PRIVILEGED_INVITE_CODE',
    ADD_INVITE_CODE_BATCH: 'ADD_INVITE_CODE_BATCH',
};
const ADMIN_INFO_FIELDS = [
    'type', 'realName', 'wechat', 'email',
];
const INACTIVE_ADMIN_INITIAL_PASSWORD = '123456'; // TODO: to be more secure

function parseType(admin) {
    return adminTypeStorage.get('id', admin['type']);
}

function parsePrivileges(admin) {
    // const adminTypeInfo = adminTypeStorage.get('id', admin['type']);
    const adminTypeInfo = parseType(admin);
    return adminTypeInfo['privileges'].concat(admin['extraPrivileges'])
        .filter(p => p)
        .map(key => {
            return adminPrivilegeStorage.get('key', key);
        });
}

function hasPrivilege(admin, privilege) {
    const privilegeKeys = parsePrivileges(admin);
    return privilegeKeys.find(p => p['key'] == privilege) !== undefined;
}

async function queryAdminList({ query, pagination }) {
    return adminService.queryList(query, undefined, pagination);
}

module.exports = {
    ADMIN_TYPES,
    ADMIN_PRIVILEGES,
    checkPrivilege(operator, privilege) {
        console.log('checkPrivilege: ', operator, privilege); // TODO:
        if (!hasPrivilege(operator, privilege)) {
            throw new Error('没有权限操作'); // TODO: use directives to control
        }
    },
    async createAdmin(operator, registerInfo) {
        checkPrivilege(operator, ADMIN_PRIVILEGES.CREATE_ADMIN);
        assertAndThrow(adminTypeStorage.get('id', registerInfo['type']) !== undefined, '管理员类型错误')
        let userId = undefined;
        try {
            const { user } = await authService.inviteCodeRegister({ ...registerInfo, registerFullUser: true, inviteCode: INACTIVE_ADMIN_INITIAL_PASSWORD });
            console.log('authService.register res', user); // TODO:
            userId = user['id'];
            const adminInfo = {
                userId,
                name: user['name'],
                type: registerInfo['type'],
                realName: user['realName'],
                mobile: user['mobile'],
                wechat: user['wechat'],
                email: user['email'],
                gender: user['gender'],
                addTime: user['addTime'],
                updateTime: user['updateTime']
            };
            await adminService.add(adminInfo);
            // return assembleAdmin(adminInfo);
            return adminInfo;
        } catch (error) {
            if (userId !== undefined) {
                console.log('into rollback'); // TODO:
                await authService.destroyUserAccount(userId);
                await adminService.delOne(userId);
            }
            throw error;
        }
    },
    async getOne(userId) {
        return adminService.getOne(userId);
        // const admin = await adminService.getOne(userId);
        // return assembleAdmin(admin);
    },
    queryAdminList,
    parseType,
    parsePrivileges,
    hasPrivilege,
    async modifyAdmin(operator, userId, updates) {
        checkPrivilege(operator, ADMIN_PRIVILEGES.MODIFY_ADMIN);
        const existedAdmin = await adminService.getOne(userId);
        if (!existedAdmin) {
            throw new Error('管理员不存在'); // TODO: use custom error
        }
        if (updates['type'] !== undefined) {
            assertAndThrow(adminTypeStorage.get('id', updates['type']) !== undefined, '错误的管理员类型');
        }
        const mergedInfo = mergeInfo(ADMIN_INFO_FIELDS, existedAdmin, updates);
        const res = await adminService.modify({ userId }, mergedInfo);
        console.log('modifyBasicInfo res', res); // TODO: tmp for test
        return res;
    },
    queryAdminList,
    // TODO add extra privileges
}
