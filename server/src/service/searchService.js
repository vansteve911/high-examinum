const _ = require('lodash');

const { searchInvertedIdx, schoolInvertedIdx } = require('../storages');
const schoolService = require('./schoolService');
const { searchTypes, searchAttributes } = require('../const/searchConstants');
// const knex = require('knex');

async function doSearchIdx({ key, value, op = '=='}) {
  console.log('doSearchIdx', key, value, op); // TODO
  const queryRes = await schoolInvertedIdx.queryCustom(builder => {
    let q = builder.select('school_id').where('attribute', key);
    if (op === '==') {
      return q.andWhere('value', value);
    } else if (op === 'like' || op === '<' || op === '>' || op === '<=' || op === '>=') {
      return q.andWhere('value', op, value);
    } else {
      throw new Error('illegal op type');
    }
  });
  return queryRes.map(res => res['school_id']);
}

module.exports = {
  async searchSchool(query, { category, batch }, filters) {
    const affirmativeTerms = [
      { key: searchAttributes.category, value: category },
      { key: searchAttributes.batch, value: batch },
    ];
    const negativeTerms = [];
    for (let filter of filters) {
      const storedAttribute = searchAttributes[filter.key];
      console.log('storedAttribute', storedAttribute); // TODO 
      if (storedAttribute) {
        if (filter['op'] === '!=') {
          negativeTerms.push({ key: storedAttribute, value: filter.value });
        } else {
          affirmativeTerms.push({ key: storedAttribute, value: filter.value, op: filter.op });
        }
      }
    }
    const affirmativeQueryFutures = affirmativeTerms.map(term => doSearchIdx(term));
    const negativeQueryFutures = negativeTerms.map(term => doSearchIdx(term));
    const affirmativeResults = await Promise.all(affirmativeQueryFutures);
    const negativeResults = await Promise.all(negativeQueryFutures);
    const ids =  _.difference(_.intersection(...affirmativeResults), _.union(...negativeResults)).slice(0, 100);
    return schoolService.batchGet(Array.from(ids));
  },
}