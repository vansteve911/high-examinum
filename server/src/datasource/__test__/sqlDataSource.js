const path = require('path');

const { SqliteDataSource } = require('../sqlDataSource');

const sqliteDataSource = new SqliteDataSource({
  dataFile: path.join(__dirname, '../../testResource/sqlite.db')
});
const schoolStorage = sqliteDataSource.buildStorage({
  table: 'school',
  primaryKey: 'id'
});

const q1 = schoolStorage.queryList({ name: '实例大学' })
  .then((data) => console.log('getList', data))
  .catch((err) => console.error(err));
const q2 = schoolStorage.getOne(1)
  .then((data) => console.log('getOne', data))
  .catch((err) => console.error(err));

const op = async () => {
  const id1 = 19999;
  const id2 = 29999;
  const data1 = { id: id1, name: '测试大学1', province: 9999, categories: '0,1', batches: '0'};
  const data2 = { id: id2, name: '测试大学2', province: 9999, categories: '0,1', batches: '0' };
  try {
    let add1Res = await schoolStorage.add(data1);
    console.log('add1', add1Res);
    let get1Res = await schoolStorage.getOne(id1);
    console.log('get1Res', get1Res);
    let add2Res = await schoolStorage.add(data2);
    console.log('add2Res', add2Res);
    let get2Res = await schoolStorage.getOne(id2);
    console.log('get2Res', get2Res);
    let mod1Res = await schoolStorage.modify({ id: id1 }, { batches: '1' });
    console.log('mod1Res', mod1Res);
    get1Res = await schoolStorage.getOne(id1);
    console.log('get1Res', get1Res);
    let modBothRes = await schoolStorage.modifyCustom(clause => clause.whereIn('id', [id1, id2]), { batches: '0' });
    console.log('modBothRes', modBothRes);
    let getBothRes = await schoolStorage.queryCustom(clause => clause.whereIn('id', [id1, id2]));
    console.log('getBothRes', getBothRes);
    let currData1 = getBothRes[0];
    currData1.province = 11;
    let put1Res = await schoolStorage.put(currData1);
    console.log('put1Res', put1Res);
    getBothRes = await schoolStorage.queryCustom(clause => clause.whereIn('id', [id1, id2]));
    console.log('getBothRes after put data1', getBothRes);
  } catch (err) {
    console.error('err!', err);
  } finally {
    await schoolStorage.del({ id: id1 });
    await schoolStorage.del({ id: id2 });
  }
}

Promise.all(
  // [q1, q2, op()]
  [op()]
).then(_ => sqliteDataSource.close());
