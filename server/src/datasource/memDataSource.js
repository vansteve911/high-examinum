class MemDataSource {
  constructor({ dataList, queryKeys }) {
    this.dataList = dataList;
    const queryMaps = {};
    queryKeys.forEach(key => {
      const queryMap = {};
      dataList.forEach(data => {
        queryMap[data[key]] = data;
      });
      queryMaps[key] = queryMap;
    });
    this.queryMaps = queryMaps;
  }

  get(key, value) {
    return this.queryMaps[key][value];
  }
  getByFilter(filter) {
    return this.dataList.find(data => {
      for (let k of Object.keys(filter)) {
        if (data[k] !== filter[k]) {
          return false;
        }
      }
      return true;
    })
  }
}

module.exports = { MemDataSource };
