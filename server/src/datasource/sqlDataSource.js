const knex = require('knex');

class SqlDataSource {

  constructor({ clientType, connection, option }) {
    const { useNullAsDefault } = (option || {});
    this.db = knex({
      client: clientType,
      connection: connection,
      useNullAsDefault: useNullAsDefault || true
    });
  }

  buildStorage({ table, primaryKey }) {
    const db = this.db;
    if (!(db && table && typeof(primaryKey) === 'string')) {
      throw Error('illegal parameter');
    }
    
    return {
      getOne: async (id, fields) => {
        return db.select(fields).from(table).where(primaryKey, id).first();
      },
      queryList: async (query, fields, pagination) => {
        let queryPromise = db.select(fields).from(table);
        if (query) {
          queryPromise = queryPromise.where(query);
        }
        if (pagination) {
          const { offset = 0, limit = 10, orderBy, orderType = 'asc' } = pagination;
          queryPromise = queryPromise.limit(limit).offset(offset);
          if (orderBy) {
            queryPromise = queryPromise.orderBy(orderBy, orderType);
          }
        }
        return queryPromise;
      },
      queryCustom: async (queryBuilder, fields) => {
        return queryBuilder(db.select(fields).from(table));
      },
      queryRaw: async (dbQueryBuilder) => {
        return dbQueryBuilder(db);
      },
      add: async (data) => {
        return db(table).insert(data);
      },
      modify: async (query, updates) => {
        if (!(query && Object.keys(query).length > 0) || !updates) {
          throw Error('empty query or updates');
        }
        return db(table).where(query).update(updates);
      },
      modifyCustom: async (queryBuilder, updates) => {
        return queryBuilder(db(table)).update(updates);
      },
      put: async (newData) => {
        if (!(newData && newData[primaryKey] !== undefined)) {
          throw Error('newData must contains primary key');
        }
        let id = newData[primaryKey];
        return db(table).where(primaryKey, id).update(newData);
      },
      del: async (query) => {
        if (!(query && Object.keys(query).length > 0)) {
          throw Error('empty query or updates');
        }
        return db(table).where(query).del();
      },
      async delOne(id) {
        return db(table).where(primaryKey, id).del();
      },
    }
  }

  close() {
    this.db.destroy();
  }

}

class SqliteDataSource extends SqlDataSource {
  constructor({ dataFile, option }) {
    super({ 
      clientType: 'sqlite3', 
      connection: {
        filename: dataFile
      },
      option
    });
  }
}


class MySqlDataSource extends SqlDataSource {
  constructor({ host, port, user, password, database, option }) {
    super({
      clientType: 'mysql',
      connection: {
        host: host,
        port: port || 3306,
        user: user,
        password: password,
        database: database
      },
      option
    });
  }
}

module.exports = { SqliteDataSource, MySqlDataSource }
