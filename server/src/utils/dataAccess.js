const { mapObjectKeys, underScoreToCamalCase, camelCaseToUnderScore } = require('./common');

module.exports = {
  sqlFieldMappingConfig: {
    deserialize: row => mapObjectKeys(row, underScoreToCamalCase),
    serialize: data => mapObjectKeys(data, camelCaseToUnderScore)
  },
}