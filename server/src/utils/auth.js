const crypto = require('crypto'); 
const jwt = require("jsonwebtoken");

const TOKEN_SECRET = process.env.TOKEN_SECRET || 'high-examinum-test';

module.exports = {
  createSalt() {
    return crypto.randomBytes(16).toString('hex');
  },
  createRandom() {
    return (parseInt(crypto.randomBytes(7).toString('hex'), 16) + (new Date()).getTime()).toString(16);
  },
  hash(input) {
    const md5 = crypto.createHash('md5');
    return md5.update(input).digest('hex');
  },
  encryptPassword(password, salt) {
    return crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');
  },
  createToken(authInfo) {
    return jwt.sign(authInfo, TOKEN_SECRET, {
      expiresIn: 60 * 60
    })
  },
  verifyToken(token) {
    try {
      return jwt.verify(token, TOKEN_SECRET);
    } catch (e) {
      throw new Error('invalid token');
    }
  }
}