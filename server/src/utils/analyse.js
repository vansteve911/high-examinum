const { averageOf } = require("./common");

const { probabilityLevel: probabilityLevelStorage } = require('../storages');

function getProbabilityDescription(probability, rankGap) {
  const descriptionTemplate = probabilityLevelStorage.get('probability', probability)['description'];
  return descriptionTemplate.replace('{RANK_GAP}', rankGap);
}

function admissionAnalyse(historyAdmissionInfos, currentYear, currentRank) {
  /*
  - group historyAdmissionInfos by year
  - extract year info: { lowestRank, averageRank }
  - from level high to low: match cases
  */
  let probability = 0;
  let customDescription = undefined;
  const yearGroupedInfos = [];
  const hints = [];
  const hasDataYearOffsets = [];
  for (let yearOffset = 1; yearOffset <= 3; yearOffset++) {
    const year = currentYear - yearOffset;
    const info = historyAdmissionInfos.find(x => x['year'] === year);
    const ranksInfo = {};
    if (info) {
      ranksInfo['year'] = year;
      ranksInfo['lowestRank'] = parseInt(info['lowestRank']);
      ranksInfo['lowestScore'] = parseInt(info['lowestScore']);
      ranksInfo['averageScore'] = parseInt(info['averageScore']);
      ranksInfo['averageRank'] = parseInt(info['averageRank']);
      ranksInfo['recruitCount'] = parseInt(info['recruitCount']);
      ranksInfo['candidateCount'] = parseInt(info['candidateCount']);
      ranksInfo['enrollCount'] = parseInt(info['enrollCount']);
      ranksInfo['rankingFromXYH'] = parseInt(info['rankingFromXYH']);
      ranksInfo['rankingFromWSL'] = parseInt(info['rankingFromWSL']);
      ranksInfo['cutoffScore'] = info['cutoffScore'];
      hasDataYearOffsets.push(yearOffset - 1);
    } else {
      hints.push({ title: '该年份没有录取记录', value: year });
    }
    yearGroupedInfos.push(ranksInfo);
  }
  if (hasDataYearOffsets.length === 0) {
    // all year data empty
    // hints.push({ title: '历史录取数据为空', value: '没有最近三年的招生数据，请参考相近名称专业或人工' });
    customDescription = '没有最近三年的招生数据，请参考相近名称专业进行分析（33%概率仅作为参考）。';
    probability = 33;
  } else if (hasDataYearOffsets.length === 1) {
    // only contains one year info
    const theOnlyValidYearInfo = yearGroupedInfos[hasDataYearOffsets[0]];
    if (currentRank <= theOnlyValidYearInfo['averageRank']) {
      customDescription = '仅有一年的录取信息，考生名次达到该年的录取平均分名次';
      probability = 95;
    } else if (currentRank <= theOnlyValidYearInfo['lowestRank']) {
      customDescription = '仅有一年的录取信息，考生名次达到该年的最低平均分名次';
      probability = 70;
    } else {
      customDescription = '仅有一年的录取信息，考生名次未达到该年的录取最低分名次';
      probability = 30;
    }
  } else if (hasDataYearOffsets.length === 2) {
    // contains two years info
    const firstNonEmptyYearInfo = yearGroupedInfos[hasDataYearOffsets[0]],
      secondNonEmptyYearInfo = yearGroupedInfos[hasDataYearOffsets[1]];
    if (currentRank <= firstNonEmptyYearInfo['averageRank'] && currentRank <= secondNonEmptyYearInfo['averageRank']) {
      customDescription = '仅有两年的录取信息，考生名次均超过这两年的录取平均分名次';
      probability = 95;
    } else if (
      (currentRank <= firstNonEmptyYearInfo['averageRank'] && currentRank <= secondNonEmptyYearInfo['lowestRank']) ||
      (currentRank <= secondNonEmptyYearInfo['averageRank'] && currentRank <= firstNonEmptyYearInfo['lowestRank'])
    ) {
      hints.push({ title: '缺少某年份录取数据', value: '仅有两年的录取信息，考生名次超过其中一年的录取平均分名次，超过另一年的录取最低分名次' });
      for (let info of yearGroupedInfos) {
        if (info['averageRank'] == undefined) {
          // set missing data year info
          info['averageRank'] = currentRank;
          info['lowestRank'] = currentRank - 1;
          break;
        }
      }
      // probability to be calculated
    } else if (currentRank <= firstNonEmptyYearInfo['lowestRank'] && currentRank <= secondNonEmptyYearInfo['lowestRank']) {
      customDescription = '仅有两年的录取信息，考生名次均超过这两年的录取最低分名次';
      probability = 70;
    } else {
      customDescription = '仅有两年的录取信息，考生名次均未达到这两年的录取最低分名次';
      probability = 30;
    }
  }
  if (probability == 0) {
    // TODO judge
    const lastYearInfo = yearGroupedInfos[0],
      twoYearsBeforeInfo = yearGroupedInfos[1],
      threeYearsBeforeInfo = yearGroupedInfos[2];
    const reachedLastYearAvgRank = currentRank <= lastYearInfo['averageRank'],
      reachedLastYearLowestRank = currentRank <= lastYearInfo['lowestRank'],
      reachedTwoYearsBeforeAvgRank = currentRank <= twoYearsBeforeInfo['averageRank'],
      reachedTwoYearsBeforeLowestRank = currentRank <= twoYearsBeforeInfo['lowestRank'],
      reachedThreeYearsBeforeAvgRank = currentRank <= threeYearsBeforeInfo['averageRank'],
      reachedThreeYearsBeforeLowestRank = currentRank <= threeYearsBeforeInfo['lowestRank'];
    // judge logic
    if (reachedLastYearAvgRank && reachedTwoYearsBeforeAvgRank && reachedThreeYearsBeforeAvgRank) {
      // 99% ~ 95%
      const threeYearsAvgRank = averageOf(lastYearInfo['averageRank'], twoYearsBeforeInfo['averageRank'], threeYearsBeforeInfo['averageRank']);
      hints.push({ title: '三年的录取平均分名次的平均值', value: parseInt(threeYearsAvgRank) });
      const rankGap = threeYearsAvgRank - currentRank;
      if (threeYearsAvgRank < 100) {
        hints.push({ title: '学校等级', value: '三年的录取平均分名次的平均值在第1-99名（清北复交等）' });
        if (rankGap > 12) {
          probability = 99;
          customDescription = getProbabilityDescription(probability, '在12名之上');
        } else if (rankGap > 9) {
          probability = 98;
          customDescription = getProbabilityDescription(probability, '在10-12名之间');
        } else if (rankGap > 6) {
          probability = 97;
          customDescription = getProbabilityDescription(probability, '在7-9名之间');
        } else if (rankGap > 3) {
          probability = 96;
          customDescription = getProbabilityDescription(probability, '在4-6名之间');
        } else {
          probability = 95;
          customDescription = getProbabilityDescription(probability, '在0-3名之间');
        }
      } else if (threeYearsAvgRank < 1000) {
        hints.push({ title: '学校等级', value: '三年的录取平均分名次的平均值在第100-999名（少部分985高校）' });
        if (rankGap > 120) {
          probability = 99;
          customDescription = getProbabilityDescription(probability, '在120名之上');
        } else if (rankGap > 90) {
          probability = 98;
          customDescription = getProbabilityDescription(probability, '在91-120名之间');
        } else if (rankGap > 60) {
          probability = 97;
          customDescription = getProbabilityDescription(probability, '在61-90名之间');
        } else if (rankGap > 30) {
          probability = 96;
          customDescription = getProbabilityDescription(probability, '在31-60名之间');
        } else {
          probability = 95;
          customDescription = getProbabilityDescription(probability, '在0-30名之间');
        }
      } else if (threeYearsAvgRank < 10000) {
        hints.push({ title: '学校等级', value: '三年的录取平均分名次的平均值在第1000-10000名（大部分985和211高校）' });
        if (rankGap > 800) {
          probability = 99;
          customDescription = getProbabilityDescription(probability, '在800名之上');
        } else if (rankGap > 600) {
          probability = 98;
          customDescription = getProbabilityDescription(probability, '在601-800名之间');
        } else if (rankGap > 400) {
          probability = 97;
          customDescription = getProbabilityDescription(probability, '在401-600名之间');
        } else if (rankGap > 200) {
          probability = 96;
          customDescription = getProbabilityDescription(probability, '在201-400名之间');
        } else {
          probability = 95;
          customDescription = getProbabilityDescription(probability, '在0-200名之间');
        }
      } else {
        hints.push({ title: '学校等级', value: '三年的录取平均分名次的平均值在第10000名以上（大部分双非高校）' });
        if (rankGap > 8000) {
          probability = 99;
          customDescription = getProbabilityDescription(probability, '在8000名之上');
        } else if (rankGap > 6000) {
          probability = 98;
          customDescription = getProbabilityDescription(probability, '在6001-8000名之间');
        } else if (rankGap > 4000) {
          probability = 97;
          customDescription = getProbabilityDescription(probability, '在4001-6000名之间');
        } else if (rankGap > 2000) {
          probability = 96;
          customDescription = getProbabilityDescription(probability, '在2001-4000名之间');
        } else {
          probability = 95;
          customDescription = getProbabilityDescription(probability, '在0-2000名之间');
        }
      }
    } else if (reachedLastYearAvgRank && reachedTwoYearsBeforeAvgRank && reachedThreeYearsBeforeLowestRank) {
      // 94% ~ 90%
      const lastAndTwoYearsBeforeAvgRank = averageOf(lastYearInfo['averageRank'], twoYearsBeforeInfo['averageRank']);
      hints.push({ title: '近两年的录取平均分名次的平均值', value: parseInt(lastAndTwoYearsBeforeAvgRank) });
      const rankGap = lastAndTwoYearsBeforeAvgRank - currentRank;
      if (lastAndTwoYearsBeforeAvgRank < 100) {
        hints.push({ title: '学校等级', value: '近两年的录取平均分名次的平均值在第1-99名（清北复交等）' });
        if (rankGap > 12) {
          probability = 94;
          customDescription = getProbabilityDescription(probability, '在12名之上');
        } else if (rankGap > 9) {
          probability = 93;
          customDescription = getProbabilityDescription(probability, '在10-12名之间');
        } else if (rankGap > 6) {
          probability = 92;
          customDescription = getProbabilityDescription(probability, '在7-9名之间');
        } else if (rankGap > 3) {
          probability = 91;
          customDescription = getProbabilityDescription(probability, '在4-6名之间');
        } else {
          probability = 90;
          customDescription = getProbabilityDescription(probability, '在0-3名之间');
        }
      } else if (lastAndTwoYearsBeforeAvgRank < 1000) {
        hints.push({ title: '学校等级', value: '近两年的录取平均分名次的平均值在第100-999名（少部分985高校）' });
        if (rankGap > 120) {
          probability = 94;
          customDescription = getProbabilityDescription(probability, '在120名之上');
        } else if (rankGap > 90) {
          probability = 93;
          customDescription = getProbabilityDescription(probability, '在91-120名之间');
        } else if (rankGap > 60) {
          probability = 92;
          customDescription = getProbabilityDescription(probability, '在61-90名之间');
        } else if (rankGap > 30) {
          probability = 91;
          customDescription = getProbabilityDescription(probability, '在31-60名之间');
        } else {
          probability = 90;
          customDescription = getProbabilityDescription(probability, '在0-30名之间');
        }
      } else if (lastAndTwoYearsBeforeAvgRank < 10000) {
        hints.push({ title: '学校等级', value: '近两年的录取平均分名次的平均值在第1000-10000名（大部分985和211高校）' });
        if (rankGap > 800) {
          probability = 94;
          customDescription = getProbabilityDescription(probability, '在800名之上');
        } else if (rankGap > 600) {
          probability = 93;
          customDescription = getProbabilityDescription(probability, '在601-800名之间');
        } else if (rankGap > 400) {
          probability = 92;
          customDescription = getProbabilityDescription(probability, '在401-600名之间');
        } else if (rankGap > 200) {
          probability = 91;
          customDescription = getProbabilityDescription(probability, '在201-400名之间');
        } else {
          probability = 90;
          customDescription = getProbabilityDescription(probability, '在0-200名之间');
        }
      } else {
        hints.push({ title: '学校等级', value: '近两年的录取平均分名次的平均值在第10000名以上（大部分双非高校）' });
        if (rankGap > 8000) {
          probability = 94;
          customDescription = getProbabilityDescription(probability, '在8000名之上');
        } else if (rankGap > 6000) {
          probability = 93;
          customDescription = getProbabilityDescription(probability, '在6001-8000名之间');
        } else if (rankGap > 4000) {
          probability = 92;
          customDescription = getProbabilityDescription(probability, '在4001-6000名之间');
        } else if (rankGap > 2000) {
          probability = 91;
          customDescription = getProbabilityDescription(probability, '在2001-4000名之间');
        } else {
          probability = 90;
          customDescription = getProbabilityDescription(probability, '在0-2000名之间');
        }
      }
    } else if (reachedLastYearAvgRank && reachedThreeYearsBeforeAvgRank && reachedTwoYearsBeforeLowestRank) {
      // 89% ~ 85%
      const lastAndThreeYearsBeforeAvgRank = averageOf(lastYearInfo['averageRank'], threeYearsBeforeInfo['averageRank']);
      hints.push({ title: '去年和大前年的录取平均分名次的平均值', value: parseInt(lastAndThreeYearsBeforeAvgRank) });
      const rankGap = lastAndThreeYearsBeforeAvgRank - currentRank;
      if (lastAndThreeYearsBeforeAvgRank < 100) {
        hints.push({ title: '学校等级', value: '去年和大前年的录取平均分名次的平均值在第1-99名（清北复交等）' });
        if (rankGap > 12) {
          probability = 89;
          customDescription = getProbabilityDescription(probability, '在12名之上');
        } else if (rankGap > 9) {
          probability = 88;
          customDescription = getProbabilityDescription(probability, '在10-12名之间');
        } else if (rankGap > 6) {
          probability = 87;
          customDescription = getProbabilityDescription(probability, '在7-9名之间');
        } else if (rankGap > 3) {
          probability = 86;
          customDescription = getProbabilityDescription(probability, '在4-6名之间');
        } else {
          probability = 85;
          customDescription = getProbabilityDescription(probability, '在0-3名之间');
        }
      } else if (lastAndThreeYearsBeforeAvgRank < 1000) {
        hints.push({ title: '学校等级', value: '去年和大前年的录取平均分名次的平均值在第100-999名（少部分985高校）' });
        if (rankGap > 120) {
          probability = 89;
          customDescription = getProbabilityDescription(probability, '在120名之上');
        } else if (rankGap > 90) {
          probability = 88;
          customDescription = getProbabilityDescription(probability, '在91-120名之间');
        } else if (rankGap > 60) {
          probability = 87;
          customDescription = getProbabilityDescription(probability, '在61-90名之间');
        } else if (rankGap > 30) {
          probability = 86;
          customDescription = getProbabilityDescription(probability, '在31-60名之间');
        } else {
          probability = 85;
          customDescription = getProbabilityDescription(probability, '在0-30名之间');
        }
      } else if (lastAndThreeYearsBeforeAvgRank < 10000) {
        hints.push({ title: '学校等级', value: '去年和大前年的录取平均分名次的平均值在第1000-10000名（大部分985和211高校）' });
        if (rankGap > 800) {
          probability = 89;
          customDescription = getProbabilityDescription(probability, '在800名之上');
        } else if (rankGap > 600) {
          probability = 88;
          customDescription = getProbabilityDescription(probability, '在601-800名之间');
        } else if (rankGap > 400) {
          probability = 87;
          customDescription = getProbabilityDescription(probability, '在401-600名之间');
        } else if (rankGap > 200) {
          probability = 86;
          customDescription = getProbabilityDescription(probability, '在201-400名之间');
        } else {
          probability = 85;
          customDescription = getProbabilityDescription(probability, '在0-200名之间');
        }
      } else {
        hints.push({ title: '学校等级', value: '去年和大前年的录取平均分名次的平均值在第10000名以上（大部分双非高校）' });
        if (rankGap > 8000) {
          probability = 89;
          customDescription = getProbabilityDescription(probability, '在8000名之上');
        } else if (rankGap > 6000) {
          probability = 88;
          customDescription = getProbabilityDescription(probability, '在6001-8000名之间');
        } else if (rankGap > 4000) {
          probability = 87;
          customDescription = getProbabilityDescription(probability, '在4001-6000名之间');
        } else if (rankGap > 2000) {
          probability = 86;
          customDescription = getProbabilityDescription(probability, '在2001-4000名之间');
        } else {
          probability = 85;
          customDescription = getProbabilityDescription(probability, '在0-2000名之间');
        }
      }
    } else if (reachedLastYearLowestRank && reachedTwoYearsBeforeAvgRank && reachedThreeYearsBeforeAvgRank) {
      // 84%~80%
      const twoAndThreeYearsBeforeAvgRank = averageOf(twoYearsBeforeInfo['averageRank'], threeYearsBeforeInfo['averageRank']);
      hints.push({ title: '前年和大前年的录取平均分名次的平均值', value: parseInt(twoAndThreeYearsBeforeAvgRank) });
      const rankGap = twoAndThreeYearsBeforeAvgRank - currentRank;
      if (twoAndThreeYearsBeforeAvgRank < 100) {
        hints.push({ title: '学校等级', value: '前年和大前年的录取平均分名次的平均值在第1-99名（清北复交等）' });
        if (rankGap > 12) {
          probability = 84;
          customDescription = getProbabilityDescription(probability, '在12名之上');
        } else if (rankGap > 9) {
          probability = 83;
          customDescription = getProbabilityDescription(probability, '在10-12名之间');
        } else if (rankGap > 6) {
          probability = 82;
          customDescription = getProbabilityDescription(probability, '在7-9名之间');
        } else if (rankGap > 3) {
          probability = 81;
          customDescription = getProbabilityDescription(probability, '在4-6名之间');
        } else {
          probability = 80;
          customDescription = getProbabilityDescription(probability, '在0-3名之间');
        }
      } else if (twoAndThreeYearsBeforeAvgRank < 1000) {
        hints.push({ title: '学校等级', value: '前年和大前年的录取平均分名次的平均值在第100-999名（少部分985高校）' });
        if (rankGap > 120) {
          probability = 84;
          customDescription = getProbabilityDescription(probability, '在120名之上');
        } else if (rankGap > 90) {
          probability = 83;
          customDescription = getProbabilityDescription(probability, '在91-120名之间');
        } else if (rankGap > 60) {
          probability = 82;
          customDescription = getProbabilityDescription(probability, '在61-90名之间');
        } else if (rankGap > 30) {
          probability = 81;
          customDescription = getProbabilityDescription(probability, '在31-60名之间');
        } else {
          probability = 80;
          customDescription = getProbabilityDescription(probability, '在0-30名之间');
        }
      } else if (twoAndThreeYearsBeforeAvgRank < 10000) {
        hints.push({ title: '学校等级', value: '前年和大前年的录取平均分名次的平均值在第1000-10000名（大部分985和211高校）' });
        if (rankGap > 800) {
          probability = 84;
          customDescription = getProbabilityDescription(probability, '在800名之上');
        } else if (rankGap > 600) {
          probability = 833;
          customDescription = getProbabilityDescription(probability, '在601-800名之间');
        } else if (rankGap > 400) {
          probability = 822;
          customDescription = getProbabilityDescription(probability, '在401-600名之间');
        } else if (rankGap > 200) {
          probability = 811;
          customDescription = getProbabilityDescription(probability, '在201-400名之间');
        } else {
          probability = 80;
          customDescription = getProbabilityDescription(probability, '在0-200名之间');
        }
      } else {
        hints.push({ title: '学校等级', value: '前年和大前年的录取平均分名次的平均值在第10000名以上（大部分双非高校）' });
        if (rankGap > 8000) {
          probability = 84;
          customDescription = getProbabilityDescription(probability, '在8000名之上');
        } else if (rankGap > 6000) {
          probability = 83;
          customDescription = getProbabilityDescription(probability, '在6001-8000名之间');
        } else if (rankGap > 4000) {
          probability = 82;
          customDescription = getProbabilityDescription(probability, '在4001-6000名之间');
        } else if (rankGap > 2000) {
          probability = 81;
          customDescription = getProbabilityDescription(probability, '在2001-4000名之间');
        } else {
          probability = 80;
          customDescription = getProbabilityDescription(probability, '在0-2000名之间');
        }
      }
    } else if (reachedLastYearAvgRank && reachedTwoYearsBeforeLowestRank && reachedThreeYearsBeforeLowestRank) {
      // 79%~75%
      const lastYearAvgRank = lastYearInfo['averageRank'];
      const rankGap = lastYearAvgRank - currentRank;
      if (lastYearAvgRank < 100) {
        hints.push({ title: '学校等级', value: '去年录取平均分名次的平均值在第1-99名（清北复交等）' });
        if (rankGap > 12) {
          probability = 79;
          customDescription = getProbabilityDescription(probability, '在12名之上');
        } else if (rankGap > 9) {
          probability = 78;
          customDescription = getProbabilityDescription(probability, '在10-12名之间');
        } else if (rankGap > 6) {
          probability = 77;
          customDescription = getProbabilityDescription(probability, '在7-9名之间');
        } else if (rankGap > 3) {
          probability = 76;
          customDescription = getProbabilityDescription(probability, '在4-6名之间');
        } else {
          probability = 75;
          customDescription = getProbabilityDescription(probability, '在0-3名之间');
        }
      } else if (lastYearAvgRank < 1000) {
        hints.push({ title: '学校等级', value: '去年录取平均分名次的平均值在第100-999名（少部分985高校）' });
        if (rankGap > 120) {
          probability = 79;
          customDescription = getProbabilityDescription(probability, '在120名之上');
        } else if (rankGap > 90) {
          probability = 78;
          customDescription = getProbabilityDescription(probability, '在91-120名之间');
        } else if (rankGap > 60) {
          probability = 77;
          customDescription = getProbabilityDescription(probability, '在61-90名之间');
        } else if (rankGap > 30) {
          probability = 76;
          customDescription = getProbabilityDescription(probability, '在31-60名之间');
        } else {
          probability = 75;
          customDescription = getProbabilityDescription(probability, '在0-30名之间');
        }
      } else if (lastYearAvgRank < 10000) {
        hints.push({ title: '学校等级', value: '去年录取平均分名次的平均值在第1000-10000名（大部分985和211高校）' });
        if (rankGap > 800) {
          probability = 79;
          customDescription = getProbabilityDescription(probability, '在800名之上');
        } else if (rankGap > 600) {
          probability = 78;
          customDescription = getProbabilityDescription(probability, '在601-800名之间');
        } else if (rankGap > 400) {
          probability = 77;
          customDescription = getProbabilityDescription(probability, '在401-600名之间');
        } else if (rankGap > 200) {
          probability = 76;
          customDescription = getProbabilityDescription(probability, '在201-400名之间');
        } else {
          probability = 75;
          customDescription = getProbabilityDescription(probability, '在0-200名之间');
        }
      } else {
        hints.push({ title: '学校等级', value: '去年录取平均分名次的平均值在第10000名以上（大部分双非高校）' });
        if (rankGap > 8000) {
          probability = 79;
          customDescription = getProbabilityDescription(probability, '在8000名之上');
        } else if (rankGap > 6000) {
          probability = 78;
          customDescription = getProbabilityDescription(probability, '在6001-8000名之间');
        } else if (rankGap > 4000) {
          probability = 77;
          customDescription = getProbabilityDescription(probability, '在4001-6000名之间');
        } else if (rankGap > 2000) {
          probability = 76;
          customDescription = getProbabilityDescription(probability, '在2001-4000名之间');
        } else {
          probability = 75;
          customDescription = getProbabilityDescription(probability, '在0-2000名之间');
        }
      }
    } else if (reachedLastYearLowestRank && reachedTwoYearsBeforeLowestRank && reachedThreeYearsBeforeLowestRank) {
      // 74%~70%
      const threeYearsLowestRank = averageOf(lastYearInfo['lowestRank'], twoYearsBeforeInfo['lowestRank'], threeYearsBeforeInfo['lowestRank']);
      hints.push({ title: '三年的录取最低分名次的平均值', value: parseInt(threeYearsLowestRank) });
      const rankGap = threeYearsLowestRank - currentRank;
      if (threeYearsLowestRank < 100) {
        hints.push({ title: '学校等级', value: '三年的录取最低分名次的平均值在第1-99名（清北复交等）' });
        if (rankGap > 12) {
          probability = 74;
          customDescription = getProbabilityDescription(probability, '在12名之上');
        }
        else if (rankGap > 9) {
          probability = 73;
          customDescription = getProbabilityDescription(probability, '在10-12名之间');
        }
        else if (rankGap > 6) {
          probability = 72;
          customDescription = getProbabilityDescription(probability, '在7-9名之间');
        }
        else if (rankGap > 3) {
          probability = 71;
          customDescription = getProbabilityDescription(probability, '在4-6名之间');
        }
        else {
          probability = 70;
          customDescription = getProbabilityDescription(probability, '在0-3名之间');
        }
      } else if (threeYearsLowestRank < 1000) {
        hints.push({ title: '学校等级', value: '三年的录取最低分名次的平均值在第100-999名（少部分985高校）' });
        if (rankGap > 120) {
          probability = 74;
          (probability, '在120名之上');
        } else if (rankGap > 90) {
          probability = 73;
          customDescription = getProbabilityDescription(probability, '在91-120名之间');
        } else if (rankGap > 60) {
          probability = 72;
          customDescription = getProbabilityDescription(probability, '在61-90名之间');
        } else if (rankGap > 30) {
          probability = 71;
          customDescription = getProbabilityDescription(probability, '在31-60名之间');
        } else {
          probability = 70;
          customDescription = getProbabilityDescription(probability, '在0-30名之间');
        }
      } else if (threeYearsLowestRank < 10000) {
        hints.push({ title: '学校等级', value: '三年的录取最低分名次的平均值在第1000-10000名（大部分985和211高校）' });
        if (rankGap > 800) {
          probability = 74;
          customDescription = getProbabilityDescription(probability, '在800名之上');
        }
        else if (rankGap > 600) {
          probability = 73;
          customDescription = getProbabilityDescription(probability, '在601-800名之间');
        }
        else if (rankGap > 400) {
          probability = 72;
          customDescription = getProbabilityDescription(probability, '在401-600名之间');
        }
        else if (rankGap > 200) {
          probability = 71;
          customDescription = getProbabilityDescription(probability, '在201-400名之间');
        }
        else {
          probability = 70;
          customDescription = getProbabilityDescription(probability, '在0-200名之间');
        }
      } else {
        hints.push({ title: '学校等级', value: '三年的录取最低分名次的平均值在第10000名以上（大部分双非高校）' });
        if (rankGap > 8000) {
          probability = 74;
          customDescription = getProbabilityDescription(probability, '在8000名之上');
        }
        else if (rankGap > 6000) {
          probability = 73;
          customDescription = getProbabilityDescription(probability, '在6001-8000名之间');
        }
        else if (rankGap > 4000) {
          probability = 72;
          customDescription = getProbabilityDescription(probability, '在4001-6000名之间');
        }
        else if (rankGap > 2000) {
          probability = 71;
          customDescription = getProbabilityDescription(probability, '在2001-4000名之间');
        }
        else {
          probability = 70;
          customDescription = getProbabilityDescription(probability, '在0-2000名之间');
        }
      }
    } else if (reachedLastYearLowestRank && reachedTwoYearsBeforeLowestRank) {
      // 69%~65%
      const lastAndTwoYearsBeforeLowestRank = averageOf(lastYearInfo['lowestRank'], twoYearsBeforeInfo['lowestRank']);
      hints.push({ title: '去年、前年的录取最低分名次平均值', value: parseInt(lastAndTwoYearsBeforeLowestRank) });
      const rankGap = lastAndTwoYearsBeforeLowestRank - currentRank;
      if (rankGap >= 4000) probability = 69;
      else if (rankGap >= 3000) probability = 68;
      else if (rankGap >= 2000) probability = 67;
      else if (rankGap >= 1000) probability = 66;
      else probability = 65;
    } else if (reachedLastYearLowestRank && reachedThreeYearsBeforeLowestRank) {
      // 64%~60%
      const lastAndThreeYearsBeforeLowestRank = averageOf(lastYearInfo['lowestRank'], threeYearsBeforeInfo['lowestRank']);
      const rankGap = lastAndThreeYearsBeforeLowestRank - currentRank;
      if (rankGap >= 4000) probability = 64;
      else if (rankGap >= 3000) probability = 63;
      else if (rankGap >= 2000) probability = 62;
      else if (rankGap >= 1000) probability = 61;
      else probability = 60;
    } else if (reachedLastYearLowestRank) {
      // 59%~55%
      const rankGap = lastYearInfo['lowestRank'] - currentRank;
      if (rankGap >= 4000) probability = 59;
      else if (rankGap >= 3000) probability = 58;
      else if (rankGap >= 2000) probability = 57;
      else if (rankGap >= 1000) probability = 56;
      else probability = 55;
    } else if (reachedTwoYearsBeforeLowestRank && reachedThreeYearsBeforeLowestRank) {
      // 54%~50%
      const twoAndThreeYearsBeforeLowestRank = averageOf(twoYearsBeforeInfo['lowestRank'], threeYearsBeforeInfo['lowestRank']);
      hints.push({ title: '前年、大前年的录取最低分名次平均值', value: parseInt(twoAndThreeYearsBeforeLowestRank) });
      const rankGap = twoAndThreeYearsBeforeLowestRank - currentRank;
      if (rankGap >= 4000) probability = 54;
      else if (rankGap >= 3000) probability = 53;
      else if (rankGap >= 2000) probability = 52;
      else if (rankGap >= 1000) probability = 51;
      else probability = 50;
    } else if (reachedTwoYearsBeforeLowestRank) {
      // 49%~45%
      const rankGap = twoYearsBeforeInfo['lowestRank'] - currentRank;
      if (rankGap >= 4000) probability = 49;
      else if (rankGap >= 3000) probability = 48;
      else if (rankGap >= 2000) probability = 47;
      else if (rankGap >= 1000) probability = 46;
      else probability = 45;
    } else if (reachedThreeYearsBeforeLowestRank) {
      // 44%~40%
      const rankGap = threeYearsBeforeInfo['lowestRank'] - currentRank;
      if (rankGap >= 4000) probability = 44;
      else if (rankGap >= 3000) probability = 43;
      else if (rankGap >= 2000) probability = 42;
      else if (rankGap >= 1000) probability = 41;
      else probability = 40;
    } else {
      const threeYearsLowestRank = averageOf(lastYearInfo['lowestRank'], twoYearsBeforeInfo['lowestRank'], threeYearsBeforeInfo['lowestRank']);
      hints.push({ title: '三年录取最低分名次的平均值', value: parseInt(threeYearsLowestRank) });
      const rankGap = threeYearsLowestRank - currentRank;
      if (rankGap > -1000) probability = 30;
      else if (rankGap > -2000) probability = 29;
      else if (rankGap > -3000) probability = 28;
      else if (rankGap > -4000) probability = 27;
      else if (rankGap > -5000) probability = 26;
      else if (rankGap > -6000) probability = 25;
      else if (rankGap > -7000) probability = 24;
      else if (rankGap > -8000) probability = 23;
      else if (rankGap > -9000) probability = 22;
      else if (rankGap > -10000) probability = 21;
      else if (rankGap > -11000) probability = 20;
      else if (rankGap > -12000) probability = 19;
      else if (rankGap > -13000) probability = 18;
      else if (rankGap > -14000) probability = 17;
      else if (rankGap > -15000) probability = 16;
      else if (rankGap > -16000) probability = 15;
      else if (rankGap > -17000) probability = 14;
      else if (rankGap > -18000) probability = 13;
      else if (rankGap > -19000) probability = 12;
      else if (rankGap > -20000) probability = 11;
      else if (rankGap > -30000) probability = 10;
      else if (rankGap > -40000) probability = 5;
      else probability = 1;
    }
  }
  const description = customDescription || probabilityLevelStorage.get('probability', probability)['description'];
  return {
    probability,
    description,
    detail: { // only visable by teachers
      rank: currentRank,
      yearGroupedInfos,
      hints
    }
  };
}

module.exports = {
  admissionAnalyse
}
