function instanceType(instance) {
  return instance && (instance.constructor ? instance.constructor.name : initialToUpper(typeof instance));
}

function initialToUpper(input) {
  let ret = '';
  let isFirst = true;
  for (let c of input) {
    ret += (isFirst ? c.toUpperCase() : c);
    isFirst = false;
  }
  return ret;
}

function objectMap(object, mapValueFn, mapKeyFn) {
  mapKeyFn = mapKeyFn || (x => x);
  return Object.keys(object).reduce((result, key) => {
    result[mapKeyFn(key)] = mapValueFn(object[key])
    return result
  }, {});
}

function underScoreToCamalCase(input) {
  let lastChar = '';
  let ret = '';
  for (let c of input) {
    if (c === '_') {
      lastChar = c;
    } else if (lastChar === '_') {
      lastChar = c;
      ret += c.toUpperCase();
    } else {
      ret += c;
    }
  }
  return ret;
}

function camelCaseToUnderScore(input) {
  let ret = '';
  for (let c of input) {
    if (/[A-Z]/.test(c)) {
      ret += '_' + c.toLowerCase();
    } else if (c !== '_') {
      ret += c;
    }
  }
  return ret;
}

function averageOf(...values) {
  let sum = 0, count = 0;
  values.forEach(x => {
    let v = parseInt(x);
    if (!isNaN(v)) {
      sum += v;
      count++;
    }
  });
  return sum / count;
}

function rangeFrom(start, end) {
  return Array(end + 1 - start).fill(start).map((_, i) => start + i);
}

function assertAndThrow(condition, message) {
  if (condition !== true) throw Error(message);
}

module.exports = {
  wrapSyncOrAsyncFunction(func, wrapAsync, wrapSync) {
    if (!f instanceof Function) {
      return undefined;
    }
    return (f.constructor.name === 'AsyncFunction') ? wrapAsync(func) : wrapSync(func);
  },
  mapObjectKeys(inputObj, converter) {
    if (instanceType(inputObj) !== 'Object' || instanceType(converter) !== 'Function') {
      return inputObj;
    }
    return objectMap(inputObj, x => x, converter);
  },
  keyMapper(keyMapping) {
    return (k) => keyMapping[k] || k;
  },
  groupBy(objs, groupKey) {
    const groupMap = {};
    objs.forEach(obj => {
      const key = (groupKey instanceof Function) ? groupKey(obj) : obj[groupKey.toString()];
      let group = groupMap[key];
      if (!group) {
        group = [];
        groupMap[key] = group;
      }
      group.push(obj);
    });
    return groupMap;
  },
  instanceType,
  objectMap,
  underScoreToCamalCase,
  camelCaseToUnderScore,
  averageOf,
  rangeFrom,
  assertAndThrow,
  currentDatetimeString() {
    const dt = new Date();
    return `${dt.getFullYear()}-${dt.getMonth() + 1}-${dt.getDate()} ${dt.getHours()}:${dt.getMinutes()}:${dt.getSeconds()}.${dt.getMilliseconds()}`;
  },
  isInEnum(enumObj, input) {
    return Object.keys(enumObj).find(k => enumObj[k] === input) !== undefined;
  },
  isMobile(input) {
    return /^[1][3,4,5,7,8,9][0-9]{9}$/.test(input);
  },
  mergeInfo(fields, exisitedInfo, updateInfo) {
    const mergedInfo = {};
    fields.forEach(field => {
      mergedInfo[field] = updateInfo[field] !== undefined ? updateInfo[field] : exisitedInfo[field];
    });
    return mergedInfo;
  },
  parsePagination(pagination) {
    const { pageNo, pageSize = 10 } = pagination;
    assertAndThrow(pageNo > 0 && pageSize > 0, 'illegal pagination param');
    return {
      offset: (pageNo - 1) * pageSize,
      limit: pageSize,
      ...pagination
    };
  }
}