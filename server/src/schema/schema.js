const { gql } = require('apollo-server');

const typeDefs = gql`
  type Query {
    myProfile: User
    myAdminProfile: Admin
    provinces: [Province]
    provinceSchools(
      """
      province id
      """
      province: Int!,
      """
      admissionQuery
      """
      admissionQuery: AdmissionQuery
    ): [School] 
    school(
      """
      school id
      """
      id: Int!,
      """
      admissionQuery
      """
      admissionQuery: AdmissionQuery
    ): School
    scoreRanking(
      """ 
      文理分科，0-理科，1-文科
      """
      category: Int!, 
      year: Int!,
      """ 
      """
      sumRule: Int,
      score: Int
    ): [ScoreRanking]
    # analyse
    schoolApplyAnalyse(
      year: Int!
      score: Int! # TODO retrived from login user info
      bonusScore: Int # TODO retrived from login user info
      schoolId: Int! 
      category: Int! # 
      batch: Int!
      schoolMajors: [String]
      applyPriotiy: Int # TODO used in level apply analyses
    ): SchoolAnalyseResult
    majorApplyAnalyse(
      year: Int!
      score: Int! # TODO retrived from login user info
      bonusScore: Int # TODO retrived from login user info
      schoolId: Int! 
      schoolMajor: String!
      category: Int! # 
      batch: Int!
      applyPriotiy: Int # TODO used in level apply analyses
    ): ApplyAnalysis
    # recommend
    schoolRecommend(
      year: Int!
      score: Int! # TODO retrived from login user info
      bonusScore: Int # TODO retrived from login user info
      category: Int! # 
      batch: Int!
      # province: Int! 
      filters: [SearchFilter]
      query: String
      topN: Int
    ): [RecommendResultGroup]
    # article APIs
    articles(
      keys: [String]
      type: String
      pagination: Pagination
    ): [Article]
    article(
      id: Int
      key: String
    ): Article
    # search APIs
    schoolSearch(
      query: String
      category: Int
      batch: Int,
      filters: [SearchFilter]
    ): [School]
    # user related admin APIs
    users(
      # TODO query filter
      pagination: Pagination
    ): [User]
    user(
      id: String
    ): User
    # simApply list
    simApplications(
      status: Int
    ): [SimApplication]
    # teacher
    teachers(
      # TODO query filter
      pagination: Pagination
    ): [Teacher]
    teacher(
      id: String!
    ): Teacher
    # invite code related query APIs
    inviteCodeBatches(
      pagination: Pagination
    ): [InviteCodeBatch]
    inviteCodeBatch(id: Int): InviteCodeBatch
    inviteCode(code: String): InviteCode
    # admin APIs
    admins(
      # TODO query filter
      pagination: Pagination
    ): [Admin]
    admin(
      id: String
    ): Admin
    # system config query APIs
    systemConfigList(
      pagination: Pagination
      keyword: String
    ): [SystemConfig]
  }

  input Pagination {
    pageNo: Int
    pageSize: Int
    orderBy: String
    orderType: String
  }

  input SearchFilter {
    key: String!
    value: String!
    op: String
  }

  type Mutation {
    # user related
    passwordRegister(
      mobile: String!
      password: String!
      realName: String
      gender: Int
      wechat: String
      email: String
    ): AuthResult
    inviteCodeRegister(
      mobile: String!
      batchId: Int!
      # specifiedInviteCode: String
    ): InviteCodeRegisterResult
    login(
      account: String!
      password: String!
    ): AuthResult
    activateInviteCodeAccount(
      password: String!
      candidateInfo: UserCandidateUpdates!
      realName: String
      gender: Int
      wechat: String
      email: String
    ): AuthResult
    modifyMyProfile(
      mobile: String
      realName: String
      gender: Int
      email: String
      wechat: String
      candidateInfo: UserCandidateUpdates
    ): UpdateResult
    # simApply related APIs
    saveSimApplication(
      content: String!
    ): UpdateResult
    submitSimApplication(
      content: String!
    ): UpdateResult
    # admin user-related APIs
    changeUserType(
      userId: String!
      type: Int!
      reason: String
    ): UpdateResult
    adminModifyUserInfo(
      userId: String!
      realName: String
      gender: Int
      email: String
      wechat: String
      candidateInfo: UserCandidateUpdates
    ): UpdateResult
    # invite code related APIs
    addInviteCodeBatch(
      name: String!
      codeCount: Int
    ): UpdateResult
    adminRegisterPrivilegedInviteCode(
      mobile: String!
      specifiedInviteCode: String
    ): InviteCodeRegisterResult
    # teacher simApply relaterd APIs
    reviewSimApplication(
      userId: String!
      status: Int!
      comment: String!
    ): UpdateResult
    # admin APIS
    createAdmin(
      mobile: String!
      type: Int!
      realName: String
      gender: Int
      wechat: String
      email: String
      extraPrivileges: [String]
    ): Admin
    modifyAdmin(
      userId: String!
      type: Int
      mobile: String
      realName: String
      gender: Int
      email: String
      wechat: String
      extraPrivileges: [String]
    ): UpdateResult
    # Edit related
    # article edit APIs
    createArticle(
      article: ArticleCreation!
    ): UpdateResult
    modifyArticle(
      updates: ArticleUpdates!
    ): UpdateResult
    # school edit APIs
    modifySchoolAdmissionInfo(
      query: SchoolAdmissionInfoQuery!
      updates: SchoolAdmissionInfoUpdates!
    ): UpdateResult
    modifyMajorAdmissionInfo(
      query: MajorAdmissionInfoQuery! 
      updates: MajorAdmissionInfoUpdates!
    ): UpdateResult
    modifySchoolInfo(
      id: Int!
      updates: SchoolInfoUpdates!
    ): UpdateResult
    # system config mutate APIs
    setSystemConfig(
      key: String!
      value: String!
    ): UpdateResult
    addSystemConfig(
      record: AddSystemConfig
    ): UpdateResult
    modifySystemConfig(
      record: ModifySystemConfig
    ): UpdateResult
  }

  # types
  type AuthResult {
    token: String
    user: User
  }

  type InviteCodeRegisterResult {
    token: String
    user: User
    inviteCode: String
  }

  type User {
    id: String
    type: UserType
    activated: Boolean
    name: String
    realName: String
    mobile: String
    email: String
    wechat: String
    gender: Gender
    candidateInfo: UserCandidateInfo
    studentInfo: StudentInfo
    addTime: String # TODO: not needed?
    updateTime: String # TODO: not needed?
  }

  type UserType {
    id: Int
    name: String
  }

  type UserCandidateInfo {
    category: Int
    ethnicity: Int
    year: Int
    score: Int
    bonusWithinProvince: Int
    bonusInterProvince: Int
  }

  type Gender {
    id: Int
    name: String
  }

  input UserCandidateUpdates { # TODO: set fragments
    category: Int
    ethnicity: Int
    year: Int
    score: Int
    bonusWithinProvince: Int
    bonusInterProvince: Int
  }

  type Article {
    id: Int
    key: String
    type: String
    title: String
    author: String
    thumbnail: String
    abstract: String
    content: String
    lastEditor: String
    addTime: String
    updateTime: String
  }

  type InviteCode {
    code: String
    batchId: Int
    ownerId: String
    owner: User
    addTime: String
    updateTime: String
  }

  type InviteCodeBatch {
    id: Int
    name: String
    type: Int
    creatorId: String
    creator: User
    codeCount: Int
    remainCodeCount: Int
    dipatchedCodes: [InviteCode]
    addTime: String
    updateTime: String
  }

  type Admin {
    userId: String
    type: AdminType
    name: String
    mobile: String
    realName: String
    email: String
    wechat: String
    privileges: [AdminPrivilege]
    addTime: String # TODO: not needed?
    updateTime: String # TODO: not needed?
  }

  type AdminType {
    id: Int
    name: String
  }

  type AdminPrivilege {
    key: String
    name: String
  }

  type SystemConfig {
    key: String
    title: String
    value: String
    editor: String
    addTime: String # TODO: not needed?
    updateTime: String # TODO: not needed?
  }

  input AddSystemConfig {
    key: String!
    title: String!
    value: String!
  }

  input ModifySystemConfig {
    key: String!
    title: String
    value: String!
  }

  input SchoolAdmissionInfoQuery {
    schoolId: Int!
    category: Int!
    batch: Int!
    year: Int!
  }

  input SchoolAdmissionInfoUpdates {
    recruitCount: Int
    candidateCount: Int
    enrollCount: Int
    lowestScore: Int
    lowestRank: Int
    averageScore: Int
    averageRank: Int
  }

  input MajorAdmissionInfoQuery {
    schoolId: Int!
    category: Int!
    batch: Int!
    year: Int!
    schoolMajor: String!
  }

  input MajorAdmissionInfoUpdates {
    recruitCount: Int
    reapplyCount: Int
    lowestScore: Int
    lowestRank: Int
    averageScore: Int
    averageRank: Int
    scoreDist: String
  }

  type UpdateResult {
    code: Int
    message: String
  }

  input AdmissionQuery {
    """
    文理分科，0-理科，1-文科
    """
    category: Int!
    """
    录取批次，0-第一批，1-第二批，2-第一批提前批预科，3-第二批提前批预科
    """
    batch: Int!
    """
    query year range
    """
    years: [Int]
  }

  input SchoolInfoUpdates {
    name: String
    province: Int
    rawIntro: String
    attributes: SchoolAttributesUpdates
  }

  input SchoolAttributesUpdates {
    ethnic_bonus: Boolean
    major_score_ladder: String
  }

  input ArticleCreation {
    key: String
    type: String!
    title: String!
    thumbnail: String
    abstract: String
    content: String!
  }
  
  input ArticleUpdates {
    id: Int!
    key: String
    title: String
    thumbnail: String
    abstract: String
    content: String
  }

  type Category {
    id: Int
    name: String
  }

  type Batch {
    id: Int
    name: String
  }

  type School {
    id: Int
    name: String
    province: Province
    """
    introduction
    """
    rawIntro: String
    attributes: SchoolAttributes
    """
    录取文理科
    """
    categories: [Category]
    """
    录取批次
    """
    batches: [Batch]
    """
    学校录取历史
    """
    schoolAdmissionInfos: [SchoolAdmissionInfo]
    """
    专业录取历史
    """
    majorAdmissionInfos: [MajorAdmissionInfo]
  }

  type Province {
    id: Int
    name: String
    abbreviation: String
  }

  type SchoolAttributes {
    ethnic_bonus: Boolean
    major_score_ladder: String
  }

  type SchoolAdmissionInfo {
    """
    年份
    """
    year: Int
    """
    计划招生数
    """
    recruitCount: Int
    """
    投档数
    """
    candidateCount: Int
    """
    录取数
    """
    enrollCount: Int
    """
    录取最低分
    """
    lowestScore: Int
    """
    录取最低分名次
    """
    lowestRank: Int
    """
    录取平均分
    """
    averageScore: Int
    """
    录取平均分名次
    """
    averageRank: Int
  }

  type MajorAdmissionInfo {
    """
    年份
    """
    year: Int
    """
    专业名称
    """
    schoolMajor: String
    """
    计划招生数
    """
    recruitCount: Int
    """
    再征集数
    """
    reapplyCount: Int
    """
    录取最低分
    """
    lowestScore: Int
    """
    录取最低分名次
    """
    lowestRank: Int
    """
    录取平均分
    """
    averageScore: Int
    """
    录取平均分名次
    """
    averageRank: Int
    """
    录取分数分布
    """
    scoreDist: String
  }

  type YearAdmissionInfo {
    """
    年份
    """
    year: Int
    """
    分数线
    """
    cutoffScore: Int
    """
    计划招生数
    """
    recruitCount: Int
    """
    投档数
    """
    candidateCount: Int
    """
    录取数
    """
    enrollCount: Int
    """
    录取最低分
    """
    lowestScore: Int
    """
    录取最低分名次
    """
    lowestRank: Int
    """
    录取平均分
    """
    averageScore: Int
    """
    录取平均分名次
    """
    averageRank: Int
    """
    校友会排名
    """
    rankingFromXYH: Int
    """
    武书连排名
    """
    rankingFromWSL: Int
  }

  type SchoolAnalyseResult {
    schoolResult: ApplyAnalysis
    majorResults: [ApplyAnalysis]
    description: String
  }

  type RecommendResultGroup {
    name: String
    description: String
    results: [ApplyAnalysis]
  }

  type ApplyAnalysis {
    name: String
    scoreDescription: String
    probability: Int
    description: String
    detail: ApplyAnalyseDetail
  }

  type ApplyAnalyseDetail {
    rank: Int
    yearGroupedInfos: [YearAdmissionInfo]
    hints: [Hint]
  }

  type Hint {
    title: String
    value: String
  }

  type ScoreRanking {
    year: Int
    score: Int
    rank: Int
  }
  # student related
  type Teacher {
    id: String
    name: String
    realName: String
    title: String
    avatar: String
    briefIntro: String
    description: String
    students: [User]
  }

  type StudentInfo {
    remarks: [StudentRemark]
    simApplications: [SimApplication]
  }

  type StudentRemark {
    id: Int
    status: StudentRemarkStatus
    content: String # HTML format
    addTime: String
    updateTime: String
  }

  type StudentRemarkStatus {
    id: Int
    name: String
  }

  type SimApplication {
    id: Int
    status: SimApplicationStatus
    content: String # TODO: json format to be decided
    comment: String
    addTime: String
    updateTime: String
  }

  type SimApplicationStatus {
    id: Int
    name: String
  }

`
module.exports = typeDefs;