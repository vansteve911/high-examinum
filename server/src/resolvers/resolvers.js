const { AuthenticationError } = require('apollo-server-koa');

const storages = require('../storages');
const { mapObjectKeys, underScoreToCamalCase, camelCaseToUnderScore, keyMapper, groupBy } = require('../utils/common');

const crudService = require('../service/crudService'); // TODO: not directly refer

const authService = require('../service/authService');
const userService = require('../service/userService');
const adminService = require('../service/adminService');
const schoolService = require('../service/schoolService');
const analyseService = require('../service/analyseService');
const simApplyService = require('../service/simApplyService');
const searchService = require('../service/searchService');
const articleService = require('../service/articleService'); 
const inviteRegisterService = require('../service/inviteRegisterService');
const systemConfigService = require('../service/systemConfigService');

const sqlFieldMappingConfig = {
  deserialize: row => mapObjectKeys(row, underScoreToCamalCase),
  serialize: data => mapObjectKeys(data, camelCaseToUnderScore)
}

const provinceService = crudService(storages.province, {
  deserialize: row => mapObjectKeys(row, keyMapper({
    'short_name': 'name'
  }))
});
const schoolHistoryService = crudService(storages.schoolAdmissionHistory, sqlFieldMappingConfig); // TODO: move to schoolService
const majorHistoryService = crudService(storages.majorAdmissionHistory, sqlFieldMappingConfig); // TODO: move to schoolService
const scoreRankingService = crudService(storages.scoreRanking, sqlFieldMappingConfig); // TODO: move to schoolService

const categoryStorage = storages.category;
const batchStorage = storages.batch;

function buildAdmissionQueryFilters(schoolId, admissionQuery, schoolMajor) { // TODO: move to school service
  const { category, batch, years } = admissionQuery;
  const filters = [{ school_id: schoolId, category, batch }];
  if (years) {
    filters.push(['year', 'in', years]);
  }
  if (schoolMajor) {
    filters.push({ school_major: schoolMajor });
  }
  return filters;
}

function checkLoginUserAndGetId(context, ignoreInactiveUser) { // TODO: make it a middleware
  if (!context.authInfo) {
    throw new AuthenticationError('请先登录');
  }
  if (!ignoreInactiveUser && context.authInfo['type'] === authService.ACCOUNT_TYPES.INVITE_CODE_REGISTER['id']) {
    throw new Error('用户未激活');
  }
  if (context.authInfo['userType'] === 0 && context.authInfo['createTime']) {
    const userCreateTime = new Date(context.authInfo['createTime']);
    if (new Date().getTime() - userCreateTime >= 3600 * 2 * 1000) {
      throw new AuthenticationError('试用时间已过，请升级为正式用户');
    }
  }
  return context.authInfo['userId'];
}

async function getLoginUser(context, ignoreInactiveUser = false) { // TODO: make it a middleware (or @directive)
  const userId = checkLoginUserAndGetId(context, ignoreInactiveUser);
  const user = await userService.getOne(userId);
  return user;
}

async function getLoginAdmin(context) {  // TODO: make it a middleware
  const userId = checkLoginUserAndGetId(context, false);
  return adminService.getOne(userId);
}

async function checkAdminAuth(context, adminPrivilege) {
  const loginAdmin = await getLoginAdmin(context);
  if (!loginAdmin) {
    throw new Error('管理员不存在');
  }
  if (adminPrivilege !== undefined) {
    const hasPrivilege = await adminService.hasPrivilege(loginAdmin, adminPrivilege);
    if (!hasPrivilege) {
      throw new Error('没有操作权限');
    }
  }
  return loginAdmin;
}

module.exports = {
  Query: {
    async myProfile(_, params, context) {
      if (!context.authInfo) {
        return null;
      }
      const loginUser = await getLoginUser(context, ignoreInactiveUser = true);
      if (loginUser) {
        loginUser.isActivated = context.authInfo['type'] !== authService.ACCOUNT_TYPES.INVITE_CODE_REGISTER['id'];
      }
      return loginUser;
    },
    async myAdminProfile(_, params, context) {
      if (!context.authInfo) {
        return null;
      }
      return getLoginAdmin(context);
    },
    async provinces(_, params, context) { // TODO params should be defined
      checkLoginUserAndGetId(context); // TODO implemented by directive
      return provinceService.queryList({});
    },
    async provinceSchools(_, { province, admissionQuery }, context) {
      checkLoginUserAndGetId(context); // TODO implemented by directive
      return schoolService.queryByAdmissionFilter({
        filters: { province },
        admissionQuery
      });
    },
    async school(_, { id, admissionQuery }, context) {
      checkLoginUserAndGetId(context); // TODO implemented by directive
      let result = await schoolService.getOne(id);
      if (result && admissionQuery) {
        Object.assign(result, { admissionQuery });
      }
      return result;
    },
    async scoreRanking(_, { category, year, sumRule, score }, context) {
      checkLoginUserAndGetId(context); // TODO implemented by directive
      return scoreRankingService.queryList({ year, category, sumRule: sumRule || 0, score });
    },
    async schoolApplyAnalyse(_, request, context) {
      const loginUser = await getLoginUser(context);
      const { year, score, schoolId, category, batch, schoolMajors, applyPriotiy } = request;
      return analyseService.schoolApplyAnalyse({ 
        year, 
        score, 
        candidateInfo: userService.parseCandidateInfo(loginUser), 
        schoolId, 
        category, 
        batch, 
        schoolMajors, 
        applyPriotiy 
      });
    },
    majorApplyAnalyse: async (_, request, context) => {
      // TODO: D.R.Y.
      const user = await getLoginUser(context); // TODO: use directives
      const { year: currentYear, score, bonusScore, schoolId, schoolMajor, category, batch, applyPriotiy } = request;
      const years = [currentYear - 1, currentYear - 2, currentYear - 3];
      let rankings = await scoreRankingService.queryList({ year: currentYear, category, sumRule: 0, score });
      const currentRank = rankings[0]['rank'];
      let majorAdmissionHistory = await majorHistoryService.queryByFilters(buildAdmissionQueryFilters(schoolId, { category, batch, years }, schoolMajor));
      return analyseService.applyAnalyse(majorAdmissionHistory, currentYear, currentRank);
    },
    // recommend API
    async schoolRecommend(_, { year, score, category, batch, filters, query }, context) {
      const loginUser = await getLoginUser(context);
      return analyseService.schoolRecommend(
        year,
        score,
        // loginUser.candidateInfo,
        userService.parseCandidateInfo(loginUser),
        category,
        batch,
        filters,
        query
      );
    },
    async schoolSearch(_, { query, category, batch, filters }, context) {
      checkLoginUserAndGetId(context);
      return searchService.searchSchool(query, { category, batch }, filters);
    },
    // simApply APIs
    async simApplications(_, { status }, context) {
      const userId = checkLoginUserAndGetId(context);
      return simApplyService.getUserApplications({ userId, status });
    },
    // article query APIs
    async articles(_, param) {
      return articleService.getList(param);
    },
    async article(_, param) {
      return articleService.getOne(param);
    },
    // admin user APIs
    async users(_, { query, pagination }, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.VIEW_USER_INFO);
      return userService.queryUserList({ query, pagination });
    },
    async user(_, id, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.VIEW_USER_INFO);
      return userService.getOne(id);
    },
    // invite code APIs
    async inviteCodeBatches(_, { pagination }, context) {
      // await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.VIEW_USER_INFO); // TODO tmp for test
      return inviteRegisterService.getCodeBatches(pagination);
    },
    async inviteCodeBatch(_, { id }, context) {
      // await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.VIEW_USER_INFO); // TODO tmp for test
      return inviteRegisterService.getCodeBatch(id);
    },
    async inviteCode(_, { code }, context) {
      // await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.VIEW_USER_INFO); // TODO tmp for test
      return inviteRegisterService.getCode(code);
    },
    // admin query APIs
    async admins(_, { query, pagination }, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.VIEW_ADMIN_INFO);
      return adminService.queryAdminList({ query, pagination });
    },
    async admin(_, id, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.VIEW_ADMIN_INFO);
      return adminService.getOne(id);
    },
    // system config query APIs
    async systemConfigList(_, { pagination, keyword }, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.EDIT_SYSTEM_CONFIG);
      return systemConfigService.queryList(pagination, keyword);
    },
  },
  Mutation: {
    // user related operations
    async passwordRegister(_, args, context) {
      if (context.authInfo) {
        throw new Error('您已登录');
      }
      return authService.passwordRegister(args);
    },
    async inviteCodeRegister(_, args, context) {
      if (context.authInfo) {
        throw new Error('您已登录');
      }
      return inviteRegisterService.inviteCodeRegister(args);
    },
    async login(_, args, context) {
      if (context.authInfo) {
        throw new Error('您已登录');
      }
      return authService.login(args)
    },
    async activateInviteCodeAccount(_, { password, candidateInfo, realName, gender, wechat, email }, context) {
      checkLoginUserAndGetId(context, ignoreInactiveUser = true);
      return authService.activateInviteCodeAccount(context.authInfo['account'], password, candidateInfo, { realName, gender, wechat, email });
    },
    async modifyMyProfile(_, updates, context) {
      const userId = checkLoginUserAndGetId(context);
      await userService.modifyInfo(userId, updates);
      return { 'code': 200, 'message': 'ok' }; // TODO: D.R.Y
    },
    // simApply related APIs
    async saveSimApplication(_, { content }, context) {
      const userId = checkLoginUserAndGetId(context);
      await simApplyService.saveApplication(userId, content);
      return { 'code': 200, 'message': 'ok' }; // TODO: D.R.Y
    },
    async submitSimApplication(_, { content }, context) {
      const userId = checkLoginUserAndGetId(context);
      await simApplyService.submitApplication(userId, content);
      return { 'code': 200, 'message': 'ok' }; // TODO: D.R.Y
    },
    // admin user-related APIs
    async changeUserType(_, { userId, type, reason }, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.MODIFY_USER_INFO);
      console.log(`>>> changeUserType: id="${userId}", type=${type}, reason="${reason}"`);
      await userService.changeUserType(userId, type); // TODO 
      return { 'code': 200, 'message': 'ok' }; // TODO: D.R.Y
    },
    async adminModifyUserInfo(_, updates, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.MODIFY_USER_INFO);
      await userService.modifyInfo(updates['userId'], updates);
      return { 'code': 200, 'message': 'ok' }; // TODO: D.R.Y
    },
    // admin invite code APIs
    async addInviteCodeBatch(_, info, context) {
      // const loginAdmin = await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.ADD_INVITE_CODE_BATCH); // TODO tmp for test
      const loginAdmin = { id: 'mockAdmin' }; // TODO tmp mock for test
      await inviteRegisterService.addCodeBatch({ name: info.name, codeCount: info.codeCount, creatorId: loginAdmin.id });
      return { 'code': 200, 'message': 'ok' }; // TODO: D.R.Y
    },
    async adminRegisterPrivilegedInviteCode(_, registerInfo, context) {
      // const loginAdmin = await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.REGISTER_PRIVILEGED_INVITE_CODE); // TODO tmp for test
      const loginAdmin = { id: 'mockAdmin' }; // TODO tmp mock for test
      return inviteRegisterService.adminRegisterPrivilegedInviteCode(registerInfo, loginAdmin);
    },
    // TODO admin simApply relaterd APIs
    async reviewSimApplication(_, { userId, status, comment }, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.REVIEW_SIM_APPLICATION);
      await simApplyService.reviewApplication(userId, status, comment);
      return { 'code': 200, 'message': 'ok' }; // TODO: D.R.Y
    },
    // admin APIs
    async createAdmin(_, args, context) {
      const loginAdmin = await getLoginAdmin(context);
      if (!loginAdmin) {
        throw new Error('管理员未登录');
      }
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.MODIFY_USER_INFO);
      return adminService.createAdmin(loginAdmin, args);
    },
    async modifyAdmin(_, args, context) {
      const loginAdmin = await getLoginAdmin(context);
      if (!loginAdmin) {
        throw new Error('管理员未登录');
      }
      return adminService.modifyAdmin(loginAdmin, args['userId'], args);
    },
    // system config edit APIs
    async setSystemConfig(_, { key, value }, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.EDIT_SYSTEM_CONFIG);
      const userId = checkLoginUserAndGetId(context);
      return systemConfigService.set(key, value, userId);
    },
    async addSystemConfig(_, record, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.EDIT_SYSTEM_CONFIG);
      return systemConfigService.upsert(record, checkLoginUserAndGetId(context));
    },
    async modifySystemConfig(_, record, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.EDIT_SYSTEM_CONFIG);
      return systemConfigService.upsert(record, checkLoginUserAndGetId(context));
    },
    // edit APIs
    async modifySchoolAdmissionInfo(_, { query, updates }, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.EDIT_SCHOOL_INFO);
      return schoolService.modifySchoolAdmissionHistory(query, updates);
    },
    async modifyMajorAdmissionInfo(_, { query, updates }, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.EDIT_SCHOOL_INFO);
      return schoolService.modifyMajorAdmissionHistory(query, updates);
    },
    async modifySchoolInfo(_, { id, updates }, context) {
      await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.EDIT_SCHOOL_INFO);
      return schoolService.modifySchoolInfo(id, updates);
    },
    async createArticle(_, { article: articleCreation }, context) {
      const loginAdmin = await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.EDIT_CONTENT_INFO);
      return articleService.add(articleCreation, loginAdmin);
    },
    async modifyArticle(_, { updates: articleUpdates }, context) {
      const loginAdmin = await checkAdminAuth(context, adminService.ADMIN_PRIVILEGES.EDIT_CONTENT_INFO); 
      return articleService.modify(articleUpdates, loginAdmin);
    },
  },
  School: {
    province: async ({ province }, params, context) => {
      return provinceService.getOne(province);
    },
    categories({ categories }) {
      return categories.split(',').map(cateId => categoryStorage.get('id', cateId));
    },
    batches({ batches }) {
      return batches.split(',').map(batchId => batchStorage.get('id', batchId));
    },
    attributes: schoolService.parseAttributes,
    schoolAdmissionInfos: async ({ id, admissionQuery }, params, context) => {
      if (admissionQuery) {
        return schoolHistoryService.queryByFilters(buildAdmissionQueryFilters(id, admissionQuery));
      } else {
        return [];
      }
    },
    majorAdmissionInfos: async ({ id, admissionQuery }, params, context) => {
      if (admissionQuery) {
        return majorHistoryService.queryByFilters(buildAdmissionQueryFilters(id, admissionQuery));
      } else {
        return [];
      }
    }
  },
  User: {
    type: userService.parseType,
    gender: userService.parseGender,
    candidateInfo: userService.parseCandidateInfo,
    async activated({ id, candidateInfo }, params, context) {
      if (candidateInfo === undefined || candidateInfo === {}) {
        return false;
      }
      const inactiveAccountType = authService.ACCOUNT_TYPES.INVITE_CODE_REGISTER['id'];
      // console.log(id, context.authInfo, id === context.authInfo['userId']); // TODO 
      if (context.authInfo && id === context.authInfo['userId'] && context.authInfo['type'] !== inactiveAccountType) {
        return true;
      }
      const userAccounts = await authService.getUserAccounts(id);
      return userAccounts.find(a => a['type'] !== inactiveAccountType) !== undefined;
    },
  },
  Admin: {
    type: adminService.parseType,
    privileges: adminService.parsePrivileges
  },
  SimApplication: {
    status: simApplyService.parseType
  },
  InviteCodeBatch: {
    async creator({ creatorId }) {
      return userService.getOne(creatorId);
    },
    async dipatchedCodes({ id }) {
      return inviteRegisterService.getCodesByBatch(id);
    }
  },
  InviteCode: {
    async owner({ ownerId }) {
      return userService.getOne(ownerId);
    },
  },
}