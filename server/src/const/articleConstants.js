module.exports = {
  articleType: {
    'dataList': [
      {
        'key': 'intro',
        'name': '首页介绍'
      },
      {
        'key': 'teacher',
        'name': '教师介绍'
      },
      {
        'key': 'announcement',
        'name': '公告'
      },
    ],
    'queryKeys': ['key']
  }
}