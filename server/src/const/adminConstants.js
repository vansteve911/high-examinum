const adminTypes = [
  {
    'id': -1,
    'key': 'INVALID',
    'name': '失效管理员',
    'privileges': [],
  },
  {
    'id': 0,
    'key': 'EDITOR',
    'name': '内容编辑',
    'privileges': [],
  },
  {
    'id': 1,
    'key': 'AUDITOR',
    'name': '审核员',
    'privileges': [],
  },
  {
    'id': 2,
    'key': 'TEACHER',
    'name': '教师',
    'privileges': [],
  },
  {
    'id': 999,
    'key': 'SUPERVISOR',
    'name': '超级管理员',
    'privileges': [],
  },
];
const adminPrivileges = [
  {
    'key': 'CREATE_ADMIN',
    'name': '创建管理员',
    'privilegedTypes': []
  },
  {
    'key': 'MODIFY_ADMIN',
    'name': '修改管理员信息',
    'privilegedTypes': []
  },
  {
    'key': 'VIEW_ADMIN_INFO',
    'name': '查看管理员信息',
    'privilegedTypes': []
  },
  {
    'key': 'EDIT_SYSTEM_CONFIG',
    'name': '修改系统配置',
    'privilegedTypes': []
  },
  {
    'key': 'MODIFY_USER_INFO',
    'name': '修改用户信息',
    'privilegedTypes': [1]
  },
  {
    'key': 'CHANGE_USER_TYPE',
    'name': '变更用户类型',
    'privilegedTypes': [1]
  },
  {
    'key': 'VIEW_USER_INFO',
    'name': '查看用户信息',
    'privilegedTypes': [1]
  },
  {
    'key': 'EDIT_SCHOOL_INFO',
    'name': '编辑学校信息',
    'privilegedTypes': [0]
  },
  {
    'key': 'EDIT_CONTENT_INFO',
    'name': '编辑内容信息',
    'privilegedTypes': [0, 1]
  },
  {
    'key': 'REVIEW_SIM_APPLICATION',
    'name': '审核用户提交的志愿模拟填报',
    'privilegedTypes': [2]
  },
  {
    'key': 'REGISTER_PRIVILEGED_INVITE_CODE',
    'name': '注册特权邀请码用户',
    'privilegedTypes': [1]
  },
  {
    'key': 'ADD_INVITE_CODE_BATCH',
    'name': '新增邀请码批次',
    'privilegedTypes': [1]
  },
].map(info => {
  info['privilegedTypes'].push(999);
  return info;
});

for (let privilege of adminPrivileges) {
  for (let adminTypeId of privilege['privilegedTypes']) {
    const adminTypeInfo = adminTypes.find(t => t['id'] == adminTypeId);
    adminTypeInfo['privileges'].push(privilege['key']);
  }
}

module.exports = {
  adminType: {
    'dataList': adminTypes,
    'queryKeys': ['id', 'key']
  },
  adminPrivilege: {
    'dataList': adminPrivileges,
    'queryKeys': ['key']
  },
}