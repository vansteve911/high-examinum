module.exports = {
  // TODO put province dataList here
  simApplicationStatus: {
    dataList: [
      {
        "id": 0,
        "key": "DRAFT",
        "name": "草稿"
      },
      {
        "id": 1,
        "key": "SUBMITTED",
        "name": "已提交"
      },
      {
        "id": 2,
        "key": "APPROVED",
        "name": "教师已通过"
      },
      {
        "id": 3,
        "key": "DECLINED",
        "name": "教师已驳回"
      },
      {
        "id": -1,
        "key": "DELETED",
        "name": "已删除"
      },
    ],
    queryKeys: ["id", "key"]
  }
}