module.exports = {
  searchTypes: {
    school: 's',
  },
  searchAttributes: {
    // common attributes
    query: 'q',
    keyword: 'kw',
    category: 'cat',
    categories: 'cats',
    batch: 'bat',
    batches: 'bats',
    province: 'prov',
    // school attributes
    schoolRank: 'sRk',
    schoolMajor: 'sMj',
    schoolFlag985: 'sF985',
    schoolFlag211: 'sF211',
    schoolFlagTopCollege: 'sFyls',
    schoolFlagTopMajor: 'sFylm',
  }
}