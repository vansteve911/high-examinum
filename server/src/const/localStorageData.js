const admissionConstants = require('./admissionConstants');
const userConstants = require('./userConstants');
const adminConstants = require('./adminConstants');
const analyseConstants = require('./analyseConstants');
const studentConstants = require('./studentConstants');
const simApplicationConstants = require('./simApplicationConstants');
const localeConstants = require('./localeConstants');

module.exports = {
  ...admissionConstants,
  ...userConstants,
  ...adminConstants,
  ...analyseConstants,
  ...studentConstants,
  ...simApplicationConstants,
  ...localeConstants,
}