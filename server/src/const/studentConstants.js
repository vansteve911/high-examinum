module.exports = {
  teacherStudentRelationStatus: {
    'dataList': [
      {
        'id': 0,
        'key': 'INVALID',
        'name': '未关联',
      },
      {
        'id': 1,
        'key': 'VALID',
        'name': '关联',
      },
    ],
    'queryKeys': ['id']
  },
  studentRemarkStatus: {
    'dataList': [
      {
        'id': 0,
        'key': 'DRAFT',
        'name': '草稿',
        'nextSteps': [1]
      },
      {
        'id': 1,
        'key': 'SUBMITTED',
        'name': '已提交',
        'nextSteps': [2]
      },
      {
        'id': 2,
        'key': 'ABORTED',
        'name': '已废弃',
        'nextSteps': []
      },
      {
        'id': -1,
        'key': 'DELETED',
        'name': '管理员已删除',
        'nextSteps': []
      }
    ],
    'queryKeys': ['id']
  },
  simApplicationStatus: {
    'dataList': [
      {
        'id': 0,
        'key': 'DRAFT',
        'name': '草稿',
        'nextSteps': [1]
      },
      {
        'id': 1,
        'key': 'SUBMITTED',
        'name': '学生已提交',
        'nextSteps': [2, 3, 4]
      },
      {
        'id': 2,
        'key': 'REVIEWED',
        'name': '教师已审阅',
        'nextSteps': [3, 4]
      },
      {
        'id': 3,
        'key': 'APPROVED',
        'name': '教师已批准',
        'nextSteps': []
      },
      {
        'id': 4,
        'key': 'DECLINED',
        'name': '教师已驳回',
        'nextSteps': [1]
      },
      {
        'id': -1,
        'key': 'DELETED',
        'name': '管理员已删除',
        'nextSteps': []
      },
    ],
    'queryKeys': ['id']
  },
}