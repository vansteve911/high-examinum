module.exports = {
  accountType: {
    'dataList': [
      {
        'id': 0,
        'key': 'PASSWORD_REGISTER',
        'name': '密码注册',
      },
      {
        'id': 1,
        'key': 'INVITE_CODE_REGISTER',
        'name': '邀请码注册',
      },
    ],
    'queryKeys': ['id', 'key']
  },
  userType: {
    'dataList': [{
      'id': 0,
      'key': 'TEMP_USER',
      'name': '体验用户',
    },
    {
      'id': 1,
      'key': 'FULL_USER',
      'name': '正式用户',
    },
    {
      'id': -1,
      'key': 'DELETED_USER',
      'name': '已删除用户',
    }],
    'queryKeys': ['id', 'key']
  },
  gender: {
    'dataList': [
      {
        'id': -1,
        'key': 'UNKNOWN',
        'name': '未知',
      },
      {
        'id': 0,
        'key': 'MALE',
        'name': '男',
      },
      {
        'id': 1,
        'key': 'FEMALE',
        'name': '女',
      },
    ],
    'queryKeys': ['id', 'key']
  },
}