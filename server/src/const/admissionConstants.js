module.exports = {
  GX_PROVINCE_ID: 45,
  // TODO put province dataList here
  admissionBatch: {
    'dataList': [
      {
        'id': 0,
        'code': '9',
        'name': '本科第一批'
      },
      {
        'id': 1,
        'code': '12',
        'name': '本科第二批'
      },
      {
        'id': 2,
        'code': '14',
        'name': '本科第一批预科'
      },
      {
        'id': 3,
        'code': '15',
        'name': '本科第二批预科B'
      }
    ],
    'queryKeys': [
      'id',
      'code'
    ]
  },
  admissionCategory: {
    'dataList': [
      {
        'id': 0,
        'key': 'like',
        'name': '理工类'
      },
      {
        'id': 1,
        'key': 'wenke',
        'name': '文史类'
      }
    ],
    'queryKeys': [
      'id'
    ]
  },

}