module.exports = {
  yearCutoffScore: {
    'dataList': [{
        "category": 0,
        "batch": 0,
        "year": 2019,
        "score": 509
      },
      {
        "category": 0,
        "batch": 0,
        "year": 2018,
        "score": 513
      },
      {
        "category": 0,
        "batch": 0,
        "year": 2017,
        "score": 473
      },
      {
        "category": 0,
        "batch": 0,
        "year": 2016,
        "score": 502
      },
      {
        "category": 0,
        "batch": 0,
        "year": 2015,
        "score": 480
      },
      {
        "category": 0,
        "batch": 0,
        "year": 2014,
        "score": 520
      },
      {
        "category": 0,
        "batch": 0,
        "year": 2013,
        "score": 510
      },
      {
        "category": 0,
        "batch": 0,
        "year": 2012,
        "score": 528
      },
      {
        "category": 0,
        "batch": 0,
        "year": 2011,
        "score": 506
      },
      {
        "category": 0,
        "batch": 0,
        "year": 2010,
        "score": 500
      },
      {
        "category": 0,
        "batch": 0,
        "year": 2009,
        "score": 507
      },
      {
        "category": 0,
        "batch": 1,
        "year": 2019,
        "score": 347
      },
      {
        "category": 0,
        "batch": 1,
        "year": 2018,
        "score": 345
      },
      {
        "category": 0,
        "batch": 1,
        "year": 2017,
        "score": 318
      },
      {
        "category": 0,
        "batch": 1,
        "year": 2016,
        "score": 333
      },
      {
        "category": 0,
        "batch": 1,
        "year": 2015,
        "score": 320
      },
      {
        "category": 0,
        "batch": 1,
        "year": 2014,
        "score": 407
      },
      {
        "category": 0,
        "batch": 1,
        "year": 2013,
        "score": 413
      },
      {
        "category": 0,
        "batch": 1,
        "year": 2012,
        "score": 444
      },
      {
        "category": 0,
        "batch": 1,
        "year": 2011,
        "score": 414
      },
      {
        "category": 0,
        "batch": 1,
        "year": 2010,
        "score": 433
      },
      {
        "category": 0,
        "batch": 1,
        "year": 2009,
        "score": 443
      },
      {
        "category": 1,
        "batch": 0,
        "year": 2019,
        "score": 521
      },
      {
        "category": 1,
        "batch": 0,
        "year": 2018,
        "score": 547
      },
      {
        "category": 1,
        "batch": 0,
        "year": 2017,
        "score": 535
      },
      {
        "category": 1,
        "batch": 0,
        "year": 2016,
        "score": 545
      },
      {
        "category": 1,
        "batch": 0,
        "year": 2015,
        "score": 530
      },
      {
        "category": 1,
        "batch": 0,
        "year": 2014,
        "score": 550
      },
      {
        "category": 1,
        "batch": 0,
        "year": 2013,
        "score": 541
      },
      {
        "category": 1,
        "batch": 0,
        "year": 2012,
        "score": 544
      },
      {
        "category": 1,
        "batch": 0,
        "year": 2011,
        "score": 519
      },
      {
        "category": 1,
        "batch": 0,
        "year": 2010,
        "score": 510
      },
      {
        "category": 1,
        "batch": 0,
        "year": 2009,
        "score": 523
      },
      {
        "category": 1,
        "batch": 1,
        "year": 2019,
        "score": 388
      },
      {
        "category": 1,
        "batch": 1,
        "year": 2018,
        "score": 403
      },
      {
        "category": 1,
        "batch": 1,
        "year": 2017,
        "score": 387
      },
      {
        "category": 1,
        "batch": 1,
        "year": 2016,
        "score": 400
      },
      {
        "category": 1,
        "batch": 1,
        "year": 2015,
        "score": 380
      },
      {
        "category": 1,
        "batch": 1,
        "year": 2014,
        "score": 463
      },
      {
        "category": 1,
        "batch": 1,
        "year": 2013,
        "score": 467
      },
      {
        "category": 1,
        "batch": 1,
        "year": 2012,
        "score": 473
      },
      {
        "category": 1,
        "batch": 1,
        "year": 2011,
        "score": 456
      },
      {
        "category": 1,
        "batch": 1,
        "year": 2010,
        "score": 453
      },
      {
        "category": 1,
        "batch": 1,
        "year": 2009,
        "score": 467
      },
      {
        "category": 0,
        "batch": 2,
        "year": 2019,
        "score": 509
      },
      {
        "category": 0,
        "batch": 2,
        "year": 2018,
        "score": 513
      },
      {
        "category": 0,
        "batch": 2,
        "year": 2017,
        "score": 473
      },
      {
        "category": 0,
        "batch": 2,
        "year": 2016,
        "score": 502
      },
      {
        "category": 0,
        "batch": 2,
        "year": 2015,
        "score": 480
      },
      {
        "category": 0,
        "batch": 2,
        "year": 2014,
        "score": 520
      },
      {
        "category": 0,
        "batch": 2,
        "year": 2013,
        "score": 510
      },
      {
        "category": 0,
        "batch": 2,
        "year": 2012,
        "score": 528
      },
      {
        "category": 0,
        "batch": 2,
        "year": 2011,
        "score": 506
      },
      {
        "category": 0,
        "batch": 2,
        "year": 2010,
        "score": 500
      },
      {
        "category": 0,
        "batch": 2,
        "year": 2009,
        "score": 507
      },
      {
        "category": 0,
        "batch": 3,
        "year": 2019,
        "score": 347
      },
      {
        "category": 0,
        "batch": 3,
        "year": 2018,
        "score": 345
      },
      {
        "category": 0,
        "batch": 3,
        "year": 2017,
        "score": 318
      },
      {
        "category": 0,
        "batch": 3,
        "year": 2016,
        "score": 333
      },
      {
        "category": 0,
        "batch": 3,
        "year": 2015,
        "score": 320
      },
      {
        "category": 0,
        "batch": 3,
        "year": 2014,
        "score": 407
      },
      {
        "category": 0,
        "batch": 3,
        "year": 2013,
        "score": 413
      },
      {
        "category": 0,
        "batch": 3,
        "year": 2012,
        "score": 444
      },
      {
        "category": 0,
        "batch": 3,
        "year": 2011,
        "score": 414
      },
      {
        "category": 0,
        "batch": 3,
        "year": 2010,
        "score": 433
      },
      {
        "category": 0,
        "batch": 3,
        "year": 2009,
        "score": 443
      },
      {
        "category": 1,
        "batch": 2,
        "year": 2019,
        "score": 521
      },
      {
        "category": 1,
        "batch": 2,
        "year": 2018,
        "score": 547
      },
      {
        "category": 1,
        "batch": 2,
        "year": 2017,
        "score": 535
      },
      {
        "category": 1,
        "batch": 2,
        "year": 2016,
        "score": 545
      },
      {
        "category": 1,
        "batch": 2,
        "year": 2015,
        "score": 530
      },
      {
        "category": 1,
        "batch": 2,
        "year": 2014,
        "score": 550
      },
      {
        "category": 1,
        "batch": 2,
        "year": 2013,
        "score": 541
      },
      {
        "category": 1,
        "batch": 2,
        "year": 2012,
        "score": 544
      },
      {
        "category": 1,
        "batch": 2,
        "year": 2011,
        "score": 519
      },
      {
        "category": 1,
        "batch": 2,
        "year": 2010,
        "score": 510
      },
      {
        "category": 1,
        "batch": 2,
        "year": 2009,
        "score": 523
      },
      {
        "category": 1,
        "batch": 3,
        "year": 2019,
        "score": 388
      },
      {
        "category": 1,
        "batch": 3,
        "year": 2018,
        "score": 403
      },
      {
        "category": 1,
        "batch": 3,
        "year": 2017,
        "score": 387
      },
      {
        "category": 1,
        "batch": 3,
        "year": 2016,
        "score": 400
      },
      {
        "category": 1,
        "batch": 3,
        "year": 2015,
        "score": 380
      },
      {
        "category": 1,
        "batch": 3,
        "year": 2014,
        "score": 463
      },
      {
        "category": 1,
        "batch": 3,
        "year": 2013,
        "score": 467
      },
      {
        "category": 1,
        "batch": 3,
        "year": 2012,
        "score": 473
      },
      {
        "category": 1,
        "batch": 3,
        "year": 2011,
        "score": 456
      },
      {
        "category": 1,
        "batch": 3,
        "year": 2010,
        "score": 453
      },
      {
        "category": 1,
        "batch": 3,
        "year": 2009,
        "score": 467
      }
    ],
    queryKeys: ["year"]
  },
  probabilityLevel: {
    'dataList': [
      {
        'probability': 1,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在50000名之外。'
      },
      {
        'probability': 5,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在40000-49999名之间。'
      },
      {
        'probability': 10,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在30000-39999名之间。'
      },
      {
        'probability': 15,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在20000-29999名之间。'
      },
      {
        'probability': 20,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在10000-19999名之间。'
      },
      {
        'probability': 21,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在9000-9999名之间。'
      },
      {
        'probability': 22,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在8000-8999名之间。'
      },
      {
        'probability': 23,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在7000-7999名之间。'
      },
      {
        'probability': 24,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在6000-6999名之间。'
      },
      {
        'probability': 25,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在5000-5999名之间。'
      },
      {
        'probability': 26,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在4000-4999名之间。'
      },
      {
        'probability': 27,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在3000-3999名之间。'
      },
      {
        'probability': 28,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在2000-2999名之间。'
      },
      {
        'probability': 29,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在1000-1999名之间。'
      },
      {
        'probability': 30,
        'description': '没有达到最近三年的录取最低分名次，且距离近三年录取最低分名次的平均值在1-999名之间。'
      },

      {
        'probability': 40,
        'description': '只达到大前年的录取最低分名次，但超过大前年的录取最低分名次在1-999名之间。'
      },
      {
        'probability': 41,
        'description': '只达到大前年的录取最低分名次，但超过大前年的录取最低分名次在1000-1999名之间。'
      },
      {
        'probability': 42,
        'description': '只达到大前年的录取最低分名次，但超过大前年的录取最低分名次在2000-2999名之间。'
      },
      {
        'probability': 43,
        'description': '只达到大前年的录取最低分名次，但超过大前年的录取最低分名次在3000-3999名之间。'
      },
      {
        'probability': 44,
        'description': '只达到大前年的录取最低分名次，但超过大前年的录取最低分名次在4000名以上。'
      },

      {
        'probability': 45,
        'description': '只达到前年的录取最低分名次，但超过前年的录取最低分名次在1-999名之间。'
      },
      {
        'probability': 46,
        'description': '只达到前年的录取最低分名次，但超过前年的录取最低分名次在1000-1999名之间。'
      },
      {
        'probability': 47,
        'description': '只达到前年的录取最低分名次，但超过前年的录取最低分名次在2000-2999名之间。'
      },
      {
        'probability': 48,
        'description': '只达到前年的录取最低分名次，但超过前年的录取最低分名次在3000-3999名之间。'
      },
      {
        'probability': 49,
        'description': '只达到前年的录取最低分名次，但超过前年的录取最低分名次在4000名以上。'
      },

      {
        'probability': 50,
        'description': '只达到前年、大前年的录取最低分名次，没有达到去年的录取最低分名次，但超过前年、大前年的录取最低分名次平均值在1-999名之间。'
      },
      {
        'probability': 51,
        'description': '只达到前年、大前年的录取最低分名次，没有达到去年的录取最低分名次，但超过前年、大前年的录取最低分名次平均值在1000-1999名之间。'
      },
      {
        'probability': 52,
        'description': '只达到前年、大前年的录取最低分名次，没有达到去年的录取最低分名次，但超过前年、大前年的录取最低分名次平均值在2000-2999名之间。'
      },
      {
        'probability': 53,
        'description': '只达到前年、大前年的录取最低分名次，没有达到去年的录取最低分名次，但超过前年、大前年的录取最低分名次平均值在3000-3999名之间。'
      },
      {
        'probability': 54,
        'description': '只达到前年、大前年的录取最低分名次，没有达到去年的录取最低分名次，但超过前年、大前年的录取最低分名次平均值在4000名以上。'
      },

      {
        'probability': 55,
        'description': '只达到去年的录取最低分名次，没有达到前年、大前年的录取最低分名次，但超过去年的录取最低分名次在1-999名之间。'
      },
      {
        'probability': 56,
        'description': '只达到去年的录取最低分名次，没有达到前年、大前年的录取最低分名次，但超过去年的录取最低分名次在1000-1999名之间。'
      },
      {
        'probability': 57,
        'description': '只达到去年的录取最低分名次，没有达到前年、大前年的录取最低分名次，但超过去年的录取最低分名次在2000-2999名之间。'
      },
      {
        'probability': 58,
        'description': '只达到去年的录取最低分名次，没有达到前年、大前年的录取最低分名次，但超过去年的录取最低分名次在3000-3999名之间。'
      },
      {
        'probability': 59,
        'description': '只达到去年的录取最低分名次，没有达到前年、大前年的录取最低分名次，但超过去年的录取最低分名次在4000名以上。'
      },

      {
        'probability': 60,
        'description': '既达到去年的录取最低分名次，也达到大前年的录取最低分名次，且超过去年、大前年的录取最低分名次平均值在1-999名之间。'
      },
      {
        'probability': 61,
        'description': '既达到去年的录取最低分名次，也达到大前年的录取最低分名次，且超过去年、大前年的录取最低分名次平均值在1000-1999名之间。'
      },
      {
        'probability': 62,
        'description': '既达到去年的录取最低分名次，也达到大前年的录取最低分名次，且超过去年、大前年的录取最低分名次平均值在2000-2999名之间。'
      },
      {
        'probability': 63,
        'description': '既达到去年的录取最低分名次，也达到大前年的录取最低分名次，且超过去年、大前年的录取最低分名次平均值在3000-3999名之间。'
      },
      {
        'probability': 64,
        'description': '既达到去年的录取最低分名次，也达到大前年的录取最低分名次，且超过去年、大前年的录取最低分名次平均值在4000名以上。'
      },

      {
        'probability': 65,
        'description': '既达到去年的录取最低分名次，也达到前年的录取最低分名次，且超过去年、前年的录取最低分名次平均值在1-999名之间。'
      },
      {
        'probability': 66,
        'description': '既达到去年的录取最低分名次，也达到前年的录取最低分名次，且超过去年、前年的录取最低分名次平均值在1000-1999名之间。'
      },
      {
        'probability': 67,
        'description': '既达到去年的录取最低分名次，也达到前年的录取最低分名次，且超过去年、前年的录取最低分名次平均值在2000-2999名之间。'
      },
      {
        'probability': 68,
        'description': '既达到去年的录取最低分名次，也达到前年的录取最低分名次，且超过去年、前年的录取最低分名次平均值在3000-3999名之间。'
      },
      {
        'probability': 69,
        'description': '既达到去年的录取最低分名次，也达到前年的录取最低分名次，且超过去年、前年的录取最低分名次平均值在4000名以上。'
      },

      {
        'probability': 70,
        'description': '达到最近三年的录取最低分名次，且超过最近三年的录取最低分名次的平均值{RANK_GAP}'
      },
      {
        'probability': 71,
        'description': '达到最近三年的录取最低分名次，且超过最近三年的录取最低分名次的平均值{RANK_GAP}'
      },
      {
        'probability': 72,
        'description': '达到最近三年的录取最低分名次，且超过最近三年的录取最低分名次的平均值{RANK_GAP}'
      },
      {
        'probability': 73,
        'description': '达到最近三年的录取最低分名次，且超过最近三年的录取最低分名次的平均值{RANK_GAP}'
      },
      {
        'probability': 74,
        'description': '达到最近三年的录取最低分名次，且超过最近三年的录取最低分名次的平均值{RANK_GAP}'
      },

      {
        'probability': 75,
        'description': '达到去年的录取平均分名次，同时达到最近三年的录取最低分名次，且超过去年平均分名次{RANK_GAP}'
      },
      {
        'probability': 76,
        'description': '达到去年的录取平均分名次，同时达到最近三年的录取最低分名次，且超过去年平均分名次{RANK_GAP}'
      },
      {
        'probability': 77,
        'description': '达到去年的录取平均分名次，同时达到最近三年的录取最低分名次，且超过去年平均分名次{RANK_GAP}'
      },
      {
        'probability': 78,
        'description': '达到去年的录取平均分名次，同时达到最近三年的录取最低分名次，且超过去年平均分名次{RANK_GAP}'
      },
      {
        'probability': 79,
        'description': '达到去年的录取平均分名次，同时达到最近三年的录取最低分名次，且超过去年平均分名次{RANK_GAP}'
      },

      {
        'probability': 80,
        'description': '达到前年、大前年的录取平均分名次，同时达到去年的录取最低分名次，且超过前年、大前年的录取平均分名次{RANK_GAP}'
      },
      {
        'probability': 81,
        'description': '达到前年、大前年的录取平均分名次，同时达到去年的录取最低分名次，且超过前年、大前年的录取平均分名次{RANK_GAP}'
      },
      {
        'probability': 82,
        'description': '达到前年、大前年的录取平均分名次，同时达到去年的录取最低分名次，且超过前年、大前年的录取平均分名次{RANK_GAP}'
      },
      {
        'probability': 83,
        'description': '达到前年、大前年的录取平均分名次，同时达到去年的录取最低分名次，且超过前年、大前年的录取平均分名次{RANK_GAP}'
      },
      {
        'probability': 84,
        'description': '达到前年、大前年的录取平均分名次，同时达到去年的录取最低分名次，且超过前年、大前年的录取平均分名次{RANK_GAP}'
      },

      {
        'probability': 85,
        'description': '达到去年、大前年的录取平均分名次，同时达到前年的录取最低分名次，且超过去年、大前年的录取平均分名次{RANK_GAP}'
      }, {
        'probability': 86,
        'description': '达到去年、大前年的录取平均分名次，同时达到前年的录取最低分名次，且超过去年、大前年的录取平均分名次{RANK_GAP}'
      }, {
        'probability': 87,
        'description': '达到去年、大前年的录取平均分名次，同时达到前年的录取最低分名次，且超过去年、大前年的录取平均分名次{RANK_GAP}'
      }, {
        'probability': 88,
        'description': '达到去年、大前年的录取平均分名次，同时达到前年的录取最低分名次，且超过去年、大前年的录取平均分名次{RANK_GAP}'
      }, {
        'probability': 89,
        'description': '达到去年、大前年的录取平均分名次，同时达到前年的录取最低分名次，且超过去年、大前年的录取平均分名次{RANK_GAP}'
      },

      {
        'probability': 90,
        'description': '同时达到最近两年的录取平均分名次、达到前年的录取最低分名次，且超过最近两年的录取平均分名次平均值{RANK_GAP}'
      },
      {
        'probability': 91,
        'description': '同时达到最近两年的录取平均分名次、达到前年的录取最低分名次，且超过最近两年的录取平均分名次平均值{RANK_GAP}'
      },
      {
        'probability': 92,
        'description': '同时达到最近两年的录取平均分名次、达到前年的录取最低分名次，且超过最近两年的录取平均分名次平均值{RANK_GAP}'
      },
      {
        'probability': 93,
        'description': '同时达到最近两年的录取平均分名次、达到前年的录取最低分名次，且超过最近两年的录取平均分名次平均值{RANK_GAP}'
      },
      {
        'probability': 94,
        'description': '同时达到最近两年的录取平均分名次、达到前年的录取最低分名次，且超过最近两年的录取平均分名次平均值{RANK_GAP}'
      },

      {
        'probability': 95,
        'description': '同时达到最近三年的录取平均分名次，且超过最近三年的录取平均分名次平均值{RANK_GAP}'
      },
      {
        'probability': 96,
        'description': '同时达到最近三年的录取平均分名次，且超过最近三年的录取平均分名次平均值{RANK_GAP}'
      },
      {
        'probability': 97,
        'description': '同时达到最近三年的录取平均分名次，且超过最近三年的录取平均分名次平均值{RANK_GAP}'
      },
      {
        'probability': 98,
        'description': '同时达到最近三年的录取平均分名次，且超过最近三年的录取平均分名次平均值{RANK_GAP}'
      },
      {
        'probability': 99,
        'description': '同时达到最近三年的录取平均分名次，且超过最近三年的录取平均分名次平均值{RANK_GAP}'
      },
      {
        'probability': -1,
        'description': '该专业为当年新设，无最近三年数据，需要人工参考'
      }
    ],
    'queryKeys': [
      'probability'
    ]
  }
}