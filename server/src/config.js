module.exports = {
  sqlite: {
    dataFile: process.env.SQLITE_DATA_FILE || (__dirname + '/../mockData/high-examinum.db')
  }
}