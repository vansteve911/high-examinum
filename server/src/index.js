const Koa = require('koa');
const koaBodyParser = require('koa-bodyparser');
const koaStatic = require('koa-static');

const { ApolloServer } = require('apollo-server-koa');

const typeDefs = require('./schema/schema');
const resolvers = require('./resolvers/resolvers');

const authService = require('./service/authService');

const app = new Koa();
app.use(koaBodyParser());

// static router
app.use(koaStatic(__dirname + '/../public'));
// graphql integration
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async (req) => {
    return authService.getRequestAuthContext(req);
  },
  formatError: (error) => {
    console.error(error, 'stacktrace: ', error.extensions.exception.stacktrace);
    // if (error.originalError instanceof AuthenticationError) {
    //   return new Error('401');
    // }
    // return new Error('Internal server error');
    return error;
  },
});
server.applyMiddleware({ app });

const port = process.env.SERVER_PORT || 4000;
app.listen(port, () => {
  console.log('server started at port ' + port);
});
