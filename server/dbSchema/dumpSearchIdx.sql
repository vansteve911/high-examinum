insert into `search_inverted_idx` (type, attribute, key, value) select "s", "kw", id, name from school;
insert into `search_inverted_idx` (type, attribute, key, value) select "s", "prov", id, province from school;
insert into `search_inverted_idx` (type, attribute, key, value) select "s", "cats", id, categories from school;
insert into `search_inverted_idx` (type, attribute, key, value) select "s", "bats", id, batches from school;
insert into `search_inverted_idx` (type, attribute, key, value) select "s", "sF211", id, "1" from school where raw_intro like '%211工程%' or raw_intro like '%“211”%' or raw_intro like '%211 工程%';
insert into `search_inverted_idx` (type, attribute, key, value) select "s", "sF211", id, "0" from school where not (raw_intro like '%211工程%' or raw_intro like '%“211”%' or raw_intro like '%211 工程%');
insert into `search_inverted_idx` (type, attribute, key, value) select "s", "sF985", id, "1" from school where raw_intro like '%985%' and raw_intro not like '%1985%';
insert into `search_inverted_idx` (type, attribute, key, value) select "s", "sF985", id, "0" from school where not (raw_intro like '%985%' and raw_intro not like '%1985%');