CREATE TABLE `school` (
  `id` INT PRIMARY KEY,
  `name` CHAR(20) NOT NULL,
  `province` INT NOT NULL,
  `categories` CHAR(30),
  `batches` CHAR(10),
  `raw_intro` TEXT NOT NULL DEFAULT '',
  `attributes` TEXT NOT NULL DEFAULT ''
);

CREATE TABLE `school_admission_history` (
  `school_id` INT NOT NULL,
  `category` INT NOT NULL,
  `batch` INT NOT NULL,
  `year` INT NOT NULL,
  `recruit_count` INT NOT NULL,
  `candidate_count` INT NOT NULL,
  `enroll_count` INT NOT NULL,
  `lowest_score` INT NOT NULL,
  `lowest_rank` INT NOT NULL,
  `average_score` INT NOT NULL,
  `average_rank` INT NOT NULL
);

CREATE TABLE `major_admission_history` (
  `school_id` INT NOT NULL,
  `category` INT NOT NULL,
  `batch` INT NOT NULL,
  `school_major` CHAR(50) NOT NULL,
  `year` INT NOT NULL,
  `recruit_count` INT NOT NULL,
  `reapply_count` INT NOT NULL,
  `lowest_score` INT NOT NULL,
  `lowest_rank` INT NOT NULL,
  `average_score` INT NOT NULL,
  `average_rank` INT NOT NULL,
  `score_dist` TEXT NOT NULL
);

CREATE TABLE `score_ranking` (
  `year` INT NOT NULL,
  `category` INT NOT NULL,
  `sum_rule` INT NOT NULL, -- 0: 总分=总成绩+全国性加分, 1: 总分=总成绩+全国性加分和地方性加分的最高分
  `score` INT NOT NULL,
  `count` INT NOT NULL,
  `accumulated_count` INT NOT NULL,
  `rank` INT NOT NULL
);

CREATE TABLE `account` (
  `account` CHAR(50) NOT NULL, -- unique key, {typePrefix}_{id}
  `user_id` INTEGER NOT NULL, -- generated
  `credential` CHAR(100) DEFAULT NULL,
  `salt` CHAR(50) DEFAULT NULL,
  `type` INT NOT NULL DEFAULT '0', -- 0~9: user, 0: in site, 1: wechat, ... ; 10: admin
  `add_time` TEXT NOT NULL,
  `update_time` TEXT NOT NULL
);

CREATE TABLE `user` (
  `id` CHAR(50) PRIMARY KEY, -- generated
  `type` INT NOT NULL DEFAULT '0', -- 0: temp user, 1: full user
  `name` CHAR(50) NOT NULL,
  `real_name` CHAR(50) DEFAULT NULL, -- 
  `mobile` CHAR(30) NOT NULL DEFAULT '',
  `wechat` CHAR(50) NOT NULL DEFAULT '',
  `email` CHAR(50) NOT NULL DEFAULT '',
  `gender` INT NOT NULL DEFAULT '-1', -- 0: male, 1: female
  `candidate_info` TEXT NOT NULL DEFAULT '{}', -- json format: {category, year, score, ...}
  `add_time` TEXT NOT NULL,
  `update_time` TEXT NOT NULL
);

CREATE TABLE `admin` (
  `user_id` CHAR(50) PRIMARY KEY, -- related to user.id
  `name` CHAR(50) NOT NULL,
  `type` INT NOT NULL DEFAULT '0', -- multi values, 0:editor, 1: auditor, 2: teacher, 999: super admin
  `real_name` CHAR(50) DEFAULT NULL, -- 
  `mobile` CHAR(30) NOT NULL DEFAULT '',
  `wechat` CHAR(50) NOT NULL DEFAULT '',
  `email` CHAR(50) NOT NULL DEFAULT '',
  `gender` INT NOT NULL DEFAULT '-1', -- 0: male, 1: female
  `extra_privileges` TEXT NOT NULL DEFAULT '', 
  `add_time` TEXT NOT NULL,
  `update_time` TEXT NOT NULL
);

-- teacher related
CREATE TABLE `teacher` (
  `user_id` CHAR(50) PRIMARY KEY,
  `name` CHAR(50) NOT NULL,
  `real_name` CHAR(50) NOT NULL,
  `title` CHAR(50) NOT NULL DEFAULT '',
  `avatar` CHAR(200) NOT NULL DEFAULT '',
  `brief_intro` TEXT NOT NULL DEFAULT '',
  `description` TEXT NOT NULL DEFAULT '',
  `add_time` TIMESTAMP,
  `update_time` TIMESTAMP
);

CREATE TABLE `teacher_student_relation` (
  `id` INTEGER PRIMARY KEY,
  `teacher_id` CHAR(50) NOT NULL,
  `student_id` CHAR(50) NOT NULL,
  `status` INT NOT NULL DEFAULT '1', -- 0: not linked，1: linked
  `add_time` TIMESTAMP,
  `update_time` TIMESTAMP
);

CREATE TABLE `student_remark` (
  `id` INTEGER PRIMARY KEY,
  `student_id` CHAR(50) NOT NULL,
  `teacher_id` CHAR(50) NOT NULL,
  `status` INT NOT NULL DEFAULT '0', -- 0: draft, 1: submitted, 2: aborted, -1: deleted
  `content` TEXT NOT NULL, -- json content
  `add_time` TIMESTAMP,
  `update_time` TIMESTAMP
);

CREATE TABLE `sim_application` (
  `id` INTEGER PRIMARY KEY,
  `user_id` CHAR(50) NOT NULL,
  `status` INT NOT NULL DEFAULT '0', -- 0: draft, 1: submitted, 2: approved, 3: declined, -1: deleted
  `content` TEXT NOT NULL, -- json content
  `comment` TEXT NOT NULL DEFAULT '',
  `add_time` TIMESTAMP,
  `update_time` TIMESTAMP
);

-- common search inverted index
CREATE TABLE `search_inverted_idx` (
  `attribute` CHAR(50) NOT NULL,
  `value` TEXT NOT NULL,
  `type` CHAR(20) NOT NULL,
  `key` CHAR(50) NOT NULL,
  `add_time` TIMESTAMP
);
CREATE INDEX `idx_type_attr_v` ON `search_inverted_idx` (`type`, `attribute`, `value`);
CREATE INDEX `idx_type_k` ON `search_inverted_idx` (`type`, `key`);

-- school search idx
CREATE TABLE `school_inverted_idx` (
  `attribute` CHAR(50) NOT NULL,
  `value` TEXT NOT NULL,
  `school_id` CHAR(50) NOT NULL
);
CREATE INDEX `idx_attr_v` ON `school_inverted_idx` (`attribute`, `value`);

-- TODO: 

-- TODO: article 
CREATE TABLE `article` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `key` char(50) NOT NULL UNIQUE,
  `type` char(10) NOT NULL,
  `title` char(150) NOT NULL,
  `author` CHAR(50) NOT NULL,
  `thumbnail` TEXT NOT NULL DEFAULT '',
  `abstract` TEXT NOT NULL DEFAULT '',
  `content` TEXT NOT NULL,
  `attributes` TEXT NOT NULL DEFAULT '',
  `last_editor` CHAR(50) NOT NULL DEFAULT '',
  `add_time` TIMESTAMP,
  `update_time` TIMESTAMP
);

-- TODO: issue
-- TODO: message

-- TODO: to be implemented next step
-- SQL for dispatching invite code: 
-- 1. select from invite_code_batch where id = <batchId> for update
-- 2. insert into invite_code
-- 3. update invite_code_batch set remain_code_count -=1
CREATE TABLE `invite_code` (
  `code` CHAR(80) PRIMARY KEY,
  `batch_id` INTEGER NOT NULL,
  `owner_id` CHAR(50) NOT NULL DEFAULT '',
  `add_time` TIMESTAMP,
  `update_time` TIMESTAMP
);
-- invite code
CREATE TABLE `invite_code_batch` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `name` CHAR(50) NOT NULL,
  `type` INT NOT NULL, -- 0: infity special batch, 1: limited batch
  `creator_id` CHAR(50) NOT NULL,
  `code_count` INT NOT NULL, -- for infity special batch, count is 0
  `remain_code_count` INT NOT NULL,
  `add_time` TEXT NOT NULL,
  `update_time` TIMESTAMP
);

CREATE TABLE `school_year_rank` (
  `school_id` INT,
  `rank_org` INT,
  `year` INT,
  `rank` INT
);
CREATE INDEX `idx_school_org_year_rank` ON `school_year_rank` (`school_id`, `rank_org`, `year`);

-- TODO raw
CREATE TABLE `raw_school_rank_wsl` (
  `rank` INT,
  `school_name` CHAR(100),
  `province_name` CHAR(10),
  `attributes` CHAR(200)
);

CREATE TABLE `raw_school_year_rank` (
  `rank_org` INT,
  `year` INT,
  `rank` INT,
  `school_name` CHAR(100)
);

CREATE TABLE `raw_school_name_info` (
  `id` INT PRIMARY KEY,
  `name` CHAR(150),
  `province` INT,
  `admin_dept` CHAR(50),
  `location` CHAR(50),
  `is_civil_run` INT
);

CREATE TABLE `system_config` (
  `key` CHAR(50) PRIMARY KEY,
  `title` CHAR(80) NOT NULL,
  `value` TEXT NOT NULL,
  `editor` CHAR(50) NOT NULL DEFAULT '',
  `add_time` TEXT NOT NULL,
  `update_time` TIMESTAMP
);


-- TODO CREATE INDEX `idx_org_year` ON `raw_school_year_rank` (`attribute`, `value`);

